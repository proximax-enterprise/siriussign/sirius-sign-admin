import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'tab-menu',
                children: [
                    {
                        path: '',
                        loadChildren: '../menu/menu.module#MenuPageModule'
                    }
                ]
            },
            {
                path: 'tab-home-zz',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-home/tab-home.module#TabHomePageModule'
                    }
                ]
            },
            {
                path: 'tab-verify',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-verify/tab-verify.module#TabVerifyPageModule'
                    }
                ]
            },
            {
                path: 'tab-history',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-history/tab-history.module#TabHistoryPageModule'
                    }
                ]
            },
            {
                path: 'tab-tasks',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-tasks/tab-tasks.module#TabTasksPageModule'
                    }
                ]
            },
            {
                path: 'upgrade-choose',
                children: [
                    {
                        path: '',
                        loadChildren: '../upgrade-choose/upgrade-choose.module#UpgradeChoosePageModule'
                    }
                ]
            },
            {
                path: 'card-info',
                children: [
                    {
                        path: '',
                        loadChildren: '../card-info/card-info.module#CardInfoPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: 'tab-menu',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'tabs/tab-menu',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class TabsPageRoutingModule { }
