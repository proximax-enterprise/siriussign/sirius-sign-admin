import { MenuController, Platform, AlertController } from '@ionic/angular';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Device } from '@ionic-native/device/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

import { SignDocumentService } from './../services/sign-document.service';
import { HelperService } from './../services/helper.service';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {
    onChooser = false;

    constructor(
        private menu: MenuController,
        private alertController: AlertController,
        private router: Router,
        private chooser: Chooser,
        private platform: Platform,
        private device: Device,
        private filePath: FilePath,
        private signDoc: SignDocumentService
    ) {
        console.log(this.platform);
        this.onChooser = this.device.platform != 'browser' && this.device.platform != null;
        //!this.platform.is('desktop') || !this.platform.is('mobileweb');
        console.log(this.onChooser);
    }

    /**
     * Close the side menu
     */
    closeMenu() {
        this.menu.close("menu");
    }

    /**
     *  Navigate to new doc signing page
     */
    onCreate() {
        this.browseFile();
    }

    /**
     * Navigate to new file view
     */
    goNewFile() {
        this.router.navigate(['new-file-view']);
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    async browseFile() {
        const file: ChooserResult = await this.chooser.getFile('application/pdf')
            .catch((e: any) => { console.log(e); return null; })

        if (file) {
            console.log(file);

            if (file.mediaType != 'application/pdf') {
                this.alertInvalidFileType();
                return;
            }

            let fileSize = file.data.length;
            if (this.platform.is('android'))
                file.uri = await this.filePath.resolveNativePath(file.uri);
            this.signDoc.setDocument(file, fileSize);
            this.goNewFile();
        }
    }

    /**
     * Read file by browser
     * @param files 
     */
    onFile(files) {
        console.log(files);
        const reader = new FileReader();
        reader.onload = (event) => {
            console.log(event.target);
            const target = <any>event.target;
            const file = {
                data: HelperService.convertDataURIToBinary(target.result),
                dataURI: target.result,
                mediaType: target.result.split(';')[0].split(':')[1],
                name: files[0].name,
                uri: ''
            }
            console.log(file);
            if (file.mediaType != 'application/pdf') {
                this.alertInvalidFileType();
                return;
            }
            this.signDoc.setDocument(file, files[0].size);
            let input = <any>document.getElementById("browseNew");
            input.value = '';
            this.goNewFile();
        }
        reader.readAsDataURL(files[0]);
    }

    /**
     * Alert invalid file type
     */
    async alertInvalidFileType() {
        const alert = await this.alertController.create({
            header: 'Invalid File Type',
            message: 'SirisuSign supports .pdf file only.',
            buttons: ['OK']
        });

        await alert.present();
    }
}
