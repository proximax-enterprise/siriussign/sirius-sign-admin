import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, AlertController } from '@ionic/angular';

import { File } from '@ionic-native/file/ngx'
import { FileOpener } from '@ionic-native/file-opener/ngx';

import { DownloadResult } from 'tsjs-chain-xipfs-sdk';

import { SiriusSignDocument } from './../model/siriussign-document.model';

import { VerifyService } from './../services/verify.service';
import { UploadStorageService } from '../services/upload-storage.service';

@Component({
    selector: 'app-verify-done',
    templateUrl: './verify-done.page.html',
    styleUrls: ['./verify-done.page.scss'],
})
export class VerifyDonePage implements OnInit {
    verifyResult;
    docResult: SiriusSignDocument;
    status: string;
    isRealVerify: boolean;

    isDownloading: boolean = false;

    constructor(
        private platform: Platform,
        private router: Router,
        private alertController: AlertController,
        private file: File,
        private fileOpener: FileOpener,
        private verify: VerifyService,
        private uploadStorage: UploadStorageService
    ) {
        this.docResult = this.verify.docResult ? this.verify.docResult : new SiriusSignDocument();
        this.isRealVerify = this.verify.isRealVerify;
        if (this.isRealVerify) {
            this.verifyResult = this.verify.verifyResult;
            this.status = this.verify.verifyResult.status;
            if (this.verify.verifyResult.isValid) this.finishVerify();
        }
        else {
            this.verifyResult = this.verify.auditResult;
            this.status = (this.verify.docResult) ? 'Confirmed' : 'This file is not signed yet';
        }
        this.verify.auditResult = { isValid: false, message: 0 };
        this.verify.docResult = null;
        this.verify.clearDocument();
    }

    ngOnInit() {
    }

    /*
     * Navigate to tab verify
     */
    goVerify() {
        this.verify.clearVerifyResult();
        this.router.navigate(['app/tabs/tab-verify']);
    }

    /**
     * Fetch and store document after cosigning done
     * @param task
     */
    finishVerify() {
       this.verify.isNeedReload = true;
       this.verify.observableIsNeedReload.next(true);
    }


    /**
    * Create HTML web view or write file and call opner on mobile to view pdf
    */
    createDocumentView(writeFileName: string, dataUri: string, blob: Blob) {
        if (this.platform.is('desktop')) {
            let pdfWindow = window.open('');
            setTimeout(() => {
                pdfWindow.document.write(`
                            <!DOCTYPE html>
                            <html>
                            <head>
                            <style>
                            .button {
                                background-color: blue;
                                border: none;
                                color: white;
                                padding: 5px 15px;
                                margin-right: 10px;
                                text-align: center;
                                font-size: 14px;
                                cursor: pointer;
                            }

                            .button:hover {
                                background-color: #4CAF50;
                            }

                            a {
                                margin-bottom: 15px;
                                text-decoration: none;
                            }

                            iframe {
                                margin-top: 10px;
                                height: 90vh;
                            }
                            </style>
                            </head>
                            <body>
                            <a 
                                download='` + writeFileName + `'
                                href='` + dataUri + `' >
                                    <button class="button">Download</button>` + writeFileName + ` 
                            </a>
                            <iframe 
                                width='100%' height='100%' 
                                src='` + dataUri + `'></iframe>
                            </body>
                            </html>
                        `);
                // pdfWindow.document.write('<html><body><object width="100%" height="100%" data="data:application/pdf;base64,' + encodeURI(data) + '" type = "application/pdf" ><embed src="data:application/pdf;base64,' + encodeURI(data) + '" type = "application/pdf" /></object></body></html>');
                pdfWindow.document.title = writeFileName;
            }, 100);
            pdfWindow.document.close();

        }
        else {
            let path = '';
            if (this.platform.is('ios')) path = this.file.documentsDirectory;
            else if (this.platform.is('android')) path = this.file.externalDataDirectory;
            const option = {
                replace: true
            }
            this.file.checkFile(path, writeFileName)
                .then(isExist => { if (isExist) console.log('File is exist') });

            this.file.writeFile(path, writeFileName, blob, option)
                .then(res => {
                    console.log(res);
                    this.fileOpener.open(res.nativeURL, 'application/pdf')
                        .then(() => console.log('File is opened'))
                        .catch(err => console.log('Error opening file', err));
                })
                .catch(err => console.log(err));
        }
    }

    /**
     * Lauch file opner on mobile or open decument in new tab on web browser
     */
    async viewDocument() {
        this.isDownloading = true;
        let downloadResult: DownloadResult;
        try {
            downloadResult = this.docResult.isEncrypt ? await this.uploadStorage.downloadFileEncrypt(this.docResult.uploadTxHash) :
                await this.uploadStorage.downloadFile(this.docResult.uploadTxHash);
        }
        catch (err) {
            this.alertMessage('Failed to download file', err.message);
            this.isDownloading = false;
        }

        const buffer: Buffer = await downloadResult.data.getContentAsBuffer()
            .catch(err => {
                this.alertMessage('Failed to open file', err.message);
                this.isDownloading = false;
                return null;
            });

        const data = buffer.toString('base64');
        const fileName = this.docResult.name.substring(0, this.docResult.name.lastIndexOf('.'));
        const fileType = this.docResult.name.substr(this.docResult.name.lastIndexOf('.'));
        const sda = ' - STH' + this.docResult.signTxHash;
        const writeFileName = fileName + sda + fileType;
        console.log(writeFileName);
        const dataUri = 'data:application/pdf;base64,' + encodeURI(data);
        const blob = new Blob([new Uint8Array(buffer.buffer, buffer.byteOffset, buffer.byteLength)]);
        this.createDocumentView(writeFileName, dataUri, blob);
        this.isDownloading = false;
    }

    /**
     * Present Alert
     * @param message 
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }
}
