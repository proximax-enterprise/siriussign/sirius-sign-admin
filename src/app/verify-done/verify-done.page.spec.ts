import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyDonePage } from './verify-done.page';

xdescribe('VerifyDonePage', () => {
    let component: VerifyDonePage;
    let fixture: ComponentFixture<VerifyDonePage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VerifyDonePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VerifyDonePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
