import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { AlertController, Platform } from '@ionic/angular';

import { Chooser } from '@ionic-native/chooser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { SignatureService } from './../services/signature.service';
import { GlobalService } from '../services/global.service';
import { WalletService } from '../services/wallet.service';
import { HelperService } from './../services/helper.service';

@Component({
    selector: 'app-signature',
    templateUrl: './signature.page.html',
    styleUrls: ['./signature.page.scss'],
})

export class SignaturePage implements OnInit {

    isDone: boolean = false;
    signatureImg: string;
    isProcessing: boolean = false;

    onChooser: boolean = false;

    dpr: number = window.devicePixelRatio;
    isLandscape: boolean = false;
    landscapeW: number = 640;
    landscapeH: number = 336;
    portraitW: number = 360;
    portraitH: number = 616;

    padW: number = 376;
    padH: number = 176;

    @ViewChild(SignaturePad) signaturePad: SignaturePad;
    signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
        'minWidth': 1,
        'canvasWidth': (this.platform.width() < 376) ? this.platform.width() : 376,
        'canvasHeight': (this.platform.width() < 376) ? this.platform.width() * 176 / 376 : 176
    };

    constructor(
        private router: Router,
        private platform: Platform,
        private domSanitizer: DomSanitizer,
        private alertController: AlertController,
        private chooser: Chooser,
        private screenOrientation: ScreenOrientation,
        private global: GlobalService,
        private wallet: WalletService,
        private signature: SignatureService
    ) {
        this.onChooser = !this.global.isBrowser;
    }

    ngOnInit() {
    }

    bypassSecurityTrustUrl = this.domSanitizer.bypassSecurityTrustUrl;

    ionViewWillEnter() {
        console.log(this.platform.width());
        console.log(this.platform.height());
        console.log(this.screenOrientation.type);
        this.isLandscape = this.platform.isLandscape();
        this.isDone = false;
        this.resizeSigPadWhenRotateScreen();
    }

    /**
     * resize SigPad when rotate screen
     */
    resizeSigPadWhenRotateScreen() {
        this.screenOrientation.onChange().subscribe(
            () => {
                if (this.platform.is("android"))
                    this.isLandscape = !this.platform.isLandscape(); // Platform is NOT updated yet at this time
                else
                    this.isLandscape = this.platform.isLandscape();
                console.log(this.screenOrientation.type);
                console.log(this.isLandscape);
                console.log(this.platform.width());
                console.log(this.platform.height());
                if (this.isLandscape) {
                    if (this.platform.is("android")) {
                        this.portraitW = this.platform.width();
                        this.portraitH = this.platform.height();
                    }
                    else {
                        this.landscapeW = this.platform.width();
                        this.landscapeH = this.platform.height();
                    }
                    this.padW = (this.landscapeH - 70) * 376 / 176;
                    this.padH = (this.landscapeH - 70);
                    document.getElementById("sigpad").style.marginTop = '5px';
                }
                else {
                    if (this.platform.is("android")) {
                        this.landscapeW = this.platform.width();
                        this.landscapeH = this.platform.height();
                    }
                    else {
                        this.portraitW = this.platform.width();
                        this.portraitH = this.platform.height();
                    }
                    this.padW = this.portraitW;
                    this.padH = this.portraitW * 176 / 376;
                    document.getElementById("sigpad").style.marginTop = '1em';
                }
                this.signaturePad.set('canvasWidth', this.padW);
                this.signaturePad.set('canvasHeight', this.padH);
            });
    }

    /**
     * Clear pad when init
     */
    ngAfterViewInit() {
        console.log(window.devicePixelRatio);
        // this.signaturePad.resizeCanvas();
        // this.signaturePad.set('canvasWidth', (this.platform.width() < 376) ? this.platform.width() : 376);
        // this.signaturePad.set('canvasHeight', (this.platform.width() < 376) ? this.platform.width() * 88 / 376 : 88);
        this.signaturePad.clear();
    }

    /**
     * Run when untouch
     */
    drawComplete() {
        // will be notified of szimek/signature_pad's onEnd event
        console.log(this.signaturePad.toDataURL('image/png'));
        console.log(this.signaturePad.toDataURL('image/svg+xml'));

        const svgBase64 = this.signaturePad.toDataURL('image/svg+xml');
        let svg = HelperService.base64toUtf8(svgBase64.split(';')[1].split(',')[1]);
        // let patterns = [RegExp('width=\"([^"]*)\"'), RegExp('height=\"([^"]*)\"'), RegExp('viewBox=\"([^"]*)\"')]
        // patterns.forEach(pattern => { svg = svg.replace(pattern, '') });
        let pattern = RegExp('width=\"([^"]*)\"');
        svg = svg.replace(pattern, 'width="376"');
        pattern = RegExp('height=\"([^"]*)\"');
        svg = svg.replace(pattern, 'height="176"');
        pattern = RegExp('viewBox=\"([^"]*)\"');
        svg = svg.replace(pattern, 'viewBox="0 0 ' + this.padW + ' ' + this.padH + '"');
        const fullSvgBase64 = HelperService.utf8toBase64(svg);
        this.signatureImg = 'data:image/svg+xml;base64,' + fullSvgBase64;
    }

    /**
     * Run when touch
     */
    drawStart() {
        // will be notified of szimek/signature_pad's onBegin event
        console.log('begin drawing');
    }

    /**
     * Clear signautre pad
     */
    onClear() {
        this.signaturePad.clear();
    }

    /**
     * Done draw sigpad
     */
    onDone() {
        this.isDone = true;
        setTimeout(() => this.drawSignature(), 100);
    }

    drawSignature() {
        console.log(this.dpr);
        const imgCanvas = <HTMLCanvasElement>document.getElementById('signature-img');
        imgCanvas.width = 376;
        imgCanvas.height = 176;
        imgCanvas.style.width = '376px';
        imgCanvas.style.maxWidth = '90%';

        const ctx = imgCanvas.getContext('2d');
        ctx.clearRect(0, 0, imgCanvas.width, imgCanvas.height);
        const signatureImgObj: HTMLImageElement = document.createElement('img');
        signatureImgObj.src = this.signatureImg;
        signatureImgObj.width = 376;
        signatureImgObj.height = 176;
        setTimeout(() => {
            ctx.drawImage(signatureImgObj, 0, 0, signatureImgObj.width, signatureImgObj.height);
        }, 100);
    }

    /**
     * Create wallet and goto sign in
     */
    async createWallet() {
        await this.wallet.createWallet();
        this.wallet.createNotarizationWallet();
        this.wallet.clearInfo();
        this.global.setIsSignedUp(true);
    }

    /**
     * Back to draw signature
     */
    onBack() {
        this.isDone = false;
        this.isLandscape = this.platform.isLandscape();
        console.log(this.isLandscape);
        setTimeout(() => {
            if (this.isLandscape) {
                this.signaturePad.set('canvasWidth', (this.landscapeH - 70) * 376 / 176);
                this.signaturePad.set('canvasHeight', (this.landscapeH - 70));
                document.getElementById("sigpad").style.marginTop = '5px';
            }
            else {
                this.signaturePad.set('canvasWidth', this.portraitW);
                this.signaturePad.set('canvasHeight', this.portraitW * 176 / 376);
            }
            this.resizeSigPadWhenRotateScreen();
        }, 10);
    }

    /**
     * Confirm signature
     */
    async onConfirm() {
        this.isProcessing = true;
        try {
            // await this.signature.assign(this.signatureImg);
            const owner = this.wallet.getInfoName();
            await this.signature.storeSignature(owner, this.signatureImg, '');
            // await this.signature.fetchFromStorage();
            await this.createWallet();
            this.isProcessing = false;
            this.router.navigate(['sign-in']);
            // this.router.navigateByUrl('app/tabs/tab-menu/account');
        }
        catch (err) {
            this.alertMessage('Failed to register the signature', err.message);
            this.isProcessing = false;
        }
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    browseFile() {
        this.chooser.getFile('image/svg+xml')
            .then(file => {
                if (file) {
                    console.log(file);
                    this.signatureImg = file.dataURI;
                    if (file.mediaType != 'image/svg+xml') {
                        this.alertMessage('Invalid File Type', 'SirisuSign supports .svg file only.');
                        return;
                    }
                    this.isDone = true;
                }
            })
            .catch((e: any) => console.log(e));
    }

    /**
     * Read file by browser
     * @param files
     */
    onFile(files) {
        console.log(files);
        const reader = new FileReader();
        reader.onload = (event) => {
            console.log(event.target);
            const target = event.target as any;
            const mediaType = target.result.split(';')[0].split(':')[1];
            if (mediaType != 'image/svg+xml') {
                this.alertMessage('Invalid File Type', 'SirisuSign supports .svg file only.');
                return;
            }
            this.signatureImg = target.result;
            this.isDone = true;
        };
        reader.readAsDataURL(files[0]);
    }

    /**
     * Present Alert
     * @param message
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }
}
