import { ComponentModule } from './../components/component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelectSignerPage } from './select-signer.page';

const routes: Routes = [
    {
        path: '',
        component: SelectSignerPage
    },
    {
        path: 'new-interactive',
        loadChildren: '../new-interactive/new-interactive.module#NewInteractivePageModule'
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ComponentModule,
        RouterModule.forChild(routes)
    ],
    declarations: [SelectSignerPage]
})
export class SelectSignerPageModule { }
