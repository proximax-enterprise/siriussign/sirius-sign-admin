import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMultiSignPage } from './new-multi-sign.page';

describe('NewMultiSignPage', () => {
  let component: NewMultiSignPage;
  let fixture: ComponentFixture<NewMultiSignPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMultiSignPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMultiSignPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
