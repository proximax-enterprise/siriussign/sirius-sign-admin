import { Component, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

import { GlobalService } from './../services/global.service';
import { WalletService } from './../services/wallet.service';
import { OrganizationsService } from '../services/organizations.service';


@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.page.html',
    styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage {

    errorMess = '';
    inputDat = {
        email: '',
        pass: ''
    };

    passView = 'password';

    wallets;

    a;

    selectedWallet;
    isFingerAllow: boolean = false;
    isKeyboardShow: boolean = false;

    constructor(
        private route: Router,
        private alertController: AlertController,
        private faio: FingerprintAIO,
        public global: GlobalService,
        private wallet: WalletService,
        private or: OrganizationsService
    ) {
        this.setStyleDefault();
    }

    async ionViewWillEnter() {
        // this.a = await this.or.fetchPrivateKey();
        // console.log(this.a);
        this.setStyleDefault();
        this.wallets = await this.wallet.fetchWallets();
        // console.log(this.wallets);
        this.selectedWallet = this.wallets[0];
        this.isFingerAllow = this.selectedWallet.tertiary.length == 64;
        this.inputDat.email = this.selectedWallet.name;
        this.global.isAllowFingerprint = this.isFingerAllow;

        // Observe Keyboard show event
        window.addEventListener('keyboardWillShow', (e) => {
            this.isKeyboardShow = true;
            this.setStyle('root', '--height-logo', 'fit-content');
            this.setStyle('root', '--padding-top-logo', '20px');
            this.setStyle('root', '--height-form', 'fit-content');
        });

        // Observe Keyboard hide event
        window.addEventListener('keyboardWillHide', () => {
            this.isKeyboardShow = false;
            this.setStyleDefault();
        });
    }

    /**
     * Set style value
     * @param selector 
     * @param property 
     * @param value 
     */
    setStyle(selector: string, property: string, value: string) {
        document.documentElement.style.setProperty(property, value);
    }

    /**
     * Set style to default values
     */
    setStyleDefault() {
        this.setStyle('root', '--height-logo', '30vh');
        this.setStyle('root', '--padding-top-logo', '15vh');
        this.setStyle('root', '--img-width-logo', '90px');
        this.setStyle('root', '--height-welcome', '20vh');
        this.setStyle('root', '--height-form', '40vh');
        this.setStyle('root', '--height-signup', '8vh');
    }

    /**
     * Show/hide password
     */
    togglePassView() {
        if (this.passView == 'password') this.passView = 'text';
        else this.passView = 'password';
    }

    /**
      * Validate email input 
      */
    checkEmail() {
        const emailPattern = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
        let isMatch = emailPattern.test(this.inputDat.email.toLowerCase());
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your email is invalid.';
            return false;
        }
    }

    /**
      * Validate password input
      */
    checkPass() {
        return true;
    }

    /*
     * Validate form input
     */
    checkInput() {
        if (this.checkEmail())
            return this.checkPass();
        return false;
    }
    /**
     * Launch Sign in process
     */
    async onSignIn() {
        let isNoEmail = this.inputDat.email == '';
        let isNoPass = this.inputDat.pass == '';

        if (isNoEmail) this.errorMess = 'Please enter your email.';
        else if (isNoPass) this.errorMess = 'Please enter your password.';
        else if (this.checkInput()) {
            //let isExist = await this.wallet.checkWalletExist(this.inputDat.email);
            try {
                this.errorMess = '';
                // this.selectedWallet = await this.wallet.getWalletByName(this.inputDat.email);
                await this.wallet.signIn(this.selectedWallet, this.inputDat.pass);
                const isDisableFingerprint = this.selectedWallet.tertiary.length < 63;
                if (!this.global.isBrowser) {
                    if (!this.isFingerAllow && !isDisableFingerprint) {
                        const password = this.inputDat.pass;
                        await this.presentAlertFingerprint(() => {
                            console.log(this.inputDat.pass);
                            console.log(password);
                            this.global.isAllowFingerprint = true;
                            this.wallet.allowFingerprint(this.inputDat.email, password);
                        });
                    }
                }
                this.inputDat.pass = '';
                this.goHome();
            }
            catch (err) {
                console.log(err);
                this.errorMess = 'Email or Password is invalid.';
            }
        }
    }

    /**
     * Ask user if using fingerprint or not
     * @param fn 
     */
    async presentAlertFingerprint(fn) {
        const alert = await this.alertController.create({
            header: 'Sign in with one touch?',
            message: 'Do you want to use your fingerprint in the system to sign in this account?',
            inputs: [
                {
                    name: 'remember',
                    type: 'checkbox',
                    label: 'Remember this choice',
                    value: 'remember',
                    checked: true
                },
            ],
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        this.goHome();
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.fingerprint(fn);
                    }
                }
            ]
        });

        alert.present();
        let result = await alert.onDidDismiss();
        if (result.data.values.length == 1) this.wallet.disableFingerprint(this.inputDat.email);
    }


    /**
     * Fingerprint
     */
    fingerprint(fn) {
        this.faio.isAvailable()
            .then(res => {
                console.log(res);
                this.faio.show({
                    // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
                    // clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
                    disableBackup: true,  //Only for Android(optional)
                    // localizedFallbackTitle: 'Use Pin', //Only for iOS
                    // localizedReason: 'Please authenticate' //Only for iOS
                })
                    .then((result: any) => { console.log(result); fn(); })
                    .catch((error: any) => console.log(error));
            })
            .catch(err => { console.log('Fingerprint is not available'); console.log(err); });
    }

    /**
     * Trigger fingerprint sign in
     */
    signInWithFingerprint() {
        if (this.isFingerAllow) {
            this.fingerprint(() => {
                this.wallet.signInWithFingerprint(this.selectedWallet);
                this.goHome();
            });
        }
    }

    /**
     * Select email
     * @param event 
     */
    onSelectEmail(event) {
        this.inputDat.email = event.target.value;
        const index = this.wallets.map(wallet => wallet.name).indexOf(this.inputDat.email);
        this.selectedWallet = this.wallets[index];
        this.isFingerAllow = this.selectedWallet.tertiary.length == 64;
        this.global.isAllowFingerprint = this.isFingerAllow;
        this.signInWithFingerprint();
    }

    /*
     * Navigate to App's Home
     */
    goHome() {
        this.route.navigate(['home']);
    }

    /*
     * Navigate to sign-up
     */
    goSignUp() {
        this.route.navigate(['sign-up']);
    }
}
