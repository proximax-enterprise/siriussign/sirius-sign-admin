import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInPage } from './sign-in.page';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { IonicStorageModule} from '@ionic/storage';


xdescribe('SignInPage', () => {
    let component: SignInPage;
    let fixture: ComponentFixture<SignInPage>;

    

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignInPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                CommonModule,
                FormsModule,
                IonicStorageModule.forRoot()
            ],
            providers: [
                { provide: Router, useClass: RouterTestingModule },
                Device,
                FingerprintAIO,
                Storage
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignInPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
