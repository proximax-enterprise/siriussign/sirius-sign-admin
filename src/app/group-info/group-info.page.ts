import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SiriusGroups, GroupElement } from './../services/group.service';

import { GlobalService } from '../services/global.service';
import { GroupService } from '../services/group.service';
import { SigningService } from './../services/signing.service';

@Component({
    selector: 'app-group-info',
    templateUrl: './group-info.page.html',
    styleUrls: ['./group-info.page.scss'],
})
export class GroupInfoPage implements OnInit {

    myAccount = '';
    selectedGroup: SiriusGroups;

    constructor(private router: Router, public group: GroupService, private global: GlobalService, private signing: SigningService) { }

    ngOnInit() {
        this.myAccount = this.global.loggedAccount.publicKey;
        this.selectedGroup = this.group.selectedGroup;
        this.group.selectedGroup = null;
        console.log(this.selectedGroup);
    }

    /*
     * Naviagte to home
     */
    goHome() {
        this.router.navigate(['app']);
    }

    /**
     * Join group
     */
    onJoin() {
        const acceptTx = this.signing.cosignMultisigTransaction(this.selectedGroup.transaction, this.global.loggedAccount);
        const hash = this.selectedGroup.transaction.transactionInfo.hash;
        const deadline = this.selectedGroup.transaction.deadline;
        console.log(acceptTx);
        this.signing.announceAggregateBondedCosignature(acceptTx)
            .subscribe(async res => {
                console.log(res);
                const group: GroupElement = {
                    name: this.selectedGroup.name,
                    multisigAcc: this.selectedGroup.multisigAcc,
                    creationTxHash: hash,
                    creationDate: new Date(deadline.value.plusHours(-23).toString()).getTime(),
                    creator: this.selectedGroup.creator,
                    members: this.selectedGroup.members
                }
                await this.group.storeGroup(group);
                this.group.waitingGroups = [];
                setTimeout(() => {
                    this.group.fetchJoinGroup();
                }, 2000);
                this.goHome();
            }, err => {
                console.log('[Group] Can not join group');
                console.log(err)
            });
    }
}
