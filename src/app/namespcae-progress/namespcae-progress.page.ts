import { SiriusSignSigningTask } from './../model/siriussign-task.model';
import { SignatureService } from './../services/signature.service';
import { UploadStorageService } from './../services/upload-storage.service';
import { DrawSignatureService } from './../services/draw-signature.service';
import { MultitaskService } from './../services/multitask.service';
import { DocumentClassificationService } from './../services/document-classification.service';
import { Component, OnInit } from '@angular/core';
import { 
  TransferTransaction,
  Deadline,
  NetworkType,
  PlainMessage,
  UInt64,
  AddressAliasTransaction,
  RegisterNamespaceTransaction,
  NamespaceId, 
  AliasActionType,
  Mosaic,
  Account, Transaction} from 'tsjs-xpx-chain-sdk';
import { MonitorService } from './../services/monitor.service';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';
import { SignDocumentService } from '../services/sign-document.service';
import { GlobalService } from '../services/global.service';
import { Router } from '@angular/router';
import { OrganizationsService } from '../services/organizations.service';
import { DocumentStorageService } from './../services/document-strorage.service';

@Component({
  selector: 'app-namespcae-progress',
  templateUrl: './namespcae-progress.page.html',
  styleUrls: ['./namespcae-progress.page.scss'],
})
export class NamespcaeProgressPage implements OnInit {
  generationHash = '031248ED76E99807C198D2A219EEBBE678D78B036D92BA505988994356DF61B8';

  constructor(
    private router: Router,
    private global: GlobalService,
    private signDoc: SignDocumentService,
    private uploadStorage: UploadStorageService,
    private monitor: MonitorService,
    private signingWithoutMultisig: SigningWithoutMultisigService,
    private signature: SignatureService,
    private drawSignature: DrawSignatureService,
    public multitask: MultitaskService,
    private documentClassification: DocumentClassificationService,
    private documentStorage: DocumentStorageService,
    private organizationsService: OrganizationsService
) {

  this.createNamespaceFlow();
  this.goTasks();
  // this.createNamespace()

  }

  ngOnInit() { }


  async createNamespaceFlow() {
    // send xpx////////////////
    
    // const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    // const sender = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);

    // const recipient = Account.generateNewAccount(this.global.networkType); // Acc link to namespace

    // console.log(recipient.privateKey);
    // const namespaceId = new NamespaceId('prx.xpx');
    // const amount = UInt64.fromUint(500000);
    // const mosaic = new Mosaic(namespaceId, amount);
    // const tx = TransferTransaction.create(
    //     Deadline.create(),
    //     recipient.address,
    //     [mosaic],
    //     PlainMessage.create('Test transaction time'),
    //     NetworkType.TEST_NET,
    //     UInt64.fromUint(0),
    // );
    // const signedTransaction = sender.sign(tx, this.generationHash);
    // const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
    //   recipient.address,
    //   signedTransaction.hash,
    //   () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
    //   .catch(err => {
    //     console.log(err);
    //     alert(err);
    //     this.organizationsService.isCreatingOr = false;
    //     return null;
    //   });

    // const recipient = Account.generateNewAccount(this.global.networkType);
    // const tx = TransferTransaction.create(
    //   Deadline.create(),
    //   recipient.address,
    //   [],
    //   PlainMessage.create('Create Organization Account'),
    //   NetworkType.PRIVATE_TEST,
    //   UInt64.fromUint(0),
    // );
    // const signedTx = this.global.loggedAccount.sign(tx, this.generationHash);
    // console.log(signedTx);

    // const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
    //   recipient.address,
    //   signedTx.hash,
    //   () => { this.signingWithoutMultisig.announceTransaction(signedTx); })
    //   .catch(err => {
    //       console.log(err);
    //       return null;
    //   });

    // create namespace/////////

    const duration = UInt64.fromUint(1000000);
    const registerNamespaceTransaction = RegisterNamespaceTransaction.createRootNamespace(
      Deadline.create(),
      this.organizationsService.namespaceName,
      duration,
      NetworkType.PRIVATE_TEST,
      UInt64.fromUint(0));
    const signedTxCreate = this.global.loggedAccount.sign(registerNamespaceTransaction, this.generationHash);
    const monitorTxCreate: Transaction = await this.monitor.waitForTransactionConfirmed(
      this.global.loggedAccount.address,
      signedTxCreate.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTxCreate); })
      .then(async () => {
        await this.organizationsService.storeNamespace(this.organizationsService.namespaceName);
        // store priv ///////////////
        // this.organizationsService.storePriv(this.global.loggedAccount);
        this.organizationsService.isCreatingOr = !this.organizationsService.isCreatingOr;
        alert('Create Organization Success!');
        this.organizationsService.isSignatories = false;
        this.organizationsService.isDocument = false;
        this.organizationsService.isNotify = false;
        this.router.navigateByUrl('/create-organizations');
      })
      .catch(err => {
          console.log(err);
          alert(err.status);
          this.organizationsService.isCreatingOr = false;
          return null;
      });

    // Link Acc to Namespace ////////////////////

    // const namespaceIdCreate = new NamespaceId(this.organizationsService.namespaceName);
    // const addressAliasTransaction = AddressAliasTransaction.create(
    //   Deadline.create(),
    //   AliasActionType.Link,
    //   namespaceIdCreate,
    //   recipient.address,
    //   NetworkType.PRIVATE_TEST,
    //   UInt64.fromUint(0),
    // );
    // const signedTxLink = this.global.loggedAccount.sign(addressAliasTransaction, this.generationHash);
    // const monitorTxLink: Transaction = await this.monitor.waitForTransactionConfirmed(
    //   recipient.address,
    //   signedTxLink.hash,
    //   () => { this.signingWithoutMultisig.announceTransaction(signedTxLink); })
    //   .then(async () => {
    //     // store namespace
    //     await this.organizationsService.storeNamespace(this.organizationsService.namespaceName);
    //     // store priv ///////////////
    //     this.organizationsService.storePriv(recipient);
    //     this.organizationsService.isCreatingOr = !this.organizationsService.isCreatingOr;
    //     alert('Create Organization Success!');
    //     this.organizationsService.isSignatories = false;
    //     this.organizationsService.isDocument = false;
    //     this.organizationsService.isNotify = false;
    //     this.router.navigateByUrl('/create-organizations');

    //   })
    //   .catch(err => {
    //     console.log(err);
    //     alert(err.status);
    //     this.organizationsService.isCreatingOr = false;
    //     return null;
    //   });
  }

  async sendXpxToAccLinkNamespace() {
    const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    const sender = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);

    // const privateKeys = '130E7960434F38FA626B85B51B21917A19F3470802237FAAE6C5313A8F3EF962';
    // const recipient = Account.createFromPrivateKey(privateKeys, NetworkType.TEST_NET);

    const recipient = Account.generateNewAccount(this.global.networkType); // Acc link to namespace
    console.log(recipient.privateKey);

    const namespaceId = new NamespaceId('prx.xpx');
    const amount = UInt64.fromUint(10000);
    const mosaic = new Mosaic(namespaceId, amount);
    const tx = TransferTransaction.create(
        Deadline.create(),
        recipient.address,
        [mosaic],
        PlainMessage.create('Test transaction time'),
        NetworkType.TEST_NET,
        UInt64.fromUint(0),
    );
    const signedTransaction = sender.sign(tx, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      recipient.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .then(async () => {
        await this.createNamespace(recipient)
        .then(async () => {
          await this.linkAccountToNamespace(recipient)
          .then(() => this.organizationsService.storePriv(recipient))
          .catch(err => {
            console.log(err);
            return null;
          });
        })
        .catch(err => {
          console.log(err);
          return null;
        });
      })
      .catch(err => {
        console.log(err);
        return null;
      });
  }

  async createNamespace(acc: Account) {
    const duration = UInt64.fromUint(100);
    // const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    const registerNamespaceTransaction = RegisterNamespaceTransaction.createRootNamespace(
      Deadline.create(),
      this.organizationsService.namespaceName,
      duration,
      NetworkType.TEST_NET,
      UInt64.fromUint(0));
    const signedTransaction = acc.sign(registerNamespaceTransaction, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      acc.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
    // return(signedTransaction);
  }

  async linkAccountToNamespace(acc: Account) {
    const namespaceId = new NamespaceId(this.organizationsService.namespaceName);
    // const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    const addressAliasTransaction = AddressAliasTransaction.create(
      Deadline.create(),
      AliasActionType.Link,
      namespaceId,
      acc.address,
      NetworkType.TEST_NET,
    );
    console.log(addressAliasTransaction);
    const signedTransaction = acc.sign(addressAliasTransaction, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      acc.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
    // return (signedTransaction);
  }

  /**
   * Fetch and store document after signing done successfully
   * @param task 
   */
  async finishSigning(task: SiriusSignSigningTask) {
      const isSuccess = (task.document.status == 'Confirmed') || (task.document.status == 'Waiting for cosignatures');
      if (isSuccess) {
          const doc = await this.documentClassification.fetchDocumentBySigningTxHash(task.document.signTxHash);
          doc.localUrl = task.document.file.uri;
          // this.documentStorage.storeDocument(doc);
          this.documentStorage.storeDocumentWithNamespace(doc, this.organizationsService.queryKeydoc);
          this.documentClassification.classify(doc);
          this.documentClassification.sortDocs('compledAndWaiting');
      }
  }

  /**
   * Navigate to tasks tab
   */
  goTasks() {
      this.global.setIsProgressDone(true);
      this.multitask.selectedTask = null;
      // this.router.navigate(['/app/tabs/tab-tasks']);
      this.organizationsService.isNotify = true;
      this.organizationsService.isDocument = false;
      this.organizationsService.isSignatories = false;
      this.router.navigateByUrl('/new-organization');

  }

}
