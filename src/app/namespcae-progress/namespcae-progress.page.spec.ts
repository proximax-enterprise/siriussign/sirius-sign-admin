import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamespcaeProgressPage } from './namespcae-progress.page';

describe('NamespcaeProgressPage', () => {
  let component: NamespcaeProgressPage;
  let fixture: ComponentFixture<NamespcaeProgressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamespcaeProgressPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamespcaeProgressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
