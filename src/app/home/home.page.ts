import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrganizationsService } from '../services/organizations.service';
import { MonitorService } from '../services/monitor.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  organizations: string[];

  constructor(
    private router: Router,
    private organizationService: OrganizationsService,
    private monitor: MonitorService
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.monitor.openListener();
    this.organizationService.setQuerykeyNamespace();
  }

  navToHome() {
    this.router.navigateByUrl('/home');
  }
  navToTest() {
    this.router.navigateByUrl('/test');
  }
  navToCreate() {
    this.router.navigateByUrl('/create-organizations');
  }
  navToNew() {
    this.router.navigateByUrl('/new-organization');
  }
  navToAmin() {
    this.router.navigateByUrl('/admin');
  }
}
