import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrganizationsService } from '../services/organizations.service';
import { MonitorService } from '../services/monitor.service';
import { MenuController, AlertController, ToastController, ActionSheetController } from '@ionic/angular';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';

import { 
  Deadline,
  NetworkType,
  UInt64,
  RegisterNamespaceTransaction,
  Account, Transaction} from 'tsjs-xpx-chain-sdk';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-create-new-project',
  templateUrl: './create-new-project.page.html',
  styleUrls: ['./create-new-project.page.scss'],
})
export class CreateNewProjectPage implements OnInit {
  subnamespaceNames: string[];
  showNamespace: string;
  constructor(
    private router: Router,
    private monitor: MonitorService,
    private alertCtrl: AlertController,
    private organizationsService: OrganizationsService,
    private signingWithoutMultisig: SigningWithoutMultisigService,
    private global: GlobalService

  ) { }

  ionViewWillEnter() {
    this.getsubNamespace();
    this.showNamespace = this.organizationsService.namespaceName;
  }

  ngOnInit() {
  }

  navToHome() {
    this.router.navigateByUrl('/home');
  }
  navToTest() {
    this.router.navigateByUrl('/test');
  }
  navToCreate() {
    this.router.navigateByUrl('/create-organizations');
  }
  navToNew() {
    this.router.navigateByUrl('/new-organization');
  }
  navToAmin() {
    this.router.navigateByUrl('/admin');
  }

  async onShowPrivateKey() {
    const alert = await this.alertCtrl.create({
        header: 'Please enter your Project name',
        inputs: [
            {
                name: 'name',
                type: 'text'
            }
        ],
        buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {
                    console.log('Confirm Cancel');
                }
            }, {
                text: 'Ok',
                handler: (data) => {
                    this.createProject(data.name);
                }
            }
        ]
    });

    await alert.present();
  }
  async createProject(name: string) {
    console.log('creating....' + name);
    console.log(this.organizationsService.queryKeySubNamespace);
    await this.createSubnamespace(name);

  }

  navToContent() {
    this.router.navigateByUrl('/organization-content');
  }

  async createSubnamespace(name: string) {
    const check = /^[a-za-z0-9-_]*$/gm.test(name);
    if (!check || name == '' ){alert('Wrong format!!! Allowed characters are a, b, c, …, z, 0, 1, 2, …, 9, _ , -.');
    } else {
    this.organizationsService.isCreatingPro = !this.organizationsService.isCreatingPro;
    // replace with root namespace name
    const rootNamespaceName = this.organizationsService.namespaceName;
    console.log(rootNamespaceName);
    // replace with root subnamespace name
    const subnamespaceName = name;
    console.log(subnamespaceName);
    // replace with network type
    const namespaceRegistrationTransaction = RegisterNamespaceTransaction.createSubNamespace(
        Deadline.create(),
        subnamespaceName,
        rootNamespaceName,
        NetworkType.PRIVATE_TEST,
        UInt64.fromUint(0));

    const signedTransaction = this.global.loggedAccount.sign(namespaceRegistrationTransaction, this.global.networkGenerationHash);

    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      this.global.loggedAccount.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .then( async () => {
        await this.organizationsService.storeSubNamespace(name);
      })
      .then(async () => {
        this.organizationsService.isCreatingPro = !this.organizationsService.isCreatingPro;
        this.subnamespaceNames = await this.organizationsService.fetchSubNamespace();
        this.organizationsService.setQuerykeyDoc(name);
        this.organizationsService.subnamespacename = name;
        alert('Create project success!!!');
        this.navToContent();
      })
      .catch(err => {
          console.log(err);
          this.organizationsService.isCreatingPro = false;
          alert(err.status);
          return null;
      });
    }
  }

  async getsubNamespace() {
    this.subnamespaceNames = await this.organizationsService.fetchSubNamespace();
    console.log(this.subnamespaceNames);
  }

  async openProject(project) {
    this.organizationsService.setQuerykeyDoc(project);
    console.log(this.organizationsService.queryKeydoc);
    this.organizationsService.subnamespacename = project;
    this.navToContent();
  }

}
