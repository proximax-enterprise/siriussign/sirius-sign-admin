import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { CommonHeaderComponent } from './common-header/common-header.component';
import { SignModeHeaderComponent } from './sign-mode-header/sign-mode-header.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule
    ],
    declarations: [CommonHeaderComponent, SignModeHeaderComponent],
    exports: [CommonHeaderComponent, SignModeHeaderComponent]
})
export class ComponentModule { }