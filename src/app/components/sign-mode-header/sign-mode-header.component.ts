import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-sign-mode-header',
    templateUrl: './sign-mode-header.component.html',
    styleUrls: ['./sign-mode-header.component.scss'],
})
export class SignModeHeaderComponent implements OnInit {

    @Input() mode: 'multi' | 'single';
    @Input() multiLink: string;
    @Input() singleLink: string;

    constructor(private navCtrl: NavController) { }

    ngOnInit() { }

    /**
     * Navigate to multi sign page
     */
    goMulti() {
        this.navCtrl.navigateForward([this.multiLink]);
    }

    /**
     * Navigate to single sign page
     */
    goSingle() {
        this.navCtrl.navigateForward([this.singleLink]);
    }
}
