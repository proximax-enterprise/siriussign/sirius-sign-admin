import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { SigningService } from './signing.service';
import { HelperService } from './helper.service';
import { SiriusDocument } from '../model/sirius-document.model';

@Injectable({
    providedIn: 'root'
})
export class HomeService {
    numNeedMySign = 0;
    numWaitOthers = 0;
    docs = [];
    isLoading = false;

    constructor(
        private global: GlobalService,
        private signing: SigningService
    ) { }

    ngOnInit() {
    }

    dateToShortString = HelperService.dateToShortString;

    /**
     * Add a documnent to home screen
     * @param document 
     */
    add(document: SiriusDocument) {
        const doc = {
            id: document.id,
            name: document.name,
            own: document.owner == this.global.loggedAccount.address.plain(),
            signed: document.isSigned,
            actionDate: document.uploadDate,
            lastModifiedDate: document.uploadDate,
            numCoSign: document.cosigners.length,
            numSigned: document.cosignatures.length + 1
        }
        this.docs.push(doc);
        if (doc.own) this.numWaitOthers += 1;
        else if (!doc.signed) this.numNeedMySign += 1;
        console.log('[Debug] Added: ' + doc.id);
        this.sortDocs();
    }

    /**
     * Fetch aggregate bonded transaction
     * @async
     */
    async fetchSigningDoc() {
        let numDone = 0;
        return new Promise(resolve => {
            this.signing.getMultisigAccountInfo(this.global.loggedAccount.address)
                .subscribe(multisigAccInfo => {
                    const multisigAccs = multisigAccInfo.multisigAccounts;
                    this.signing.multisigAccs = multisigAccs.map(acc => acc.address.plain());
                    multisigAccs.forEach((multisigAcc, index) => {
                        let lastId = null;
                        let isLast = false;
                        let loadAll = async () => {//setInterval(() => {
                            if (!isLast) {
                                await this.signing.fetchAggregateBondedTransactions(multisigAcc, lastId, transactions => {
                                    if (transactions.length != 0) {
                                        lastId = transactions[transactions.length - 1].transactionInfo.id;
                                    }
                                    isLast = transactions.length < 10;
                                    transactions.forEach(transaction => {
                                        numDone -= 1;
                                        this.signing.addMultisigDocInfo(transaction, doc => {
                                            this.add(doc);
                                            numDone += 1;
                                            if (numDone == multisigAccs.length) resolve();
                                        })
                                    });
                                    loadAll();
                                });
                            }
                            else {
                                numDone += 1;
                                if (numDone == multisigAccs.length) resolve();
                                // this.sortDocs();
                                // clearInterval(loadAll);
                            }
                        };
                        loadAll();
                        // }, 2000);
                    });
                },
                err => resolve());
        });
    }

    /**
     * Reload aggregate bonded transactions
     * @async
     */
    async reload() {
        if (this.isLoading) return;
        this.isLoading = true;
        this.docs = [];
        this.signing.documentsInfo = [];
        this.numNeedMySign = 0;
        this.numWaitOthers = 0;
        await this.fetchSigningDoc();
        this.isLoading = false;
    }

    /**
     * Sort document in descending order of id/time
     */
    sortDocs() {
        let signedDocs = this.docs.filter(doc => (doc.signed == 1));
        let needSignDocs = this.docs.filter(doc => (doc.signed != 1));
        signedDocs.sort((a, b) => {
            if (a.id > b.id) return -1;
            else return 1;
        });
        needSignDocs.sort((a, b) => {
            if (a.id > b.id) return -1;
            else return 1;
        });
        this.docs = [...needSignDocs, ...signedDocs];
    }
}
