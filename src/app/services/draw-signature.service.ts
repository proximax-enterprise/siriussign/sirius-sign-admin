import { Injectable } from '@angular/core';

import { NamePosition } from '../model/signature-canvas.model';
import { Account } from 'tsjs-xpx-chain-sdk';
export interface SignaturePosition {
    page: number,
    x: number,
    y: number,
    name: string,
    namePosition: NamePosition
}

export interface SignatureInfo {
    publicKey: string,
    signaturePosition: SignaturePosition
}

export interface VerifyInfo {
    publicKey: string,
    publicAcc: Account
    // signaturePosition: SignaturePosition
}

export interface SignatureImageInfo {
    publicKey: string,
    signatureImg: string
}

@Injectable({
    providedIn: 'root'
})
export class DrawSignatureService {

    name: string;
    pdfUri: string;

    isInit: boolean = false;
    signers: SignatureInfo[] = [];
    signatureImgs: SignatureImageInfo[] = [];
    selectedSignerIndex: number = 0;

    cosigner: SignatureInfo;

    constructor() {
        this.selectedSignerIndex = 0;
    }

    /**
     * Reset draw state
     */
    reset() {
        this.selectedSignerIndex = 0;
        this.signers = [];
        this.isInit = false;
    }

    /**
     * Init signer with signature
     * @param publicKey 
     * @param name 
     */
    setSigner(publicKey: string, name: string) {
        const signatureInfo: SignatureInfo = {
            publicKey: publicKey,
            signaturePosition: {
                name: name,
                namePosition: NamePosition.NONE,
                page: 1,
                x: 0,
                y: 0
            }
        }
        this.signers.push(signatureInfo);
    }

    /**
     * Update position when user drag and drop signature
     * @param publicKey 
     * @param page 
     * @param x 
     * @param y 
     * @param namePosition 
     */
    updateSignaturePosition(publicKey: string, page: number, x: number, y: number, namePosition: NamePosition) {
        const publicKeys = this.signers.map(signer => signer.publicKey);
        const index = publicKeys.indexOf(publicKey);
        this.signers[index].signaturePosition.page = page;
        this.signers[index].signaturePosition.x = x;
        this.signers[index].signaturePosition.y = y;
        this.signers[index].signaturePosition.namePosition = namePosition;
    }
}
