import { TestBed } from '@angular/core/testing';

import { BuyCryptoService } from './buy-crypto.service';

describe('BuyCryptoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BuyCryptoService = TestBed.get(BuyCryptoService);
    expect(service).toBeTruthy();
  });
});
