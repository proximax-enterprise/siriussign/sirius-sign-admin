import { HelperService } from './helper.service';
import { Injectable } from '@angular/core';

import * as CryptoJS from 'crypto-js';

import {
    BlockchainNetworkConnection,
    IpfsConnection,
    UploadParameter,
    Uploader,
    ConnectionConfig,
    Protocol,
    Uint8ArrayParameterData,
    Downloader,
    DownloadParameter,
    DownloadResult
} from 'tsjs-chain-xipfs-sdk';
import { PublicAccount } from 'tsjs-xpx-chain-sdk';

import { GlobalService } from './global.service';
import { SignDocument } from './sign-document.service';

@Injectable({
    providedIn: 'root'
})
export class UploadStorageService {
    // Creates ipfs connection
    ipfsConnection = new IpfsConnection(
        this.global.ipfsDomain, // the host or multi address
        this.global.ipfsPort, // the port number
        (this.global.ipfsProtocol == 'https') ? { protocol: 'https' } : { protocol: 'http' } // the optional protocol
    );

    // Creates Proximax blockchain network connection
    blockchainConnection = new BlockchainNetworkConnection(
        this.global.blockchainNetworkType, // the network type
        this.global.apiDomain, // the rest api base endpoint
        this.global.apiPort, // the optional websocket end point
        (this.global.apiProtocol == 'https') ? Protocol.HTTPS : Protocol.HTTP
    );

    // Connection Config
    conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(this.blockchainConnection, this.ipfsConnection);

    //
    uploader = new Uploader(this.conectionConfig);
    dowloader = new Downloader(this.conectionConfig);

    constructor(private global: GlobalService) { }

    /**
     * Create upload params builder
     * @param document 
     */
    private createParamsBuilder(document: SignDocument) {
        const file = document.file;
        console.log(file);
        const fileContent = file.data;
        const fileName = file.name;
        const fileType = file.mediaType;

        const documentAccountPriv = CryptoJS.AES.encrypt(document.documentAcc.privateKey, this.global.loggedAccount.privateKey);
        const documentAccountEncPriv = HelperService.base64toHex(documentAccountPriv.toString())

        const metaData = new Map<string, string>([
            ['SFH', document.fileHash],
            ['SDA', documentAccountEncPriv]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(fileContent, fileName, '', fileType, metaData);
        console.log(metaParams);
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);
        return uploadParamsBuilder;
    }

    private createParamsBuilders(document: any) {
        const file = document.file;
        console.log(file);
        const fileContent = file.data;
        const fileName = file.name;
        const fileType = file.mediaType;
        console.log(document.documentAcc.privateKey);
        
        
        const documentAccountPriv = CryptoJS.AES.encrypt(document.documentAcc.privateKey
            , 'B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433');
        const documentAccountEncPriv = HelperService.base64toHex(documentAccountPriv.toString());

        const metaData = new Map<string, string>([
            ['SFH', document.fileHash],
            ['SDA', documentAccountEncPriv]
        ]);

        const privateKey = 'B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433';

        const metaParams = Uint8ArrayParameterData.create(fileContent, fileName, '', fileType, metaData);
        console.log(metaParams);
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);
        return uploadParamsBuilder;
    }

    /**
     * Upload file from signDoc
     * @param document 
     */
    uploadFile(document: SignDocument) {
        try {
            const uploadParamsBuilder = this.createParamsBuilder(document);
            const uploadParams = uploadParamsBuilder
                .withRecipientPublicKey(document.documentAcc.publicKey)
                .withTransactionMosaics([])
                .build();
            console.log(uploadParams);

            // call upload services
            return this.uploader.upload(uploadParams);
            // console.log('Result');
            // console.log(result);
        }
        catch (err) {
            console.log(err);
            throw new Error('Failed to upload file');
        }
    }

    uploadFiles(document: any) {
        try {
            const uploadParamsBuilder = this.createParamsBuilders(document);
            const uploadParams = uploadParamsBuilder
                .withRecipientPublicKey(document.documentAcc.publicKey)
                .withTransactionMosaics([])
                .build();
            console.log(uploadParams);

            // call upload services
            return this.uploader.upload(uploadParams);
            // console.log('Result');
            // console.log(result);
        }
        catch (err) {
            console.log(err);
            throw new Error('Failed to upload file');
        }
    }

    /**
     * Upload file from signDoc
     * @param document 
     */
    uploadFileEncrypt(document: SignDocument) {
        const privateKey = this.global.loggedAccount.privateKey;
        const publicKey = this.global.loggedAccount.publicKey;

        const uploadParamsBuilder = this.createParamsBuilder(document);
        const uploadParams = uploadParamsBuilder
            .withRecipientPublicKey(document.documentAcc.publicKey)
            .withTransactionMosaics([])
            .withNemKeysPrivacy(privateKey, publicKey)
            .build();
        console.log(uploadParams);

        // call upload services
        try {
            return this.uploader.upload(uploadParams);
            // console.log('Result');
            // console.log(result);
        }
        catch (err) {
            console.log(err);
            throw new Error('Failed to upload file');
        }
    }

    /**
     * Download file from file hash
     * @param hash 
     */
    downloadFile(hash: string) {
        const downloadParamsBuilder = DownloadParameter.create(hash);
        console.log(downloadParamsBuilder);
        const downloadParams = downloadParamsBuilder.build();
        console.log(downloadParams);

        return this.dowloader.download(downloadParams);
    }

    /**
     * Download file by hash and convert to data URI
     * @param hash
     * @param isEncrypt
     * @param privateKey
     */
    async downloadFileToDataUri(hash: string, isEncrypt: boolean = false) {
        let downloadResult: DownloadResult
        try {
            if (isEncrypt) downloadResult = await this.downloadFileEncrypt(hash)
            else downloadResult = await this.downloadFile(hash)
        }
        catch (err) {
            throw err;
        }
        const buffer: Buffer = await downloadResult.data.getContentAsBuffer()
            .catch(err => {
                throw err;
            });
        const dataString = await downloadResult.data.getContentsAsString();
        // console.log('########### Data string ##############');
        // console.log(dataString);
        const data = buffer.toString('base64');
        const fileType = downloadResult.data.contentType;
        // console.log(buffer);
        // console.log(buffer.toJSON().data.toString());
        // console.log(data);
        return 'data:' + fileType + ';base64,' + encodeURI(data);
    }

    /**
     * Download file from file hash
     * @param hash 
     */
    downloadFileEncrypt(hash: string) {
        const privateKey = this.global.loggedAccount.privateKey;
        const publicKey = this.global.loggedAccount.publicKey;

        const downloadParamsBuilder = DownloadParameter.create(hash);
        console.log(downloadParamsBuilder);
        const downloadParams = downloadParamsBuilder.withNemKeysPrivacy(privateKey, publicKey).build();
        console.log(downloadParams);

        return this.dowloader.download(downloadParams);
    }

    /**
     * Upload signature image
     * @param signatureImg 
     */
    uploadSignature(signatureImg: string) {
        const fileContent = HelperService.convertDataURIToBinary(signatureImg);
        const userNameParts = this.global.loggedWallet.name.split('@');
        const fileName = userNameParts[0] + '-' + userNameParts[1] + '-signature.svg';
        const fileType = 'image/svg+xml';
        const fileDescription = 'SiriusSign signature image';

        const fileHash = CryptoJS.SHA256(signatureImg).toString();
        const metaData = new Map<string, string>([
            ['SFH', fileHash]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileContent,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        console.log(metaParams);

        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);

        const uploadParams = uploadParamsBuilder.withTransactionMosaics([]).build();
        console.log(uploadParams);

        return this.uploader.upload(uploadParams);
    }

    /**
     * Upload signature image
     * @param signatureImg 
     */
    uploadSignatureForDocSigning(signatureImg: string, documentPublicAccount: PublicAccount) {
        const fileContent = HelperService.convertDataURIToBinary(signatureImg);
        const userNameParts = this.global.loggedWallet.name.split('@');
        const fileName = userNameParts[0] + '-' + userNameParts[1] + '-signature.svg';
        const fileType = 'image/svg+xml';
        const fileDescription = 'SiriusSign signature image';

        const fileHash = CryptoJS.SHA256(signatureImg).toString();
        const metaData = new Map<string, string>([
            ['SFH', fileHash]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileContent,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        console.log(metaParams);

        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);

        const uploadParams = uploadParamsBuilder
            .withRecipientPublicKey(documentPublicAccount.publicKey)
            .withTransactionMosaics([])
            .build();
        console.log(uploadParams);

        return this.uploader.upload(uploadParams);
    }
}
