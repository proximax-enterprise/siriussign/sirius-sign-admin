import { Injectable } from '@angular/core';

import { QRCode, ErrorCorrectLevel, QR8BitByte } from 'qrcode-generator-ts/js'

import {
    SiriusSignMessage,
    SiriusMessageType,
    SsDocumentSigningMessage,
    SsNotifyDocumentCosignMessage,
    SsDocumentVerifyMessage,
    SsNotifyDocumentVerifyMessage
} from './../model/siriussign-message.model';

@Injectable({
    providedIn: 'root'
})
export class HelperService {

    constructor() { }

    /*
     * Get MM dd, YYYY from Date
     */
    public static dateToShortString(date: Date) {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        //return  date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
        return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    }

    /**
     * Calculate time from a date to now
     * @param date 
     */
    public static nowFromDate(date: Date) {
        let now = Date.now();
        let millis = now - date.getTime();
        let min = Math.floor(millis / 60000);
        if (min < 0) return '0 m ago';
        else if (min < 60) return min + 'm ago';
        else if (min < 1440) return Math.floor(min / 60) + 'h ' + min % 60 + 'm ago';
        else if (min < 10080) return Math.floor(min / 1440) + ' days ago';
        else return HelperService.dateToShortString(date);
    }

    /**
     * Parse message of transaction
     * @param message 
     */
    public static parseMessage(message: string): any {
        const errorResult = { appCodeName: '', header: '' };

        const fieldValue = message.split('|');

        if (fieldValue.length != 3) return errorResult;
        const info = {
            header: fieldValue[0],
            body: fieldValue[1],
            meta: fieldValue[2]
        }

        const header = info.header.split(',');
        const body = info.body.split(',');

        if (header.length != 3) return errorResult;
        const messageInfo = {
            appCodeName: header[0],
            appVersion: header[1],
            messageType: header[2]
        };

        if (messageInfo.messageType == '1') {
            if (body.length != 4) return errorResult;
            const documentInfo = {
                appCodeName: messageInfo.appCodeName,
                fileHash: body[0],
                name: body[1],
                uploadTxHash: body[2],
                isEncrypt: body[3],
                meta: info.meta
            };
            return documentInfo;
        }

        if (messageInfo.messageType == '2') {
            if (body.length != 2) return errorResult;
            const notification = {
                appCodeName: messageInfo.appCodeName,
                messageType: messageInfo.messageType,
                content: body[0],
                groupName: body[1]
            }
            return notification;
        }

        // If (invalid messageType)
        return errorResult;
    }

    /**
     * Parse message for new signing process
     * @param message 
     */
    public static parseSsMessage(message: string): SiriusSignMessage {
        const errorResult = null; // { appCodeName: '', header: '' };

        const fieldValue = message.split('|');
        if (fieldValue.length != 3) return errorResult;
        const info = {
            header: fieldValue[0],
            body: fieldValue[1],
            meta: fieldValue[2]
        }

        const header = info.header.split(',');
        if (header.length != 3) return errorResult;
        const messageInfo = {
            appCode: header[0],
            appVersion: header[1],
            messageType: header[2]
        };

        if (messageInfo.messageType == SiriusMessageType.SIGN.toString()) {
            try {
                return SsDocumentSigningMessage.createFromString(
                    messageInfo.appCode,
                    messageInfo.appVersion,
                    info.body,
                    info.meta
                );
            }
            catch (err) { return errorResult; }
        }
        if (messageInfo.messageType == SiriusMessageType.VERIFY.toString()) {
            try {
                return SsDocumentVerifyMessage.createFromString(
                    messageInfo.appCode,
                    messageInfo.appVersion,
                    info.body,
                    info.meta
                );
            }
            catch (err) { return errorResult; }
        }
        if (messageInfo.messageType == SiriusMessageType.SIGN_NOTIFY.toString()) {
            try {
                return SsNotifyDocumentCosignMessage.createFromString(
                    messageInfo.appCode,
                    messageInfo.appVersion,
                    info.body,
                    info.meta
                );
            }
            catch (err) { return errorResult; }
        }
        if (messageInfo.messageType == SiriusMessageType.VERYFY_NOTIFY.toString()) {
            try {
                return SsNotifyDocumentVerifyMessage.createFromString(
                    messageInfo.appCode,
                    messageInfo.appVersion,
                    info.body,
                    info.meta
                );
            }
            catch (err) { return errorResult; }
        }
        return errorResult;
    }

    /**
     * Get string from number of bytes
     * @param size 
     */
    public static sizeToString(size: number) {
        if (size == 0) return 'Unknown';
        if (size < 1024) return size + ' bytes';
        else if (size < 1024 ** 2) return (size / 1024).toFixed(2) + ' KB';
        else if (size < 1024 ** 3) return (size / (1024 ** 2)).toFixed(2) + ' MB';
        else if (size < 1024 ** 4) return (size / (1024 ** 3)).toFixed(2) + ' GB';
        else return 'Too large';
    }

    /**
     * Get array of intersection elements from 2 arrays
     * @param arr1 
     * @param arr2 
     */
    public static intersecArray(arr1: any[], arr2: any[]) {
        return arr1.filter(element1 => arr2.includes(element1));
    }

    /**
     * Convert data URI to Base64 Unit8Array
     * @param dataURI 
     */
    public static convertDataURIToBinary(dataURI: string) {
        const BASE64_MARKER = ';base64,';
        const base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        const base64 = dataURI.substring(base64Index);
        const raw = window.atob(base64);
        const rawLength = raw.length;
        const array = new Uint8Array(new ArrayBuffer(rawLength));

        for (let i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    }

    /**
     * Convert base64 string to hex string
     * @param base64 
     */
    public static base64toHex(base64: string) {
        let raw = atob(base64);
        let hex = '';
        for (let i = 0; i < raw.length; i++) {
            let _hex = raw.charCodeAt(i).toString(16)
            hex += (_hex.length == 2 ? _hex : '0' + _hex);
        }
        return hex;
    }

    /**
     * Convert hex string to base64 string
     * @param hex 
     */
    public static hexToBase64(hex: string) {
        return btoa(String.fromCharCode.apply(null,
            hex.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
        );
    }

    /**
     * Decode base 64 string to UTF-8
     * @param base64
     */
    public static base64toUtf8(base64: string) {
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(base64).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    public static utf8toBase64(str: string) {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode(('0x' + p1) as any);
            }));
    }

    /**
     * Asynchronous forEach
     * @param array 
     * @param callback 
     */
    public static async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    /**
     * Get QR code of a string
     * @param str 
     * @param typeNumber 
     */
    public static getQr(str: string, typeNumber: number) {
        let qr = new QRCode();
        qr.setTypeNumber(typeNumber);
        qr.setErrorCorrectLevel(ErrorCorrectLevel.L);
        qr.addData(new QR8BitByte(str));
        qr.make();
        return qr.toDataURL(typeNumber);
    }

    /**
     * Get signature image in png base64 form svg base64
     */
    public static getPngSignature(svgSignature: string) {
        const dpr = window.devicePixelRatio || 1;

        let signatureImgObj: HTMLImageElement = document.createElement('img');
        signatureImgObj.src = svgSignature;
        signatureImgObj.width = 376;
        signatureImgObj.height = 176;

        const imgCanvas = <HTMLCanvasElement>document.createElement('canvas');
        imgCanvas.width = 376;
        imgCanvas.height = 176;

        let ctx = imgCanvas.getContext('2d');
        return new Promise<string>(resolve => {
            setTimeout(() => {
                ctx.drawImage(signatureImgObj, 0, 0, signatureImgObj.width, signatureImgObj.height);
                const pngDataUrl = imgCanvas.toDataURL('image/png');
                resolve(pngDataUrl);
            }, 100)
        })
    }

    /**
     * Convert ArrayBuffer (returned by File plugin) to Buffer (returned by xipfsDownloader)
     */
    public static convertArrayBufferToBuffer(arrBuf: ArrayBuffer) {
        var isArrayBufferSupported = (new Buffer(new Uint8Array([1]).buffer)[0] === 1);

        if (isArrayBufferSupported)
            return new Buffer(arrBuf);
        else {
            var buffer = new Buffer(arrBuf.byteLength);
            var view = new Uint8Array(arrBuf);
            for (var i = 0; i < buffer.length; ++i) {
                buffer[i] = view[i];
            }
            return buffer;
        }
    }
}
