import { Injectable } from '@angular/core';
import { CanActivate, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { GlobalService } from './../global.service';
import { MultitaskService } from './../multitask.service'
@Injectable({
    providedIn: 'root'
})
export class ProgressGuardService implements CanActivate {

    constructor(private global: GlobalService) { }

    /**
     * Use to prevent back button pressing navigates to progress page
     * @returns
     */
    canActivate() {
        return !this.global.getIsProgressDone();
    }
}

export interface CanComponentDeactivate {
    canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
    providedIn: 'root'
})
export class ProgressGuardCanDeactiveService implements CanDeactivate<CanComponentDeactivate> {

    constructor(private global: GlobalService) { }

    canDeactivate(component: CanComponentDeactivate) {
        return this.global.getIsProgressDone();
    }
}

@Injectable({
    providedIn: 'root'
})
export class NewDoneGuardService implements CanActivate {

    constructor(private multitask: MultitaskService) { }

    canActivate() {
        return this.multitask.selectedTask != null;
    }
}