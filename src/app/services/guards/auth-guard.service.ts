import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { GlobalService } from './../global.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(private global: GlobalService, private router: Router) { }

    /**
     * Return route permission according to log in state
     */
    canActivate() {
        // let notLoggedInPage = ['sign-in', 'sign-up', 'code-verify'];
        // let onNotLoggedIn = notLoggedInPage.includes(this.router.url)
        // if (onNotLoggedIn)
        return !this.global.getIsLoggedIn();
        // else {
        //   return this.global.getIsLoggedIn();
        // }
    }
}
