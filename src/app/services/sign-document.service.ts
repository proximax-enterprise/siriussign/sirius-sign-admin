import { Injectable } from '@angular/core';
import { ChooserResult } from '@ionic-native/chooser/ngx'
import * as CryptoJS from 'crypto-js';
import { PublicAccount, Account } from 'tsjs-xpx-chain-sdk';

/**
 * Interface of the document that import to be signed
 * @interface
 */
export interface SignDocument {
    file: ChooserResult,
    size: number,
    signDate: Date,
    cosigners: string[],
    verifiers: string[],
    multisigAcc: PublicAccount,     //phase 1
    documentAcc: Account,     //phase 2
    fileHash: string,
    signTxHash: string,
    uploadTxHash: string,
    isEncrypt: boolean,
    signatureUploadTxHash: string,
    status: string
}

@Injectable({
    providedIn: 'root'
})

export class SignDocumentService {

    document: SignDocument = {
        file: {
            data: new Uint8Array(0),
            dataURI: '',
            mediaType: '',
            name: '',
            uri: ''
        },
        size: 0,
        signDate: new Date(),
        cosigners: [],
        verifiers: [],
        multisigAcc: null,
        documentAcc: null,
        fileHash: '',
        signTxHash: '',
        uploadTxHash: '',
        isEncrypt: false,
        signatureUploadTxHash: '',
        status: ''
    };

    constructor() { }

    /**
     * Set document for sign process
     * @param {ChooserResult} file
     */
    setDocument(file: ChooserResult, size: number = 0) {
        this.document.file = file;
        this.document.size = size;
        this.document.signDate = new Date(Date.now());
        this.document.cosigners = [];
        this.document.verifiers = [];
        this.document.multisigAcc = null;
        this.document.documentAcc = null;
        this.document.fileHash = this.hashFile('SHA256');
        this.document.signTxHash = '';
        this.document.uploadTxHash = '';
        this.document.isEncrypt = false;
        this.document.signatureUploadTxHash = '';
        this.document.status = '';
    }

    /**
     * Get the document in sign process
     * @returns
     */
    getDocmument() {
        return this.document;
    }

    /**
     * Get document name
     * @returns
     */
    getDocName() {
        return this.document.file.name;
    }

    /**
     * Get document upload date
     * @returns
     */
    getDocDate() {
        return this.document.signDate;
    }

    /**
     * Generate file hash
     * @param method 
     */
    hashFile(method: string) {
        const data = this.document.file.dataURI;
        // const data = CryptoJS.enc.Base64.parse(this.document.file.dataURI);
        switch (method) {
            case 'SHA256': return CryptoJS.SHA256(data).toString();
            case 'MD5': return CryptoJS.MD5(data).toString();
            case 'SHA3': return CryptoJS.SHA3(data).toString();
            default: return CryptoJS.SHA256(data).toString();
        }
    }
}
