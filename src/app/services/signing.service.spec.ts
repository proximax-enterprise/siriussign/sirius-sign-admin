import { TestBed, async } from '@angular/core/testing';

import { SigningService } from './signing.service';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';
import { GlobalService } from './global.service';
import { MonitorService } from './monitor.service';
import { SiriusDocument } from '../model/sirius-document.model';
import { Account, NetworkType, Address, SignedTransaction, PublicAccount, AggregateTransaction, TransactionType, Deadline, AggregateTransactionCosignature } from 'tsjs-xpx-chain-sdk';
import { serializePath } from '@angular/router/src/url_tree';


// Mock class
class MockMonitorService {
    // Listen hash lock and announce aggregate bonded transaction
    listenTransaction(sender, hash, fAnounce, fConfirm, fError) {}
    listenAggregateBondedTransaction(address, f1, f2) {}
}

describe('SigningService', () => {
    let service: SigningService;
    let global:GlobalService;
    let account:Account;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                IonicStorageModule.forRoot()
            ],
            providers: [
                Storage,
                Device,
                GlobalService,
                { provide: MonitorService, useClass: MockMonitorService }
            ]
        })
        service = TestBed.get(SigningService);
        global = TestBed.get(GlobalService);
        account = Account.createFromPrivateKey('EC994CC2F78603D554809FC06CC587357CDE40E6577B4B069E881ECA6D901E04', NetworkType.TEST_NET);
        global.setLoggedAccount(account);
        global.notarizationAddress = Address.createFromRawAddress('SCQALPC7AQHA6OEDCSOF45HEMLKIKFTSFISN2LVG');
        global.mosaicId  = global.namespaceId;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('Sign document without cosignatures', ()=>{
        const doc = {
            fileHash: 'filehash',
            uploadTxHash: 'uploadtxhash',
            isEncrypt: false,
            file: {
                name: 'filetest'
            }
        };
        
        const result = service.signOnlyMe(doc);
        expect(result).toBeDefined();
        expect(result.hash).toBeDefined();

    });


    it('Create multisig account', ()=>{
        const cosignerPublicKeys: string[] = ['5E5F8CCEB8A6C411F121D335B775EED3621693FE7EFE4B103D54E111126B5450'];
        const name = 'TESTGROUP';

        const result = service.createMultisigAccount(cosignerPublicKeys, name);

        expect(result).toBeDefined()

    });


    it('Sign docment with cosignatures', async ()=>{
        const doc = {
            fileHash: 'filehash',
            uploadTxHash: 'uploadtxhash',
            isEncrypt: false,
            file: {
                name: 'filetest'
            }
        };

        const multisigAccountPublicKey = '8318DCD082263D43D1D78E80DA06E39124D6034A12861E6B2FDD2AC4245D5CDA';
        const multisigAcc:PublicAccount  = PublicAccount.createFromPublicKey(multisigAccountPublicKey, NetworkType.TEST_NET);

        const result = await service.signWithOthers(doc, multisigAcc);

        expect(result).toBeDefined()

        
    });


    it('Fetch aggregate bonded transactions', async ()=>{
        const acc:PublicAccount = PublicAccount.createFromPublicKey('1225C50B9E939659E7B63298461AC95786A67F74F41CAD84F6AD9F24C9CB1BCB', NetworkType.TEST_NET);
        let result;
        const fetch = await service.fetchAggregateBondedTransactions(acc, null, (transactions)=>{
            result = transactions;
        });

        expect(result).toBeDefined()
    });

    





});
