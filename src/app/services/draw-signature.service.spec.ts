import { TestBed } from '@angular/core/testing';

import { DrawSignatureService } from './draw-signature.service';

describe('DrawSignatureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DrawSignatureService = TestBed.get(DrawSignatureService);
    expect(service).toBeTruthy();
  });
});
