import { Injectable } from '@angular/core';
import {
  TransferTransaction,
  Deadline,
  NetworkType,
  PlainMessage,
  UInt64,
  AddressAliasTransaction,
  RegisterNamespaceTransaction,
  NamespaceHttp,
  NamespaceId,
  AliasActionType,
  AccountHttp,
  Order,
  AggregateTransaction,
  NetworkCurrencyMosaic,
  Mosaic,
  MosaicId,
  SimpleWallet, Listener, Address, Account, PublicAccount, Transaction, TransactionHttp } from 'tsjs-xpx-chain-sdk';
import { GlobalService } from '../services/global.service';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {
  loggedAccount: string;
  queryKeyNamespace: string; // use to get Acc link to namespace
  queryKeySubNamespace: string; // use to get Acc link to namespace
  queryKeydoc: string; // get doc
  queryKeyPriv: string = 'priv'; // use to get Acc link to namespace
  accChoosed: Account; // acc link to namespace choosed
  accLinkToNamespace: any[] = [];
  mosaicId: MosaicId;
  maxFee = UInt64.fromUint(0);
  generationHash = '56D112C98F7A7E34D1AEDC4BD01BC06CA2276DD546A93E36690B785E82439CA9';
  namespaceName: string = 'tri6';
  subnamespacename: string = 'go';
  namespaceNames: string[] = ['a', 'b'];
  isCreatingPro: boolean = false;
  isCreatingOr: boolean = false;
  isSignatories: boolean = false;
  isDocument: boolean = false;
  isNotify: boolean = false;
  isDone: boolean = false; //verify organization is done?
  organizationManage = {
    signatories: [],
    namespaceNames: this.namespaceNames,
    documents: [],
  };
  constructor(
    private global: GlobalService,
    private storage: Storage
  ) { }

  setQuerykeyNamespace() { // Need to add to home init
    this.loggedAccount = this.global.loggedWallet.name;
    this.queryKeyNamespace = this.loggedAccount;
  }

  setQuerykeySubnamespace(name: string) {
    this.queryKeySubNamespace = this.queryKeyNamespace + '-' + name;
  }

  setQuerykeyDoc(name: string) {
    this.loggedAccount = this.global.loggedWallet.name;
    this.queryKeydoc = this.queryKeySubNamespace + '-' + name;
  }

  public setNamspaceName(namespaceName: string) {
    this.namespaceName = namespaceName;
  }
  public getNamespaceName() {
      return this.namespaceName;
  }
  checknameSpace() {
    const namespaceHttp = new NamespaceHttp('https://bctestnet2.brimstone.xpxsirius.io');
    const namespace = new NamespaceId('foo');
    namespaceHttp
      .getNamespace(namespace)
      .subscribe(namespace => console.log(namespace), err => console.log(err)
      );
  }

  createNamespace(privateKey: string) {
    const duration = UInt64.fromUint(100);
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    const registerNamespaceTransaction = RegisterNamespaceTransaction.createRootNamespace(
      Deadline.create(),
      this.namespaceName,
      duration,
      NetworkType.TEST_NET,
      this.maxFee);
    const signedTransaction = account.sign(registerNamespaceTransaction, this.generationHash);
    return(signedTransaction);
  }

  linkAccountToNamespace(privateKey: string) {
    const namespaceId = new NamespaceId(this.namespaceName);
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    const addressAliasTransaction = AddressAliasTransaction.create(
      Deadline.create(),
      AliasActionType.Link,
      namespaceId,
      account.address,
      NetworkType.TEST_NET,
    );
    console.log(addressAliasTransaction);
    const signedTransaction = account.sign(addressAliasTransaction, this.generationHash);
    return (signedTransaction);
  }

  sendXpxToNamespace(senderAcc, recipientAddress) {
    const amount = UInt64.fromUint(1);
    const namespaceId = new NamespaceId('prx.xpx');
    const mosaic = new Mosaic(namespaceId, amount);
    const transferTransaction = TransferTransaction.create(
      Deadline.create(),
      recipientAddress,
      [mosaic],
      PlainMessage.create('Welcome To Sirius Chain'),
      NetworkType.TEST_NET,
      this.maxFee);
    const account = Account.createFromPrivateKey(senderAcc.privateKey, NetworkType.TEST_NET);
    const signedTransaction = account.sign(transferTransaction, this.generationHash);
    return (signedTransaction);
  }

  agreegateTx(loggedAcc, recipientAddress) {
    // create namespace
    const duration = UInt64.fromUint(100);
    const account = Account.createFromPrivateKey(loggedAcc.privateKey, NetworkType.TEST_NET);
    const registerNamespaceTransaction = RegisterNamespaceTransaction.createRootNamespace(
      Deadline.create(),
      this.namespaceName,
      duration,
      NetworkType.TEST_NET,
      this.maxFee).toAggregate(loggedAcc.publicAccount);
    // send xpx to Acc Namespace
    const amount = UInt64.fromUint(1);
    const namespaceId = new NamespaceId('prx.xpx');
    const mosaic = new Mosaic(namespaceId, amount);
    const transferTransaction = TransferTransaction.create(
      Deadline.create(),
      recipientAddress,
      [mosaic],
      PlainMessage.create('Welcome To Sirius Chain'),
      NetworkType.TEST_NET,
      this.maxFee).toAggregate(loggedAcc.publicAccount);
    const aggregateTransaction = AggregateTransaction.createComplete(
      Deadline.create(),
      [
        registerNamespaceTransaction,
        transferTransaction
          // aliceTransferTransaction.toAggregate(danAccount.publicAccount),
          // bobTransferTransaction.toAggregate(danAccount.publicAccount)
      ],
      NetworkType.TEST_NET,
      [],
      this.maxFee
    );
  }

  async transferTxToNamespace() {
    const namespaceId = new NamespaceId('fdstest');
    const sender = Account.createFromPrivateKey('B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433', NetworkType.TEST_NET);

    const transferTransaction = TransferTransaction.create(
      Deadline.create(),
      namespaceId,
      [],
      PlainMessage.create('Test'),
      NetworkType.TEST_NET,
      UInt64.fromUint(0));

    const signedTransaction = sender.sign(transferTransaction, this.generationHash);
  }
  // get namespace of an owner create
  getNamespaceMulti() {
    const namespaceHttp = new NamespaceHttp(this.global.apiNode);
    const sender = Account.createFromPrivateKey('AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1', NetworkType.TEST_NET);
    const getReadableNamesForSetOfNamespaces = () => {
      namespaceHttp.getNamespacesFromAccount(sender.address).subscribe(namespaceInfos => {
          namespaceHttp.getNamespacesName(namespaceInfos.map(namespaceInfo => namespaceInfo.id))
          .subscribe(namespaceNames => {
              console.log(namespaceNames);
          }, error => {
              console.error(error);
          }, () => {
              console.log('done.');
          });
      }, error => {
          console.error(error);
      }, () => {
      });
    };
    getReadableNamesForSetOfNamespaces();
  }

  getAddressLinktoNamespace() {
    const namespaceHttp = new NamespaceHttp(this.global.apiNode);
    const getNamespaceInformation = () => {
      const namespaceId = new NamespaceId('foo');
      namespaceHttp.getLinkedAddress(namespaceId)
      .subscribe(namespaceInfo => {
          console.log(namespaceInfo.pretty());
          }, error => {
              console.error(error);
          }, () => {
              console.log('done.');
          });
    };
    getNamespaceInformation();
  }

  async filterRegisterNamespace(privateKey: string) {
    const registerNamespaceTx: RegisterNamespaceTransaction[] = (await this.getAllTxFromAcc(privateKey)
      .then( tx => tx.filter( tx => tx.type == 16718).map(tx => tx as RegisterNamespaceTransaction))
      .catch((e) => { console.log('e'); return []; }));
    const namespaceName: string[] = registerNamespaceTx.map(tx => tx.namespaceName);
    console.log(namespaceName);
    return(namespaceName);
  }

  async filterTransferTx(privateKey: string) {
    const transferTxFromNamespace: TransferTransaction[] = (await this.getAllTxFromAcc(privateKey)
      .then( tx => tx.filter( tx => tx.type == 16724).map(tx => tx as TransferTransaction))
      .catch((e) => { console.log('e'); return []; }));
    const address: Address[] = transferTxFromNamespace.map(add => add.recipient as Address );
    console.log(address);
    return(address);
  }

  getAllTxFromAcc(privateKey: string) {
    const accountHttp = new AccountHttp(this.global.apiNode);
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    console.log(account);
    return accountHttp.transactions(account.publicAccount, {
        pageSize: 100,
        order: Order.DESC
    }).toPromise();
  }

  getConfirmedOutgoingTransactions(privateKey: string) {
    const accountHttp = new AccountHttp(this.global.apiNode);
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    return accountHttp.outgoingTransactions(account.publicAccount, {
      pageSize: 10,
      order: Order.DESC
    }).subscribe(tx => {
        console.log(tx);
    }, error => {
        console.error(error);
    }, () => {
        console.log('done.');
    });
  }

  async storeNamespace(name: string) {
    const namespaceNames: string[] = await this.storage.get(this.queryKeyNamespace)
      .catch(err => {
      return null;
    });
    console.log(namespaceNames);
    if (!namespaceNames) { return this.storage.set(this.queryKeyNamespace, [name]); }
    let addedNamespaces = [...namespaceNames, name];
    const sortCondition = (a: string, b: string) => {
        if (a > b) { return -1; }
        else { return 1; }
    };
    addedNamespaces = addedNamespaces.sort((a, b) => sortCondition(a, b));
    return this.storage.set(this.queryKeyNamespace, addedNamespaces);
  }

  async storeSubNamespace(name: string) {
    const namespaceNames: string[] = await this.storage.get(this.queryKeySubNamespace)
      .catch(err => {
      return null;
    });
    console.log(namespaceNames);
    if (!namespaceNames) { return this.storage.set(this.queryKeySubNamespace, [name]); }
    let addedNamespaces = [...namespaceNames, name];
    const sortCondition = (a: string, b: string) => {
        if (a > b) { return -1; }
        else { return 1; }
    };
    addedNamespaces = addedNamespaces.sort((a, b) => sortCondition(a, b));
    return this.storage.set(this.queryKeySubNamespace, addedNamespaces);
  }

  async storePriv(account: Account) {
    console.log(account);
    const privateKeys: string[] = await this.storage.get(this.queryKeyPriv)
      .catch(err => {
      return null;
    });
    console.log(privateKeys);

    if (!privateKeys) { return this.storage.set(this.queryKeyPriv, [account.privateKey]); }
    let addedPrivateKeys = [...privateKeys, account.privateKey];
    const sortCondition = (a: string, b: string) => {
        if (a > b) { return -1; }
        else { return 1; }
    };
    addedPrivateKeys = addedPrivateKeys.sort((a, b) => sortCondition(a, b));
    return this.storage.set(this.queryKeyPriv, addedPrivateKeys);
  }

  async fetchPrivateKey() {
    const privateKeys: string[] = await this.storage.get(this.queryKeyPriv)
        .catch(err => {
            return [];
        });
    if (!privateKeys) { return []; }
    console.log(privateKeys);
    return privateKeys;
  }

  async fetchNamespace() {
    const namespaceName: string[] = await this.storage.get(this.queryKeyNamespace)
        .catch(err => {
            return [];
        });
    if (!namespaceName) { return []; }
    return namespaceName;
  }

  async fetchSubNamespace() {
    const namespaceName: string[] = await this.storage.get(this.queryKeySubNamespace)
        .catch(err => {
            return [];
        });
    if (!namespaceName) { return []; }
    return namespaceName;
  }

  async fetchAccLinkNamespace(name: string) {
    const privateKeys: string[] = await this.fetchPrivateKey();
    console.log(privateKeys);
    const namespaceHttp = new NamespaceHttp(this.global.apiNode);
    const namespaceId = new NamespaceId(name);
    console.log(namespaceId);
    namespaceHttp.getLinkedAddress(namespaceId)
      .subscribe(namespaceInfo => {
        const account = (privateKeys.map((privateKey) => {
          return Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
        })).filter( acc => acc.address.plain() === namespaceInfo.plain());
        // this.accChoosed = account[0];
        console.log(this.accChoosed);
        console.log(account[0].privateKey);
      }, error => {
        console.error(error);
      }, () => {
        console.log('done.');
      });
  }
}
