import { TestBed } from '@angular/core/testing';

import { MonitorService } from './monitor.service';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';
import { GlobalService } from './global.service';
import { Account, NetworkType } from 'tsjs-xpx-chain-sdk';


describe('MonitorService', () => {
  let service: MonitorService;
  let global:GlobalService;
  let account:Account;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot()
      ],
      providers: [
          Storage,
          Device,
          GlobalService
      ]
    })
    service = TestBed.get(MonitorService)
    global = TestBed.get(GlobalService);
    account = Account.createFromPrivateKey('EC994CC2F78603D554809FC06CC587357CDE40E6577B4B069E881ECA6D901E04', NetworkType.TEST_NET);
    global.setLoggedAccount(account);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


 
});
