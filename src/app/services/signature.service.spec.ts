import { TestBed } from '@angular/core/testing';

import { SignatureService } from './signature.service';
import { IonicStorageModule } from '@ionic/storage';
import { GlobalService } from './global.service';
import { Device } from '@ionic-native/device/ngx';

describe('SignatureService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      IonicStorageModule.forRoot()
    ],
    providers: [
        Storage,
        GlobalService,
        Device
    ]
  }));

  it('should be created', () => {
    const service: SignatureService = TestBed.get(SignatureService);
    expect(service).toBeTruthy();
  });
});
