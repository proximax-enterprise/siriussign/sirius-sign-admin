import { TestBed, async } from '@angular/core/testing';

import { HelperService } from './helper.service';
import { asyncScheduler } from 'rxjs';

describe('HelperService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    describe('init', ()=>{
        it('should be created', () => {
            const service: HelperService = TestBed.get(HelperService);
            expect(service).toBeTruthy();
        });
    })

    

    describe('calculate time from a date to now', ()=>{
        beforeAll(()=>{
            // const service: HelperService = TestBed.get(HelperService);
        })

        it('set new date and start caculating',async(async (
            ()=>{
                var d = new Date();
                // should run
                let result = HelperService.nowFromDate(d);
                expect(result).toBeTruthy();
                // 1 hour ago
                d.setHours(d.getHours() - 1);
                result = HelperService.nowFromDate(d);
                expect(result == '1h 0m ago').toBeTruthy()
                // 6 days 1 hour ago
                d.setDate(d.getDate() - 6);
                result = HelperService.nowFromDate(d);
                expect(result).toEqual('6 days ago')
                // 20 days 1 hour ago (meaningless) => not check
            }
        )))


    })


    describe('Convert data URI to Base64 Unit8Array', ()=>{
        beforeAll(()=>{
            
        })

        it('try to convert', async(async ()=>{
            const dataURI:string = `data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==`
            const result:Uint8Array = HelperService.convertDataURIToBinary(dataURI);

            const resultExpected = [137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 5, 0, 0, 0, 5, 8, 6, 0, 0, 0, 141, 111, 38, 229, 0, 0, 0, 28, 73, 68, 65, 84, 8, 215, 99, 248, 255, 255, 63, 195, 127, 6, 32, 5, 195, 32, 18, 132, 208, 49, 241, 130, 88, 205, 4, 0, 14, 245, 53, 203, 209, 142, 14, 31, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130];

            expect(Array.from(result)).toEqual(resultExpected);
        }))
    })

    describe('Convert base64 string to hex string', ()=>{
        it('try to convert', async( async()=>{
            const base64:string = 'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';
            const result:string = HelperService.base64toHex(base64);

            const resultExpected:string = '89504e470d0a1a0a0000000d49484452000000050000000508060000008d6f26e50000001c4944415408d763f8ffff3fc37f062005c3201284d031f18258cd04000ef535cbd18e0e1f0000000049454e44ae426082';

            expect(result).toEqual(resultExpected);
        }))
    })


});
