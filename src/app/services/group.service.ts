import { GroupElement, SiriusGroups } from './group.service';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalService } from './global.service';

import {
    TransactionHttp,
    AggregateTransaction,
    ModifyMultisigAccountTransaction,
    AccountHttp,
    PublicAccount
} from 'tsjs-xpx-chain-sdk';

import { SigningService } from './signing.service';
import { HelperService } from './helper.service';
import { GroupShortInfo } from './backup.service';

export interface SiriusGroups {
    name: string,
    creator: string,
    isActive: boolean,
    numOfAccept: number,
    multisigAcc: any,
    members: string[],
    transaction: AggregateTransaction,
}

export interface GroupElement {
    name: string,
    multisigAcc: PublicAccount,
    creationTxHash: string,
    creationDate: number,
    creator: string,
    members: string[]
}

interface GroupsArrayElement {
    owner: string,
    groups: GroupElement[]
}

@Injectable({
    providedIn: 'root'
})
export class GroupService {

    groups: SiriusGroups[] = [];

    selectedGroup: SiriusGroups;

    mode = 0;

    waitingGroups: SiriusGroups[] = [];

    isInvite = false;

    constructor(private storage: Storage, private global: GlobalService, private signing: SigningService) { }

    /**
     * Store a group to storage
     * @param group 
     */
    async storeGroup(group: GroupElement) {
        const owner = this.global.loggedWallet.name;
        const groupsArr: GroupsArrayElement[] = await this.storage.get('groupsArr')
            .catch(async (err) => {
                console.log(err);
                const newGroupsElement = {
                    owner: owner,
                    groups: [group]
                }
                await this.storage.set('groupsArr', [newGroupsElement]);
            });

        if (groupsArr) {
            let groupsArray = groupsArr;
            const selectedGroupArray = groupsArr.filter(groupsElement => groupsElement.owner === owner);
            if (selectedGroupArray.length > 0) {
                let selectedGroupsElement = selectedGroupArray[0];
                const selectedIndex = groupsArr.indexOf(selectedGroupsElement);
                if (selectedIndex != -1) {
                    let groups = selectedGroupsElement.groups;
                    let isExist = groups.includes(group);
                    if (!isExist) {
                        groups.push(group);
                        selectedGroupsElement.groups = groups;
                        groupsArray[selectedIndex] = selectedGroupsElement;
                        await this.storage.set('groupsArr', groupsArray);
                    }
                    else {
                        throw new Error('Group already exists.');
                    }
                }
                else {
                    const newGroupsElement: GroupsArrayElement = {
                        owner: owner,
                        groups: [group]
                    }
                    groupsArray.push(newGroupsElement);
                    await this.storage.set('groupsArr', groupsArray);
                }
            }
        }
        else {
            const newGroupsElement: GroupsArrayElement = {
                owner: owner,
                groups: [group]
            }
            await this.storage.set('groupsArr', [newGroupsElement]);
        }
    }

    /**
     * Get full group info to restore
     * @param groupsShortInfo 
     */
    fetchGroupInfo(account: PublicAccount, groupsShortInfo: GroupShortInfo[]) {
        let groups: GroupElement[] = [];
        const txHashes = groupsShortInfo.map(group => group.creationTxHash);
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        const accountHttp = new AccountHttp(this.global.apiNode);
        return new Promise<GroupElement[]>((resolve, reject) => {
            transactionHttp.getTransactionsStatuses(txHashes)
                .subscribe(statuses => {
                    let validGroupsStatuses = [];
                    const validGroups = statuses.map((status, index) => {
                        if (status.group != 'failed') {
                            validGroupsStatuses.push(status.group);
                            return groupsShortInfo[index];
                        }
                    });
                    console.log(validGroups);
                    console.log(validGroupsStatuses);
                    validGroupsStatuses.forEach((status, index) => {
                        if (status == 'confirmed') {
                            transactionHttp.getTransaction(validGroups[index].creationTxHash)
                                .subscribe(tx => {
                                    const aggTx = tx as AggregateTransaction;
                                    const modMultisigTx = aggTx.innerTransactions[0] as ModifyMultisigAccountTransaction;
                                    const cosigners = modMultisigTx.modifications.map(mod => {
                                        return mod.cosignatoryPublicAccount.publicKey;
                                    });
                                    const group: GroupElement = {
                                        name: validGroups[index].name,
                                        multisigAcc: aggTx.innerTransactions[0].signer,
                                        creationTxHash: validGroups[index].creationTxHash,
                                        creationDate: new Date(aggTx.deadline.value.plusHours(-23).toString()).getTime(),
                                        creator: aggTx.signer.publicKey,
                                        members: cosigners
                                    }
                                    groups.push(group);
                                    if (index == validGroupsStatuses.length - 1) resolve(groups);
                                }, err => reject(err));
                        }

                        if (status == 'unconfirmed') {
                            accountHttp.unconfirmedTransactions(account)
                                .subscribe(txs => {
                                    const txsHashes = txs.map(tx => tx.transactionInfo.hash);
                                    const txIndex = txsHashes.indexOf(validGroups[index].creationTxHash);
                                    if (txIndex >= 0) {
                                        const aggTx = txs[txIndex] as AggregateTransaction;
                                        const modMultisigTx = aggTx.innerTransactions[0] as ModifyMultisigAccountTransaction;
                                        const cosigners = modMultisigTx.modifications.map(mod => {
                                            return mod.cosignatoryPublicAccount.publicKey;
                                        });
                                        const group: GroupElement = {
                                            name: validGroups[index].name,
                                            multisigAcc: aggTx.innerTransactions[0].signer,
                                            creationTxHash: validGroups[index].creationTxHash,
                                            creationDate: new Date(aggTx.deadline.value.plusHours(-23).toString()).getTime(),
                                            creator: aggTx.signer.publicKey,
                                            members: cosigners
                                        }
                                        groups.push(group);
                                        if (index == validGroupsStatuses.length - 1) resolve(groups);
                                    }
                                }, err => reject(err));
                        }

                        if (status == 'partial') {
                            accountHttp.aggregateBondedTransactions(account)
                                .subscribe(txs => {
                                    const txsHashes = txs.map(tx => tx.transactionInfo.hash);
                                    const txIndex = txsHashes.indexOf(validGroups[index].creationTxHash);
                                    if (txIndex >= 0) {
                                        const aggTx = txs[txIndex] as AggregateTransaction;
                                        const modMultisigTx = aggTx.innerTransactions[0] as ModifyMultisigAccountTransaction;
                                        const cosigners = modMultisigTx.modifications.map(mod => {
                                            return mod.cosignatoryPublicAccount.publicKey;
                                        });
                                        const group: GroupElement = {
                                            name: validGroups[index].name,
                                            multisigAcc: aggTx.innerTransactions[0].signer,
                                            creationTxHash: validGroups[index].creationTxHash,
                                            creationDate: new Date(aggTx.deadline.value.plusHours(-23).toString()).getTime(),
                                            creator: aggTx.signer.publicKey,
                                            members: cosigners
                                        }
                                        groups.push(group);
                                        if (index == validGroupsStatuses.length - 1) resolve(groups);
                                    }
                                }, err => reject(err));
                        }
                    })
                }, err => reject(err));
        });
    }

    /**
     * Restore backuped groups
     */
    async restore(owner: string, groups: GroupElement[]) {
        const newGroupsElement = {
            owner: owner,
            groups: groups
        }
        console.log(newGroupsElement);
        return new Promise(resolve => {
            this.storage.get('groupsArr')
                .then(async (groupsArr) => {
                    let groupsArray = groupsArr;
                    groupsArray.push(newGroupsElement);
                    await this.storage.set('groupsArr', groupsArray);
                    resolve();
                })
                .catch(async (err) => {
                    console.log(err);
                    await this.storage.set('groupsArr', [newGroupsElement]);
                    resolve();
                });
        });
    }

    async fetchAndRestore(owner, account, groupsShortInfo) {
        const groups = await this.fetchGroupInfo(account, groupsShortInfo)
            .catch(err => { throw new Error(err); });
        console.log(groups);
        await this.restore(owner, groups);
    }

    /**
     * Fetch created groups of logged in account
     */
    async fetchGroups() {
        const owner = this.global.loggedWallet.name;
        let groupsArr: GroupsArrayElement[];
        try {
            groupsArr = await this.storage.get('groupsArr');
            const selectedGroupsElement = groupsArr.filter(groupsElement => groupsElement.owner === owner);
            return selectedGroupsElement[0].groups;
        }
        catch {
            console.log('No group found');
            return [];
        }
    }

    /**
     * Fetch gruops from storage and fetch their status from network
     */
    async setGroups(fn?) {
        const groups: GroupElement[] = await this.fetchGroups();
        this.groups = groups.map(group => {
            const sGroup: SiriusGroups = {
                name: group.name,
                creator: group.creator,
                isActive: false,
                numOfAccept: 1,
                multisigAcc: group.multisigAcc,
                members: group.members,
                transaction: null,
            }
            return sGroup;
        })
        this.groups.forEach((groupElement, index) => {
            groupElement.isActive = false;
            groupElement.numOfAccept = 1;
            if (fn) fn(groupElement);
        });
    }

    /**
     * Remove a group
     * @param name
     */
    async removeGroup(name) {
        const owner = this.global.loggedWallet.name;

        let groupsArr = await this.storage.get('groupsArr');
        let selectedGroupsElement = groupsArr.filter(groupsElement => groupsElement.owner === owner)[0];
        const selectedIndex = groupsArr.indexOf(selectedGroupsElement);
        const groups = selectedGroupsElement.groups;
        const modifiedGroups = groups.filter(group => group.name != name);
        selectedGroupsElement.groups = modifiedGroups;
        groupsArr[selectedIndex] = selectedGroupsElement;

        return new Promise(async resolve => {
            await this.storage.set('groupsArr', groupsArr);
            resolve();
        });
    }

    /**
     * 
     */
    fetchJoinGroup() {
        this.waitingGroups = [];
        let lastId = null;
        let isLast = false;
        let loadAll = async () => {
            if (!isLast) {
                try {
                    await this.signing.fetchAggregateBondedTransactions(this.global.loggedAccount, lastId, transactions => {
                        let last;
                        if (transactions.length != 0) {
                            last = transactions[transactions.length - 1].transactionInfo.id;
                        }
                        isLast = (transactions.length < 10) || (last == lastId);
                        lastId = last;
                        transactions.forEach(tx => {
                            const isCreateMultisig = this.signing.checkJoinMultisigAcc(tx);
                            const isNotCreator = tx.signer.publicKey != this.global.loggedAccount.publicKey;
                            const signedCosigners = tx.cosignatures.map(cosignature => cosignature.signer.publicKey);
                            const isNotSigned = !signedCosigners.includes(this.global.loggedAccount.publicKey);
                            if (isCreateMultisig && isNotCreator && isNotSigned) {
                                let cosigners = [];
                                cosigners.push(tx.signer.publicKey);
                                tx.innerTransactions.forEach((tx, index) => {
                                    if (index != 0) {
                                        cosigners.push(tx.signer.publicKey);
                                    }
                                });
                                // Add group
                                const messageInfo = HelperService.parseMessage(tx.innerTransactions[1].message.payload);
                                const groupName = messageInfo.groupName;
                                const group: SiriusGroups = {
                                    name: groupName,
                                    multisigAcc: tx.innerTransactions[0].signer,
                                    creator: tx.signer.publicKey,
                                    members: cosigners,
                                    isActive: false,
                                    numOfAccept: tx.cosignatures.length,
                                    transaction: tx
                                }
                                this.waitingGroups.push(group);
                            }

                        })
                    });
                }
                catch {
                    isLast = true;
                }
                loadAll();
            }
        }
        loadAll();
    }
}
;
