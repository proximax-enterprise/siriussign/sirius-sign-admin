import { TestBed } from '@angular/core/testing';

import { BackupService } from './backup.service';
import { File } from '@ionic-native/file/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';

describe('BackupService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      IonicStorageModule.forRoot()
    ],
    providers: [
      File,
      Storage,
      Device,
      FileOpener
    ]
  }));

  it('should be created', () => {
    const service: BackupService = TestBed.get(BackupService);
    expect(service).toBeTruthy();
  });
});
