import { Injectable } from '@angular/core';
import {
  TransferTransaction,
  Deadline,
  NetworkType,
  PlainMessage,
  UInt64,
  AddressAliasTransaction,
  RegisterNamespaceTransaction,
  NamespaceHttp,
  NamespaceId,
  AliasActionType,
  AccountHttp,
  Order,
  AggregateTransaction,
  NetworkCurrencyMosaic,
  Mosaic,
  MosaicId,
  SimpleWallet, Listener, Address, Account, PublicAccount, Transaction, TransactionHttp } from 'tsjs-xpx-chain-sdk';
import { GlobalService } from '../services/global.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DocumentOrganizationService {

  constructor() { }
}
