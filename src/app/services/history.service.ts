import { Injectable } from '@angular/core';

import {
    AccountHttp,
    TransferTransaction,
    QueryParams,
    Transaction,
    AggregateTransaction,
    TransactionType
} from 'tsjs-xpx-chain-sdk';

import { SiriusDocument } from './../model/sirius-document.model';
import { HelperService } from './../services/helper.service';
import { GlobalService } from './global.service';

interface HistoryDocument {
    id: string,
    name: string,
    own: boolean,
    isSigned: number,
    actionDate: Date,
    lastModifiedDate: Date,
    numCoSign: number,
    numSigned: number
}

@Injectable({
    providedIn: 'root'
})

export class HistoryService {

    docs: HistoryDocument[] = [];
    lastId = '';
    isLast = false;
    documentInfo: SiriusDocument[] = [];

    constructor(private global: GlobalService) { }

    /**
     * Add a Sirius Document to history
     * @param doc 
     */
    add(doc: SiriusDocument) {
        let numCoSign = doc.cosigners.length;

        let numSigned = numCoSign;  //Temp, TODO: will update when multisig is considered

        let newDoc = {
            id: doc.id,
            name: doc.name,
            own: doc.owner == this.global.loggedAccount.address.plain(),
            isSigned: doc.isSigned,
            actionDate: doc.uploadDate,
            lastModifiedDate: doc.uploadDate,
            numCoSign: numCoSign,
            numSigned: numSigned
        }
        this.documentInfo.push(doc);
        this.docs.push(newDoc);
    }

    /**
     * Fetch multisig account info for creating sirius document to add to history
     * @param transaction 
     */
    private createHistoryMultisigSiriusDoc(transaction, fn) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const innerTx = transaction.innerTransactions[0];
        const payload = innerTx.message.payload;
        let info = HelperService.parseMessage(payload);

        let uploadDate = new Date(transaction.deadline.value.plusHours(-23).toString());
        const now = Date.now();
        const millis = now - uploadDate.getTime();
        if (millis < 0) uploadDate = new Date(info.meta);

        accountHttp.getMultisigAccountInfo(innerTx.signer.address)
            .subscribe(multisigAccInfo => {
                const cosigners = multisigAccInfo.cosignatories.map(account => account.address.plain());
                const cosignatures = transaction.cosignatures
                    .map(aggTxCosignatures => aggTxCosignatures.signer.address.plain());
                let doc = SiriusDocument.create(
                    transaction.transactionInfo.id,
                    info.name,
                    info.fileHash,
                    uploadDate,
                    transaction.isConfirmed ? 'Confirmed' : 'Unconfirmed',
                    transaction.signer.address.plain(),
                    1,
                    cosigners,
                    cosignatures,
                    innerTx.signer,
                    null,
                    transaction.transactionInfo.hash,
                    info.uploadTxHash,
                    false
                );
                fn(doc);
            })
    }

    /**
     * Create sirius document to add to history
     * @param transaction 
     */
    private createHistorySingleSiriusDoc(transaction: TransferTransaction, fn) {
        const payload = transaction.message.payload;
        let info = HelperService.parseMessage(payload);

        let uploadDate = new Date(transaction.deadline.value.plusHours(-2).toString());
        const now = Date.now();
        const millis = now - uploadDate.getTime();
        if (millis < 0) uploadDate = new Date(info.meta);

        const isEncrypt = info.isEncrypt == '1';
        let doc = SiriusDocument.create(
            transaction.transactionInfo.id,
            info.name,
            info.fileHash,
            uploadDate,
            transaction.isConfirmed ? 'Confirmed' : 'Unconfirmed',
            transaction.signer.address.plain(),
            1,
            [],
            [],
            null,
            null,
            transaction.transactionInfo.hash,
            info.uploadTxHash,
            isEncrypt
        );
        fn(doc);
    }

    /**
     * Fetch Transactions from network
     */
    fetchTransactions(fn?) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        let querryParams = null;
        if (this.lastId != '') {
            querryParams = new QueryParams(15, this.lastId);
        }

        accountHttp.transactions(this.global.loggedAccount.publicAccount, querryParams)
            .subscribe((transactions: any) => {
                console.log('Fetch from: ' + this.lastId);
                console.log(transactions);
                let last;
                if (transactions.length != 0) {
                    last = transactions[transactions.length - 1].transactionInfo.id;
                }
                this.isLast = (transactions.length < 10) || (last == this.lastId);
                this.lastId = last;
                console.log(this.lastId);
                transactions.forEach((transaction, index) => {
                    let isSiriusTransaction = this.checkSiriusTransactionType(transaction);
                    if (isSiriusTransaction) {
                        if (transaction.type == TransactionType.AGGREGATE_BONDED) {
                            this.createHistoryMultisigSiriusDoc(transaction, document => {
                                this.add(document)
                            });
                        }
                        else {
                            this.createHistorySingleSiriusDoc(transaction, document => {
                                this.add(document)
                            });
                        }
                    }
                    if (index == (transactions.length - 1) && fn) { fn(); }
                });
            });
    }

    /**
     * Check if a transaction is Sirius Sign transaction
     * @param transaction 
     */
    checkSiriusTransactionType(transaction: Transaction) {
        if (transaction.type == TransactionType.TRANSFER) {
            let tx = <TransferTransaction>transaction;
            const info = HelperService.parseMessage(tx.message.payload);
            if (info.appCodeName == this.global.appCodeName) return true;
        }
        if (transaction.type == TransactionType.AGGREGATE_BONDED) {
            let anyTx = <any>transaction;
            let aggregateBondedTx = <AggregateTransaction>anyTx;
            if (aggregateBondedTx.innerTransactions[0].type != TransactionType.TRANSFER) return false;
            let tx = <TransferTransaction>aggregateBondedTx.innerTransactions[0];
            const info = HelperService.parseMessage(tx.message.payload);
            if (info.appCodeName == this.global.appCodeName) return true;
        }
        return false;
    }

    /**
     * Reload history document list
     */
    reloadHistory() {
        this.docs = [];
        this.lastId = '';
        this.isLast = false;
        this.documentInfo = [];

        const recursiveLoad = () => {
            if ((this.docs.length < 16) && (!this.isLast)) {
                this.fetchTransactions(recursiveLoad);
            }
        }

        recursiveLoad();
    }

    /**
     * Sort document in descending order of id/time
     */
    sortDocs() {
        this.docs.sort((a, b) => {
            if (a.id > b.id) return -1;
            else return 1;
        });
    }

    /**
     * Get document by ID
     * @param id 
     */
    getDocById(id) {
        let ids = this.documentInfo.map(doc => doc.id);
        let index = ids.indexOf(id);
        if (index != -1) return this.documentInfo[index];
        else throw new Error("No documents have the given id.")
    }
}
