import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SimpleWallet, Password, AccountHttp, Crypto, WalletAlgorithm, Account, Address } from 'tsjs-xpx-chain-sdk';

import { SiriusWallet } from './../model/sirius-wallet.model';

import { GlobalService } from './global.service';

export enum WalletType {
    /** User wallet */
    WLT = 'wlt',
    /** User notarization wallet */
    NWLT = 'nWlt'
}

@Injectable({
    providedIn: 'root'
})

export class WalletService {

    private preparedInfo = {
        name: '',
        pass: '',
        privateKey: '',
        notaPrivateKey: '',
        plan: 1
    }

    constructor(private storage: Storage, private global: GlobalService) {
        this.clearInfo();
    }

    /**
     * Fetch stored wallets
     * @async
     * @param type optional, default: WalletType.WLT
     * @returns
     */
    async fetchWallets(type: WalletType = WalletType.WLT) {
        let wallets: SiriusWallet[] = await this.storage.get(type);
        return wallets;
    }

    /**
     * Store wallet to storage
     * @param {SiriusWallet} wallet
     * @param type optional, default: WalletType.WLT
     */
    private async storeWallet(wallet: SiriusWallet, type: WalletType = WalletType.WLT) {
        const wallets: SiriusWallet[] = await this.fetchWallets(type)
            .catch(err => {
                console.log(err);
                throw err;
            });

        if (wallets) {
            const walletNames = wallets.filter(wlt => wlt.name === wallet.name);
            const isExist = walletNames.length > 0;
            if (!isExist) {
                wallets.push(wallet);
                await this.storage.set(type, wallets)
                    .catch(err => {
                        console.log(err);
                        throw err;
                    });
            }
            else {
                throw new Error('Wallet already exists.');
            }
        }
        else {
            await this.storage.set(type, [wallet]);
        }
    }

    /**
     * Check if a wallet with particular name exists
     * @async
     * @param {string} walletName
     * @returns
     */
    async checkWalletExist(walletName: string) {
        try {
            let wallets = await this.fetchWallets();
            let names = wallets.map(wallet => wallet.name);
            return names.includes(walletName);
        }
        catch (err) {
            console.log(err);
            return false;
        }
    }

    /**
     * Create new wallet
     * @param {string} name
     * @param {string} password
     * @returns {SiriusWallet}
     */
    async createWallet(name?: string, password?: string, type: WalletType = WalletType.WLT) {
        let wName = this.preparedInfo.name;
        let wPassword = this.preparedInfo.pass;
        let wPlan = this.preparedInfo.plan;
        if (name != undefined) {
            wName = name;
            wPlan = 0;
        }
        if (password != undefined) wPassword = password;
        const wPass = new Password(wPassword);
        const wallet = SimpleWallet.create(wName, wPass, this.global.networkType);
        console.log(wallet);
        const siriusWallet = SiriusWallet.createFromSimpleWallet(wallet, wPassword, wPlan);
        console.log(siriusWallet);
        try {
            await this.storeWallet(siriusWallet, type);
            return siriusWallet;
        }
        catch (err) {
            console.log(err);
        }
    }

    /**
     * Create wallet from private key for recover an account
     */
    async createWalletFromPrivateKey(type: WalletType = WalletType.WLT) {
        if ((this.preparedInfo.privateKey == '') || (this.preparedInfo.notaPrivateKey == ''))
            throw new Error('No Private Key');
        let wName = this.preparedInfo.name;
        let wPassword = this.preparedInfo.pass;
        let wPrivateKey = this.preparedInfo.privateKey;
        let wPlan = this.preparedInfo.plan;
        if (type == WalletType.NWLT) {
            wName = 'notarization.' + this.preparedInfo.name;
            wPrivateKey = this.preparedInfo.notaPrivateKey;
        }
        const wPass = new Password(wPassword);
        const wallet = SimpleWallet.createFromPrivateKey(wName, wPass, wPrivateKey, this.global.networkType);
        console.log(wallet);
        const siriusWallet = SiriusWallet.createFromSimpleWallet(wallet, wPassword, wPlan);
        console.log(siriusWallet);
        try {
            await this.storeWallet(siriusWallet, type);
            return siriusWallet;
        }
        catch (err) {
            this.updateWallet(siriusWallet.name, type, (wlt: SiriusWallet) => siriusWallet);
            console.log(err);
        }
    }

    /**
     * Update a wallet
     * @param name 
     * @param fnUpdate 
     */
    async updateWallet(name: string, type: WalletType, fnUpdate) {
        console.log('update ' + name);
        const wallets: SiriusWallet[] = await this.storage.get(type)
            .catch(err => {
                console.log(err);
                throw new Error('No wallet with given name.');
            });

        console.log(wallets);
        const names = wallets.map(wallet => wallet.name);
        const isExist = names.includes(name);
        const index = names.indexOf(name);

        if (!isExist) {
            throw new Error('No wallet with given name.');
        }
        else {
            const sWallet = SiriusWallet.createFromRaw(wallets[index]);
            const updatedWallet = fnUpdate(sWallet);
            console.log(updatedWallet);
            wallets[index] = updatedWallet;
            this.storage.set(type, wallets);
        }
    }

    /**
     * Override wallet for recovery
     * @param wallet 
     * @param notaWallet 
     */
    overrideWallet(wallet: SiriusWallet, notaWallet: SiriusWallet) {
        this.updateWallet(wallet.name, WalletType.WLT, (wlt: SiriusWallet) => wallet);
        this.updateWallet(notaWallet.name, WalletType.NWLT, (wlt: SiriusWallet) => notaWallet);
    }

    /**
     * Allow fingerprint sign in a wallet then update to storage
     * @param name 
     * @param password 
     */
    allowFingerprint(name, password) {
        this.updateWallet(
            name,
            WalletType.WLT,
            wallet => {
                wallet.generateCode(password);
                return wallet;
            }
        );
        this.updateWallet(
            'notarization.' + name,
            WalletType.NWLT,
            (wallet: SiriusWallet) => {
                wallet.generateCode(password);
                return wallet;
            }
        );
    }

    /**
     * Remember not use fingerprint
     * @param name 
     */
    disableFingerprint(name: string) {
        this.updateWallet(
            name,
            WalletType.WLT,
            (wallet: SiriusWallet) => {
                wallet.tertiary = wallet.tertiary.substr(0, 62);
                return wallet;
            }
        );
    }

    /**
     * Update plat of user
     * @param name 
     * @param plan 
     */
    changePlan(name: string, plan: number) {
        this.updateWallet(
            name,
            WalletType.WLT,
            (wallet: SiriusWallet) => {
                wallet.plan = plan;
                return wallet;
            }
        );
        this.updateWallet(
            'notarization.' + name,
            WalletType.NWLT,
            (wallet: SiriusWallet) => {
                wallet.plan = plan;
                return wallet;
            }
        );
    }

    /**
     * Get account balance
     */
    async fetchBalance() {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const mosaicId = this.global.mosaicId;
        const mosaicInfo = this.global.mosaicInfo;
        const mosaicName = this.global.mosaicName;

        return new Promise<string>(resolve => {
            accountHttp.getAccountInfo(this.global.loggedAccount.address)
                .subscribe(accountInfo => {
                    const mosaics = accountInfo.mosaics;
                    const filteredMosaic = mosaics.filter(mosaic => mosaic.id.equals(mosaicId));
                    if (filteredMosaic.length == 0) resolve('0 ' + mosaicName);
                    else {
                        const amount = filteredMosaic[0].amount;
                        const balance = amount.compact() / Math.pow(10, mosaicInfo.divisibility);
                        resolve(balance + ' ' + mosaicName);
                    }
                },
                    err => resolve('0 ' + mosaicName));
        });
    }

    /**
     * Set prepared name
     * @param {string} name
     */
    setInfo(name: string, pass: string, plan: number, privateKey?: string, notaPrivateKey?: string) {
        this.preparedInfo.name = name;
        this.preparedInfo.pass = pass;
        this.preparedInfo.plan = plan;
        if (privateKey) this.preparedInfo.privateKey = privateKey;
        if (notaPrivateKey) this.preparedInfo.notaPrivateKey = notaPrivateKey;
    }

    /**
     * Get prepared name
     * @returns
     */
    getInfoName() {
        return this.preparedInfo.name;
    }

    /**
     * Get prepared password
     */
    getInfoPass() {
        return this.preparedInfo.pass;
    }

    /**
     * Clear the prepared Info
     */
    clearInfo() {
        this.setInfo('', '', 0, '', '');
    }

    /**
     * Get stored wallet by name
     * @param name
     * @param type - optional, default: WalletType.WLT
     * @async
     */
    async getWalletByName(name: string, type: WalletType = WalletType.WLT) {
        const wallets = await this.fetchWallets(type);
        if (wallets) {
            const names = wallets.map(wallet => wallet.name);
            const index = names.indexOf(name);
            if (index >= 0) {
                const sWallet = SiriusWallet.createFromRaw(wallets[index]);
                return sWallet;
            }
        }
        throw new Error('No wallet with given name');
    }

    /**
     * Verify password
     * @param wallet
     * @param password
     */
    checkWalletPassword(wallet: SiriusWallet, password: string) {
        try {
            const common = {
                password: password,
                privateKey: '',
            }
            const walletKey = {
                encrypted: wallet.encryptedPrivateKey.encryptedKey,
                iv: wallet.encryptedPrivateKey.iv,
            };
            Crypto.passwordToPrivateKey(common, walletKey, WalletAlgorithm.Pass_bip32);
            if (common.privateKey == "" || (common.privateKey.length != 64 && common.privateKey.length != 66))
                throw new Error("Invalid password");
            return true;
        } catch (err) {
            return false;
        }
    }

    /**
     * Get SiriusWallet Object from wallet Object fetch from storage
     * @param wallet 
     * @param password 
     */
    getWalletFromRaw(wallet, password) {
        try {
            const common = {
                password: password,
                privateKey: '',
            }
            const walletKey = {
                encrypted: wallet.encryptedPrivateKey.encryptedKey,
                iv: wallet.encryptedPrivateKey.iv,
            };
            Crypto.passwordToPrivateKey(common, walletKey, WalletAlgorithm.Pass_bip32);
            const simpleWallet = SimpleWallet.createFromPrivateKey(
                wallet.name,
                new Password(password),
                common.privateKey,
                this.global.networkType
            );
            return SiriusWallet.createFromSimpleWallet(simpleWallet, password, wallet.plan);
        } catch (err) {
            console.log(err);
        }
    }

    /**
     * Get 'real' SimpleWallet Object from wallet Object fetch from storage
     * @param wallet 
     * @param password 
     */
    getSimpleWalletFromRaw(wallet, password) {
        try {
            const common = {
                password: password,
                privateKey: '',
            }
            const walletKey = {
                encrypted: wallet.encryptedPrivateKey.encryptedKey,
                iv: wallet.encryptedPrivateKey.iv,
            };
            Crypto.passwordToPrivateKey(common, walletKey, WalletAlgorithm.Pass_bip32);
            return SimpleWallet.createFromPrivateKey(
                wallet.name,
                new Password(password),
                common.privateKey,
                this.global.networkType
            );
        } catch (err) {
            console.log(err);
        }
    }

    /**
     * Create a notarization wallet
     * @param name 
     * @param password 
     */
    createNotarizationWallet(name?: string, password?: string) {
        let notarizationWalletName = 'notarization.' + this.preparedInfo.name;
        let notarizationWalletPass = this.preparedInfo.pass;
        if (name != undefined) notarizationWalletName = 'notarization.' + name;
        if (password != undefined) notarizationWalletPass = password;
        this.createWallet(notarizationWalletName, notarizationWalletPass, WalletType.NWLT);
    }

    /**
     * Create a notarization wallet from private key to recover an account
     * @param name 
     * @param password 
     */
    async createNotarizationWalletFromPrivateKey() {
        if ((this.preparedInfo.privateKey == '') || (this.preparedInfo.notaPrivateKey == ''))
            throw new Error('No Notarization Private Key');
        await this.createWalletFromPrivateKey(WalletType.NWLT);
    }

    /**
     * Fetch logged in wallet from storage.
     * @async
     */
    async fetchLoggedWallet() {
        let isLogged = await this.global.fetchIsLoggedIn();
        if (isLogged) {
            let walletObj = await this.storage.get('loggedWlt')
            let master = await this.storage.get('master');
            let slave = await this.storage.get('slave');
            let key = this.global.deriveSlaveKey(slave, master);
            let wallet = this.getWalletFromRaw(walletObj, key);
            this.global.loggedWallet = wallet;
            return this.global.loggedWallet;
        }
        else {
            throw new Error('There is no wallet logged.')
        }
    }

    /**
     * Wrapper for sign in function
     * @param wallet
     * @param password
     */
    async signIn(wallet: SiriusWallet, password: string) {
        let isPassMatch = this.checkWalletPassword(wallet, password);
        console.log(isPassMatch);
        if (isPassMatch) {
            const siriusWallet = this.getWalletFromRaw(wallet, password);
            const simpleWallet = this.getSimpleWalletFromRaw(wallet, password);
            this.global.setIsLoggedIn(true);
            this.global.setLoggedWallet(siriusWallet);
            // let loggedAcc = this.global.loggedWallet.open(new Password(password));
            // let masterKey = this.createMasterKey(loggedAcc);
            // this.createSlaveKey(password, masterKey);
            const loggedAcc = Account.createFromPrivateKey(simpleWallet.open(new Password(password)).privateKey, this.global.networkType);
            this.global.setLoggedAccount(loggedAcc);

            //Notarization
            const wltName = this.global.loggedWallet.name;
            const nWltName = 'notarization.' + wltName;
            const nWallet: SiriusWallet = await this.getWalletByName(nWltName, WalletType.NWLT)
                .catch(err => { throw err; });
            const nSimpleWallet = this.getSimpleWalletFromRaw(nWallet, password);
            const nAccount = nSimpleWallet.open(new Password(password));
            this.global.notarizationAddress = Address.createFromRawAddress(nWallet.address.plain());
            this.global.notarizationAccount = nAccount;
        }
        else {
            throw new Error('Password is invalid.');
        }
    }

    async signInWithFingerprint(wallet) {
        const siriusWallet = SiriusWallet.createFromRaw(wallet);
        const privateKey = siriusWallet.getPrivKey();
        const loggedAcc = Account.createFromPrivateKey(privateKey, this.global.networkType);
        this.global.setIsLoggedIn(true);
        this.global.setLoggedWallet(siriusWallet);
        this.global.setLoggedAccount(loggedAcc);

        //Notarization
        const wltName = this.global.loggedWallet.name;
        const nWltName = 'notarization.' + wltName;
        const nWallet = await this.getWalletByName(nWltName, WalletType.NWLT);
        const nSiriusWallet = SiriusWallet.createFromRaw(nWallet);
        const nPrivateKey = nSiriusWallet.getPrivKey();
        const nAccount = Account.createFromPrivateKey(nPrivateKey, this.global.networkType);
        this.global.notarizationAddress = Address.createFromRawAddress(nWallet.address.plain());
        this.global.notarizationAccount = nAccount;
    }
}


