import { TestBed } from '@angular/core/testing';

import { DocumentHistoryService } from './document-history.service';

describe('DocumentHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentHistoryService = TestBed.get(DocumentHistoryService);
    expect(service).toBeTruthy();
  });
});
