import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { BehaviorSubject } from 'rxjs';

import { UploadStorageService } from './upload-storage.service';
import { GlobalService } from './global.service';

interface Signature {
    owner: string,
    name: string,
    signatureImg: string,
    uploadTxHash: string
}

@Injectable({
    providedIn: 'root'
})
export class SignatureService {

    name: string = '';
    isUploadedSigImg: boolean;
    signatureImg: string = '';
    signatureImgTxHash: string = '';
    observableSignatureImg: BehaviorSubject<string>;

    constructor(
        private storage: Storage,
        private global: GlobalService,
        private uploadStorage: UploadStorageService
    ) {
        this.observableSignatureImg = new BehaviorSubject<string>(this.signatureImg);
    }

    /**
     * Store signature to storage
     * @param signatureImg 
     * @param uploadTxHash 
     */
    async storeSignature(owner: string, signatureImg: string, uploadTxHash: string) {
        const signautre: Signature = {
            owner: owner,
            name: '',
            signatureImg: signatureImg,
            uploadTxHash: uploadTxHash
        }

        const signatureArray: Signature[] = await this.storage.get('signature')
            .catch(err => {
                this.storage.set('signature', [signautre]);
            });
        if (!signatureArray) return this.storage.set('signature', [signautre]);

        const signatureOwners = signatureArray.map(signatureElement => signatureElement.owner);
        const isExist = signatureOwners.includes(owner);
        if (isExist) throw new Error('This account registered a signature');

        signatureArray.push(signautre);
        return this.storage.set('signature', signatureArray);
    }

    /**
     * Upload and store signature image
     * @param imgUri 
     */
    async assign(imgUri: string) {
        this.signatureImg = imgUri;
        const uploadResult = await this.uploadStorage.uploadSignature(this.signatureImg)
            .catch(err => { throw new Error('Upload signature: ' + err.message); });
        this.signatureImgTxHash = uploadResult.transactionHash;
        await this.storeSignature(this.global.loggedWallet.name, this.signatureImg, this.signatureImgTxHash)
            .catch(err => { throw new Error('Store signature: ' + err.message); });
        return this.signatureImg;
    }

    /**
     * Fetch signautre image from storage
     */
    async fetchFromStorage() {
        const signatures: Signature[] = await this.storage.get('signature')
            .catch(err => {
                this.signatureImg = '';
                this.signatureImgTxHash = '';
                this.name = '';
            });
        if (!signatures) {
            this.signatureImg = '';
            this.signatureImgTxHash = '';
            this.name = '';
        }
        else {
            const signature = signatures.filter(signature => signature.owner === this.global.loggedWallet.name);
            if (signature.length == 0) {
                this.signatureImg = '';
                this.signatureImgTxHash = '';
                this.name = '';
            }
            else {
                this.signatureImg = signature[0].signatureImg;
                this.signatureImgTxHash = signature[0].uploadTxHash;
                this.name = signature[0].name;
            }
        }
        this.observableSignatureImg.next(this.signatureImg);
        return this.signatureImg;
    }

    /**
     * Ovveride signature image for recovery
     * @param owner 
     * @param signatureImg 
     * @param uploadTxHash 
     */
    async override(owner: string, name: string, signatureImg?: string, uploadTxHash?: string) {
        const signautre: Signature = {
            owner: owner,
            name: name,
            signatureImg: signatureImg,
            uploadTxHash: uploadTxHash
        }

        const signatureArray: Signature[] = await this.storage.get('signature')
            .catch(err => {
                this.storage.set('signature', [signautre]);
            });
        if (!signatureArray) return this.storage.set('signature', [signautre]);

        const signatureOwners = signatureArray.map(signatureElement => signatureElement.owner);
        const isExist = signatureOwners.includes(owner);
        if (!isExist) throw new Error('This account has no signature to override');
        const index = signatureOwners.indexOf(owner);
        signatureArray[index] = signautre;
        return this.storage.set('signature', signatureArray);
    }
}
