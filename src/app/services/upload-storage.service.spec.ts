import { TestBed } from '@angular/core/testing';

import { UploadStorageService } from './upload-storage.service';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';
import { GlobalService } from './global.service';
import { Account, NetworkType } from 'tsjs-xpx-chain-sdk';
import { SignDocumentService } from './sign-document.service';
import { UploadResult } from 'tsjs-chain-xipfs-sdk';

describe('UploadStorageService', () => {
  let service:UploadStorageService;
  let global:GlobalService;
  let sign: SignDocumentService;
  let account:Account;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot()
      ],
      providers: [
          Storage,
          Device,
          GlobalService,
          SignDocumentService
      ]
    })

    service = TestBed.get(UploadStorageService)
    global = TestBed.get(GlobalService)
    sign = TestBed.get(SignDocumentService)
    account = Account.createFromPrivateKey('EC994CC2F78603D554809FC06CC587357CDE40E6577B4B069E881ECA6D901E04', NetworkType.TEST_NET);
    global.setLoggedAccount(account);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Download file from file hash (no encrypt)', async ()=>{
    const fileHash = '5C33FEDC78E3BC8BF3936E4953C4747AD4FA0D210EC362E8D909AED39D91B193';
    const result = await service.downloadFile(fileHash);
    expect(result).toBeDefined()
    expect(result.transactionHash).toEqual(fileHash)
  });

  it('Download file from file hash (encrypted)', async ()=>{
    const fileHash = '5C33FEDC78E3BC8BF3936E4953C4747AD4FA0D210EC362E8D909AED39D91B193';
    const result = await service.downloadFileEncrypt(fileHash);
    expect(result).toBeDefined()
    expect(result.transactionHash).toEqual(fileHash)
    // expect(result).toEqual('')
  })

  it('Upload file', async () => {
    // Download file, after that, upload that file to blockchain
    const fileHash = '5C33FEDC78E3BC8BF3936E4953C4747AD4FA0D210EC362E8D909AED39D91B193';
    const download = await service.downloadFile(fileHash);
    const fileData:Buffer = await download.data.getContentAsBuffer()
    expect(fileData.length).not.toEqual(0)
    const fileInBase64 =  Buffer.from(fileData).toString('base64');


    let fileTest = {
      data: fileData,
      dataURI: 'data:application/pdf;base64,' + fileInBase64,
      mediaType: 'application/pdf',
      name: 'TestDocument.pdf',
      uri: 'TestDocument.pdf'
    }


    // Set document
    sign.setDocument(fileTest)
    expect(sign.getDocmument()).toBeDefined()


    /**
     * Upload, it maybe cause lost balance, uncomment this when running test this case
     */
    // const result:UploadResult = await service.uploadFile(sign.getDocmument())
    // expect(result.transactionHash).not.toEqual('')

  })


  it('Upload file with Encrypt', async () => {
    // Download file, after that, upload that file to blockchain
    const fileHash = '5C33FEDC78E3BC8BF3936E4953C4747AD4FA0D210EC362E8D909AED39D91B193';
    const download = await service.downloadFile(fileHash);
    const fileData:Buffer = await download.data.getContentAsBuffer()
    expect(fileData.length).not.toEqual(0)
    const fileInBase64 =  Buffer.from(fileData).toString('base64');


    let fileTest = {
      data: fileData,
      dataURI: 'data:application/pdf;base64,' + fileInBase64,
      mediaType: 'application/pdf',
      name: 'TestDocument.pdf',
      uri: 'TestDocument.pdf'
    }


    // Set document
    sign.setDocument(fileTest)
    expect(sign.getDocmument()).toBeDefined()


    /**
     * Upload, it maybe cause lost balance, uncomment this when running test this case
     */
    // const result:UploadResult = await service.uploadFileEncrypt(sign.getDocmument())
    // expect(result.transactionHash).not.toEqual('')

  })


});
