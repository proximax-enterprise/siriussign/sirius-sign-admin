import { Injectable } from '@angular/core';
import { ChooserResult } from '@ionic-native/chooser/ngx'
import * as CryptoJS from 'crypto-js';
import { BehaviorSubject } from 'rxjs';

import { PublicAccount, Deadline, PlainMessage, TransferTransaction, UInt64 } from 'tsjs-xpx-chain-sdk';

import { SiriusSignDocument } from '../model/siriussign-document.model';
import { SsDocumentVerifyMessage } from '../model/siriussign-message.model';

import { GlobalService } from './global.service';
import { SignDocument } from './sign-document.service';

@Injectable({
    providedIn: 'root'
})
export class VerifyService {
    // For audit
    document = {
        file: {
            data: new Uint8Array(0),
            dataURI: '',
            mediaType: '',
            name: '',
            uri: ''
        },
        size: 0,
        signDate: new Date(),
        cosigners: [],
        fileHash: '',
        signTxHash: '',
        status: ''
    }

    auditResult = {
        isValid: false,
        message: 0
    }

    docResult: SiriusSignDocument;

    // For verify
    isRealVerify: boolean = false;
    verifyResult = {
        isValid: false,
        status: ''
    }

    isNeedReload: boolean = false;
    observableIsNeedReload: BehaviorSubject<boolean>;

    constructor(private global: GlobalService) { 
        this.observableIsNeedReload = new BehaviorSubject<boolean>(this.isNeedReload);
    }

    /**
     * Set document to verify
     * @param file 
     * @param size 
     */
    setDocument(file: ChooserResult, size: number = 0) {
        this.document.file = file;
        this.document.signDate = new Date(Date.now());
        this.document.fileHash = this.hashFile('SHA256');
        this.document.signTxHash = '';
        this.document.size = size;

        const nameArr = file.name.split('-');
        nameArr.forEach(str => {
            let trimmedStr = str.replace(/\s/g, "");
            if (trimmedStr.substr(0, 3) == 'STH') {
                this.document.signTxHash = trimmedStr.split('.')[0].substr(3);
            }
        });
    }

    /**
     * Clear document
     */
    clearDocument() {
        this.document = {
            file: {
                data: new Uint8Array(0),
                dataURI: '',
                mediaType: '',
                name: '',
                uri: ''
            },
            size: 0,
            signDate: new Date(),
            cosigners: [],
            fileHash: '',
            signTxHash: '',
            status: ''
        }
    }

    /**
     * Generate file hash
     * @param method 
     */
    hashFile(method: string) {
        const data = this.document.file.dataURI; //CryptoJS.enc.Base64.parse(this.document.file.dataURI);
        switch (method) {
            case 'SHA256': return CryptoJS.SHA256(data).toString();
            case 'MD5': return CryptoJS.MD5(data).toString();
            default: return CryptoJS.SHA256(data).toString();
        }
    }

    /**
     * Clear verify result
     */
    clearVerifyResult() {
        this.auditResult = {
            isValid: false,
            message: 0
        }
        this.verifyResult = {
            isValid: false,
            status: ''
        }
    }

    /**
     * Create VERIFY transaction to verify a document
     * @param document 
     * @param isOwner 
     */
    createDocVerifyTransaction(document: SignDocument | SiriusSignDocument) {
        let fileHash: string;
        let fileName: string;
        let documentAcc: PublicAccount;

        if (document instanceof SiriusSignDocument) {
            const ssDoc = document as SiriusSignDocument;
            fileHash = ssDoc.fileHash.sha256Hash;
            fileName = ssDoc.name;
            documentAcc = ssDoc.documentAccount;
        }
        else {
            fileHash = document.fileHash;
            fileName = document.file.name;
            documentAcc = document.documentAcc.publicAccount;
        }

        const ssMessage = new SsDocumentVerifyMessage(
            this.global.appCodeName,
            this.global.appVersion,
            fileHash,
            fileName,
            false,
            document.uploadTxHash,
            document.isEncrypt,
        );

        const message = PlainMessage.create(ssMessage.toMessage());

        const docVerifyTransaction = TransferTransaction.create(
            Deadline.create(),
            documentAcc.address,
            [],
            message,
            this.global.networkType,
            new UInt64([0, 0])
        );

        const networkGenerationHash = this.global.networkGenerationHash;
        const verifiedDocTransaction = this.global.loggedAccount.sign(docVerifyTransaction, networkGenerationHash);

        return verifiedDocTransaction;
    }
}
