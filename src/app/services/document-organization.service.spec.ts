import { TestBed } from '@angular/core/testing';

import { DocumentOrganizationService } from './document-organization.service';

describe('DocumentOrganizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentOrganizationService = TestBed.get(DocumentOrganizationService);
    expect(service).toBeTruthy();
  });
});
