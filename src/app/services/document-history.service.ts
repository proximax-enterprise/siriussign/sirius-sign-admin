import { SsDocumentSigningMessage, SiriusMessageType } from './../model/siriussign-message.model';
import { Injectable } from '@angular/core';

import {
    AccountHttp,
    TransferTransaction,
    QueryParams,
    Transaction,
    AggregateTransaction,
    TransactionType,
    Address,
    AccountRestrictionsInfo,
    RestrictionType,
    PublicAccount,
    AccountInfo
} from 'tsjs-xpx-chain-sdk';

import { SiriusDocument } from './../model/sirius-document.model';
import { HelperService } from './../services/helper.service';
import { GlobalService } from './global.service';

interface HistoryDocument {
    id: string,
    name: string,
    own: boolean,
    isSigned: number,
    actionDate: Date,
    lastModifiedDate: Date,
    numCoSign: number,
    numSigned: number
}

@Injectable({
    providedIn: 'root'
})
export class DocumentHistoryService {

    docs: HistoryDocument[] = [];
    lastId = '';
    isLast = false;
    documentInfo: SiriusDocument[] = [];

    constructor(private global: GlobalService) { }

    /**
     * Add a Sirius Document to history
     * @param doc 
     */
    add(doc: SiriusDocument) {
        let numCoSign = doc.cosigners.length;

        let numSigned = numCoSign;  //Temp, TODO: will update when multisig is considered

        let newDoc = {
            id: doc.id,
            name: doc.name,
            own: doc.owner == this.global.loggedAccount.address.plain(),
            isSigned: doc.isSigned,
            actionDate: doc.uploadDate,
            lastModifiedDate: doc.uploadDate,
            numCoSign: numCoSign,
            numSigned: numSigned
        }
        this.documentInfo.push(doc);
        this.docs.push(newDoc);
    }

    async createHistoryDocument(transaction: TransferTransaction) {
        const payload = transaction.message.payload;
        const info = <SsDocumentSigningMessage>HelperService.parseSsMessage(payload);

        let uploadDate = new Date(transaction.deadline.value.plusHours(-2).toString());
        const now = Date.now();
        const millis = now - uploadDate.getTime();
        if (millis < 0) uploadDate = new Date(info.meta["timestamp"]);

        // Fetch cosigners of the document
        const accountHttp = new AccountHttp(this.global.apiNode);
        const docAccRestiction: AccountRestrictionsInfo = await accountHttp.getAccountRestrictions(<Address>transaction.recipient)
            .toPromise().catch(err => { console.log(err); return null; });
        const docAccAllow = docAccRestiction.accountRestrictions.restrictions
            .filter(restriction => restriction.restrictionType == RestrictionType.AllowAddress)[0];
        const doccAccAllowAddresses = docAccAllow.values;
        const cosigners = doccAccAllowAddresses.filter((address: Address) => !address.equals(this.global.loggedAccount.address));

        // Fetch cosigners who signed
        const docAccInfo: AccountInfo = await accountHttp.getAccountInfo(<Address>transaction.recipient).toPromise()
            .catch(err => { console.log(err); return null; });
        const docAccPublicKey = docAccInfo.publicKey;
        const docPublicAcc = PublicAccount.createFromPublicKey(docAccPublicKey, this.global.networkType);
        const incomingTxs: Transaction[] = await accountHttp.incomingTransactions(docPublicAcc).toPromise()
            .catch(err => { console.log(err); return null; });
        let cosignatures = [];
        incomingTxs.forEach(tx => {
            if (tx.type == TransactionType.TRANSFER) {
                const transferTx = <TransferTransaction>tx;
                const txMsg = <SsDocumentSigningMessage>HelperService.parseSsMessage(transferTx.message.payload);
                if (txMsg && (txMsg.header.messageType == SiriusMessageType.SIGN))
                    cosignatures.push(transferTx.signer.publicKey);
            }
        });
        cosignatures = cosignatures.filter(cosignature => cosignature != this.global.loggedAccount.publicKey);

        const doc = SiriusDocument.create(
            transaction.transactionInfo.id,
            info.body.fileName,
            info.body.fileHash,
            uploadDate,
            transaction.isConfirmed ? 'Confirmed' : 'Unconfirmed',
            transaction.signer.address.plain(),
            1,
            cosigners,
            cosignatures,
            null,
            docPublicAcc,
            transaction.transactionInfo.hash,
            info.body.uploadTxHash,
            info.body.isEncrypt
        );
        return doc;
    }

    /**
     * Fetch Transactions from network
     */
    fetchTransactions(fn?) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        let querryParams = null;
        if (this.lastId != '') {
            querryParams = new QueryParams(15, this.lastId);
        }

        accountHttp.transactions(this.global.loggedAccount.publicAccount, querryParams)
            .subscribe((transactions: any) => {
                console.log('Fetch from: ' + this.lastId);
                console.log(transactions);
                let last;
                if (transactions.length != 0) {
                    last = transactions[transactions.length - 1].transactionInfo.id;
                }
                this.isLast = (transactions.length < 10) || (last == this.lastId);
                this.lastId = last;
                console.log(this.lastId);
                transactions.forEach(async (transaction, index) => {
                    const isSiriusTransaction = this.checkSiriusTransactionType(transaction);
                    if (isSiriusTransaction) {
                        const doc = await this.createHistoryDocument(transaction);
                        this.add(doc);
                    }
                    if (index == (transactions.length - 1) && fn) { fn(); }
                });
            });
    }

    /**
     * Check if a transaction is Sirius Sign transaction
     * @param transaction 
     */
    checkSiriusTransactionType(transaction: Transaction) {
        if (transaction.type == TransactionType.TRANSFER) {
            let tx = <TransferTransaction>transaction;
            let info: SsDocumentSigningMessage;
            try {
                info = <SsDocumentSigningMessage>HelperService.parseSsMessage(tx.message.payload);
                console.log(info);
            }
            catch (err) {
                console.log(err);
                return false;
            }
            if (info && (info.header.appCode == this.global.appCodeName)) return true;
        }
        return false;
    }

    /**
     * Reload history document list
     */
    reloadHistory() {
        this.docs = [];
        this.lastId = '';
        this.isLast = false;
        this.documentInfo = [];

        const recursiveLoad = () => {
            if ((this.docs.length < 16) && (!this.isLast)) {
                this.fetchTransactions(recursiveLoad);
            }
        }

        recursiveLoad();
    }

    /**
     * Sort document in descending order of id/time
     */
    sortDocs() {
        this.docs.sort((a, b) => {
            if (a.id > b.id) return -1;
            else return 1;
        });
    }

    /**
     * Get document by ID
     * @param id 
     */
    getDocById(id) {
        let ids = this.documentInfo.map(doc => doc.id);
        let index = ids.indexOf(id);
        if (index != -1) return this.documentInfo[index];
        else throw new Error("No documents have the given id.")
    }
}