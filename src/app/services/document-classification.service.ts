import { Injectable } from '@angular/core';

import {
    PublicAccount,
    QueryParams,
    AccountHttp,
    Transaction,
    TransferTransaction,
    TransactionType,
    AggregateTransaction,
    AccountRestrictionsInfo,
    RestrictionType,
    Address,
    AccountInfo,
    TransactionHttp
} from 'tsjs-xpx-chain-sdk';

import { SiriusSignDocument } from '../model/siriussign-document.model';
import { SiriusMessageType, SsDocumentSigningMessage, SsNotifyDocumentCosignMessage } from '../model/siriussign-message.model';

import { GlobalService } from './global.service';
import { HelperService } from './helper.service';
import { SignatureInfo } from './draw-signature.service';

export enum DocumentSigningStatus {
    COMPLETED,
    WAITING,
    NEEDSIGN,
    ANY
}

export enum DocumentVerifyingStatus {
    VERIFIED,
    VERIFYING,
    NEEDVERIFY,
    ANY
}

@Injectable({
    providedIn: 'root'
})
export class DocumentClassificationService {

    completed: SiriusSignDocument[] = [];
    waiting: SiriusSignDocument[] = [];
    needSign: SiriusSignDocument[] = [];

    needVerify: SiriusSignDocument[] = [];
    verifying: SiriusSignDocument[] = [];
    verified: SiriusSignDocument[] = [];

    selectedDocInfo: SiriusSignDocument = null;
    selectedDocStatus: string;

    // Flags to prevent fetch method run multiple times
    isCompletedAndWaitingFetching: boolean = false;
    isNeedSignFetching: boolean = false;

    /** Transaction ID get from universal link */
    universalId: string = '';

    constructor(
        private global: GlobalService
    ) { }

    private fetchConfirmedTransactions(account: PublicAccount, lastId: string) {
        console.log('fetchIncomingTransactions: ' + lastId);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.transactions(account, querryParams).toPromise();
    }

    private fetchIncomingTransactions(account: PublicAccount, lastId: string) {
        console.log('fetchIncomingTransactions: ' + lastId);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.incomingTransactions(account, querryParams).toPromise();
    }

    private fetchOutgoingTransactions(account: PublicAccount, lastId: string) {
        console.log('fetchOutgoingTransactions: ' + lastId);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.outgoingTransactions(account, querryParams).toPromise();
    }

    /**
     * Fetch all incoming transations of an account
     * @param publicAccount 
     */
    async fetchAllIncomingTransactions(publicAccount: PublicAccount) {
        const allIncomingTxs = await this.fetchAll(publicAccount, true,
            (publicAccount: PublicAccount, lastId: string) => this.fetchIncomingTransactions(publicAccount, lastId),
            txs => { });
        return allIncomingTxs;
    }

    /**
     * Fetch all outgoing transations of an account
     * @param publicAccount 
     */
    async fetchAllOutgoingTransactions(publicAccount: PublicAccount) {
        const allOutgoingTxs = await this.fetchAll(publicAccount, true,
            (publicAccount: PublicAccount, lastId: string) => this.fetchOutgoingTransactions(publicAccount, lastId),
            txs => { });
        return allOutgoingTxs;
    }

    /**
     * Fetch all aggregate complete transaction of an account
     * @param publicAccount 
     */
    private async fetchAllAggregateCommpleteTransactions(publicAccount: PublicAccount) {
        let allAggCompleteTxs: AggregateTransaction[] = [];
        await this.fetchAll(publicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchConfirmedTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.AGGREGATE_COMPLETE)
                        allAggCompleteTxs = [...allAggCompleteTxs, <AggregateTransaction>tx];
                });
            });
        return allAggCompleteTxs;
    }

    /**
     * Fetch all transactions of a kind od transation according to fnFetch
     * @param publicAccount 
     * @param isReturnAll 
     * @param fnFetch 
     * @param fnEachFetch 
     */
    private async fetchAll(publicAccount: PublicAccount, isReturnAll: boolean, fnFetch, fnEachFetch) {
        let allTxs: Transaction[] = [];
        let lastId = null;
        let isLast = false;
        const loadAll = async () => {
            if (!isLast) {
                const txs: Transaction[] = await fnFetch(publicAccount, lastId)
                    .catch(err => { console.log(err); return null; });
                if (txs) {
                    if (txs.length > 0) lastId = txs[txs.length - 1].transactionInfo.id;
                    isLast = txs.length < 10;

                    fnEachFetch(txs);
                    if (isReturnAll) allTxs = [...allTxs, ...txs];
                }
                else {
                    isLast = true;
                }
                await loadAll();
            }
        }
        await loadAll();
        if (isReturnAll) return allTxs;
    }

    /**
     * Fetch transaction by hash
     * @param hash 
     */
    fetchTransaction(hash: string) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.getTransaction(hash).toPromise();
    }

    /**
     * Filter Document Signing Notifications from an array of Aggregate Commplete Transactions
     * @param aggCompleteTxs 
     */
    filterDocumentSigningNotifications(aggCompleteTxs: AggregateTransaction[]) {
        let documentSigningNotifications: AggregateTransaction[] = [];
        aggCompleteTxs.forEach(aggTx => {
            const checkTx = aggTx.innerTransactions[0];
            let isNotification = false;
            if (checkTx.type == TransactionType.TRANSFER) {
                const info = HelperService.parseSsMessage((<TransferTransaction>checkTx).message.payload);
                if (info && (info.header.appCode == this.global.appCodeName) && (info.header.messageType == SiriusMessageType.SIGN_NOTIFY))
                    isNotification = true;
            }

            if (isNotification) {
                documentSigningNotifications.push(aggTx);
            }
        });
        return documentSigningNotifications;
    }

    /**
     * Fetch all document signing notifications
     */
    async fetchDocumentSigningNotifications() {
        // Fetch all incomming transaction of user account
        let allAggCompleteTxs: AggregateTransaction[] = await this.fetchAllAggregateCommpleteTransactions(this.global.loggedAccount.publicAccount);
        // Filter document signing notificaion transaction - Aggregate Complete Tx with NOTIFY TransferTx inner
        const documentSigningNotifications = this.filterDocumentSigningNotifications(allAggCompleteTxs);
        return documentSigningNotifications;
    }

    /**
     * Fetch signer public key, address
     * @param address document address
     */
    async fetchDocumentSignersInfoFromDocumentAccount(address: Address) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const docAccRestriction: AccountRestrictionsInfo = await accountHttp.getAccountRestrictions(address).toPromise()
            .catch(err => { console.log(err); return null; });
        const docAccAllow = docAccRestriction.accountRestrictions.restrictions
            .filter(restriction => restriction.restrictionType == RestrictionType.AllowAddress)[0];
        const doccAccAllowAddresses = docAccAllow.values;

        // TODO: distinguish signers and verifiers

        const signersInfo = await accountHttp.getAccountsInfo(<Address[]>doccAccAllowAddresses).toPromise();
        const signers = signersInfo.map(signerInfo => { return { publicKey: signerInfo.publicKey, address: signerInfo.address }; });

        return signers;
    }

    /**
     * Fetch signer public key, address of cosigners and verifiers from agg noti tx
     * @param aggNotiTx 
     */
    async fetchDoucmentSignersAndVerifiersFromNotiTx(aggNotiTx: AggregateTransaction) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const notiTxs = aggNotiTx.innerTransactions.map(tx => <TransferTransaction>tx);

        const recvAddresses = notiTxs.map(tx => tx.recipient);
        const recvInfos = await accountHttp.getAccountsInfo(<Address[]>recvAddresses).toPromise();
        const recvs = recvInfos.map(info => { return { publicKey: info.publicKey, address: info.address }; })

        let cosignerAddresses: Address[] = [];
        let verifierAddresses: Address[] = [];
        notiTxs.forEach(tx => {
            const ssMsg = HelperService.parseSsMessage(tx.message.payload);
            if (ssMsg.header.messageType == SiriusMessageType.SIGN_NOTIFY) cosignerAddresses.push(<Address>tx.recipient);
            if (ssMsg.header.messageType == SiriusMessageType.VERYFY_NOTIFY) verifierAddresses.push(<Address>tx.recipient);
        });

        const cosigners = recvs.filter(recv => cosignerAddresses.map(addr => addr.plain()).includes(recv.address.plain()));
        const verifiers = recvs.filter(recv => verifierAddresses.map(addr => addr.plain()).includes(recv.address.plain()));

        return [cosigners, verifiers]
    }

    /**
     * Fetch document public account of address
     * @param address 
     */
    async fetchDocumentPublicAccount(address: Address) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const docAccInfo: AccountInfo = await accountHttp.getAccountInfo(address).toPromise()
            .catch(err => { console.log(err); return null; });
        const docAccPublicKey = docAccInfo.publicKey;
        const docPublicAcc = PublicAccount.createFromPublicKey(docAccPublicKey, this.global.networkType);
        return docPublicAcc;
    }


    /**
     * Get signatures info
     * @param notiAggTx 
     * @param signersInfo 
     * @param ownerPublicKey 
     * @param ownerSigningMsg 
     */
    getSignaturesInfo(
        notiAggTx: AggregateTransaction,
        signersInfo: { publicKey: string, address: Address }[],
        ownerPublicKey: string,
        ownerSigningMsg: SsDocumentSigningMessage
    ) {
        let signaturesInfo: SignatureInfo[] = [];
        if (notiAggTx) {
            signaturesInfo = notiAggTx.innerTransactions.filter(innerTx => {
                const notiTx = <TransferTransaction>innerTx;
                const notiMsg = HelperService.parseSsMessage(notiTx.message.payload);
                return notiMsg.header.messageType == SiriusMessageType.SIGN_NOTIFY;
            }).map(innerTx => {
                const notiTx = <TransferTransaction>innerTx;
                const notiMsg = <SsNotifyDocumentCosignMessage>HelperService.parseSsMessage(notiTx.message.payload);
                const publicKey = signersInfo.filter(signer => signer.address.equals(<Address>notiTx.recipient))[0].publicKey;

                const signatureInfo: SignatureInfo = {
                    publicKey: publicKey,
                    signaturePosition: {
                        name: notiMsg.body.signaturePosition.name,
                        page: notiMsg.body.signaturePosition.page,
                        x: notiMsg.body.signaturePosition.x,
                        y: notiMsg.body.signaturePosition.y,
                        namePosition: notiMsg.body.signaturePosition.namePosition
                    }
                }
                return signatureInfo;
            });
        }
        const ownerSignatureInfo: SignatureInfo = {
            publicKey: ownerPublicKey,
            signaturePosition: {
                name: ownerSigningMsg.body.signaturePosition.name,
                page: ownerSigningMsg.body.signaturePosition.page,
                x: ownerSigningMsg.body.signaturePosition.x,
                y: ownerSigningMsg.body.signaturePosition.y,
                namePosition: ownerSigningMsg.body.signaturePosition.namePosition
            }
        }
        signaturesInfo.push(ownerSignatureInfo);
        return signaturesInfo;
    }

    /**
     * Get signed info: signer pulickey, verified publickey, owner info, signature upload tx hash
     * from incoming transactions of a document account
     * @param incomingTxs 
     */
    getSignedInfo(incomingTxs: Transaction[]) {
        // Filter SIGN transactions to get signatures
        let signatures: string[] = [];  // Signed publickey
        let verifierSignatures: string[] = [];  // Verified publickey
        let owner: string;              // Owner public key
        let ownerSigningMsg: SsDocumentSigningMessage;
        let ownerSigningTx: TransferTransaction;
        let signaturesUploadTxHash: string[] = [];
        let userSigningTx: TransferTransaction = null;
        incomingTxs.forEach(tx => {
            if (tx.type == TransactionType.TRANSFER) {
                const transferTx = <TransferTransaction>tx;
                const txMsg = <SsDocumentSigningMessage>HelperService.parseSsMessage(transferTx.message.payload);
                if (txMsg && (txMsg.header.messageType == SiriusMessageType.SIGN)) {
                    signatures.push(transferTx.signer.publicKey);
                    signaturesUploadTxHash.push(txMsg.body.signatureUploadTxHash);
                    if (txMsg.body.isOwner) {
                        owner = transferTx.signer.publicKey;
                        ownerSigningMsg = txMsg;
                        ownerSigningTx = transferTx;
                    }
                    if (transferTx.signer.publicKey == this.global.loggedAccount.publicKey)
                        userSigningTx = transferTx;
                }
                if (txMsg && (txMsg.header.messageType == SiriusMessageType.VERIFY)) {
                    verifierSignatures.push(transferTx.signer.publicKey);
                }
            }
        });
        return {
            signatures: signatures,
            verifierSignatures: verifierSignatures,
            owner: owner,
            ownerSigningMsg: ownerSigningMsg,
            ownerSigningTx: ownerSigningTx,
            signaturesUploadTxHash: signaturesUploadTxHash,
            userSigningTx: userSigningTx
        }
    }

    /**
     * Fetch all document that need user to sign
     */
    async fetchNeedSignDocs() {
        if (this.isNeedSignFetching) return;
        this.isNeedSignFetching = true;
        let needSign: SiriusSignDocument[] = [];
        let needVerify: SiriusSignDocument[] = [];
        const notiAggTx: AggregateTransaction[] = await this.fetchDocumentSigningNotifications().catch(err => { console.log(err); return []; });
        const fetchNeedSign = async () => HelperService.asyncForEach(notiAggTx, async notiAggTx => {
            // notiAggTx.forEach(async notiAggTx => {
            //Fetch all incomming transactions of document account
            const docPublicAcc = await this.fetchDocumentPublicAccount(notiAggTx.signer.address);
            const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

            // Filter SIGN transactions to get signatures
            const signedInfo             = this.getSignedInfo(incomingTxs);
            const signatures             = signedInfo.signatures;
            const verifierSignatures     = signedInfo.verifierSignatures;
            const owner                  = signedInfo.owner;
            const ownerSigningMsg        = signedInfo.ownerSigningMsg;
            const signaturesUploadTxHash = signedInfo.signaturesUploadTxHash;

            // const userSignature = signatures.filter(signature => signature == this.global.loggedAccount.publicKey);
            // const isSigned = userSignature.length > 0;
            const isSigned = [owner, ...signatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);

            if (!isSigned) {
                // Check cosigners
                // const signersInfo = await this.fetchDocumentSignersInfo(notiAggTx.signer.address)
                // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
                let cosigners = [];
                let verifiers = [];
                let cosignerInfos = [];
                let verifierInfos = [];
                if (notiAggTx) {
                    [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
                    cosigners = cosignerInfos.map(info => info.publicKey);
                    verifiers = verifierInfos.map(info => info.publicKey);
                }
                const cosignatures = signatures.filter(cosignature => cosignature != owner);

                const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

                const doc = SiriusSignDocument.create(
                    notiAggTx.transactionInfo.id,
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileHash,
                    new Date(notiAggTx.deadline.value.plusHours(-2).toString()),
                    owner,
                    isSigned,
                    null,
                    cosigners,
                    cosignatures,
                    notiAggTx.signer,
                    null,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.isEncrypt,
                    signaturesInfo,
                    signaturesUploadTxHash,
                    verifiers,
                    verifierSignatures,
                    null
                );

                const isNeedSign = cosigners.includes(this.global.loggedAccount.publicKey);
                const isNeedVerify = verifiers.includes(this.global.loggedAccount.publicKey);
                if (isNeedSign) needSign.push(doc);
                if (isNeedVerify) needVerify.push(doc);
            }
        });
        await fetchNeedSign();
        // this.needSign = needSign;
        // this.needSign = [];
        // needSign.forEach((doc, i) => setTimeout(() => this.needSign.push(doc), 200 * i));
        this.isNeedSignFetching = false;
        return [needSign, needVerify];
    }

    async fetchAllDocumentSigningTransactions() {
        let allDocSigningTxs: TransferTransaction[] = [];
        await this.fetchAll(this.global.loggedAccount.publicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchOutgoingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.TRANSFER) {
                        const info = HelperService.parseSsMessage((<TransferTransaction>tx).message.payload);
                        if (info && (info.header.appCode == this.global.appCodeName) &&
                            ((info.header.messageType == SiriusMessageType.SIGN) || (info.header.messageType == SiriusMessageType.VERIFY))) {
                            allDocSigningTxs = [...allDocSigningTxs, <TransferTransaction>tx];
                        }
                    }
                });
            })
        return allDocSigningTxs;
    }

    async fetchCompletedAndWaiting() {
        if (this.isCompletedAndWaitingFetching) return;
        this.isCompletedAndWaitingFetching = true;
        let waiting: SiriusSignDocument[] = [];
        let completed: SiriusSignDocument[] = [];
        let verifying: SiriusSignDocument[] = [];
        let verified: SiriusSignDocument[] = [];
        const signTxs = await this.fetchAllDocumentSigningTransactions();
        const fetchDocuments = async () => {
            await HelperService.asyncForEach(signTxs, async (tx: TransferTransaction) => {
                const info = <SsDocumentSigningMessage>HelperService.parseSsMessage((<TransferTransaction>tx).message.payload);
                const docPublicAcc = await this.fetchDocumentPublicAccount(<Address>tx.recipient);

                // Fetch noti agg transaction
                const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
                const docAggTxs = docOutgoingTxs
                    .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
                    .map(aggTx => <AggregateTransaction>aggTx);
                const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);

                // Fetch cosigners and verifiers
                // const signersInfo = await this.fetchDocumentSignersInfoFromDocumentAccount(<Address>tx.recipient);
                const notiAggTx = notiAggTxs[0];
                let cosigners = [];
                let verifiers = [];
                let cosignerInfos = [];
                let verifierInfos = [];
                if (notiAggTx) {
                    [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
                    cosigners = cosignerInfos.map(info => info.publicKey);
                    verifiers = verifierInfos.map(info => info.publicKey);
                }

                // if (signersInfo.length == 1) {  //Only me sign the document
                // if (cosigners.length == 0) { //Only me sign the document
                //     const signatureInfo: SignatureInfo = {
                //         publicKey: tx.signer.publicKey,
                //         signaturePosition: info.body.signaturePosition
                //     }

                //     let uploadDate = new Date(tx.deadline.value.plusHours(-2).toString());
                //     const now = Date.now();
                //     const millis = now - uploadDate.getTime();
                //     if (millis < 0 && info.meta['timestamp']) uploadDate = new Date(info.meta['timestamp']);

                //     const doc = SiriusSignDocument.create(
                //         tx.transactionInfo.id,
                //         info.body.fileName,
                //         info.body.fileHash,
                //         uploadDate,
                //         tx.signer.publicKey,
                //         true,
                //         uploadDate,
                //         [],
                //         [],
                //         docPublicAcc,
                //         tx.transactionInfo.hash,
                //         info.body.uploadTxHash,
                //         info.body.isEncrypt,
                //         [signatureInfo],
                //         [info.body.signatureUploadTxHash],
                //         [],
                //         []
                //     );
                //     completed.push(doc);
                // }
                // else {  //I am a cosigner of this document
                //Fetch all incomming transactions of document account
                const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

                // Filter SIGN and VERIFY transactions to get signatures
                const signedInfo             = this.getSignedInfo(incomingTxs);
                const signatures             = signedInfo.signatures;
                const verifierSignatures     = signedInfo.verifierSignatures;
                const owner                  = signedInfo.owner;
                const ownerSigningTx         = signedInfo.ownerSigningTx;
                const ownerSigningMsg        = signedInfo.ownerSigningMsg;
                const signaturesUploadTxHash = signedInfo.signaturesUploadTxHash;

                // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
                const cosignatures = signatures.filter(cosignature => cosignature != owner);
                // const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
                // const docAggTxs = docOutgoingTxs
                //     .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
                //     .map(aggTx => <AggregateTransaction>aggTx);
                // const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
                // if (notiAggTxs.length != 1) return;

                // Get signatures info
                // const notiAggTx = notiAggTxs[0];
                const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

                let uploadDate = new Date(ownerSigningTx.deadline.value.plusHours(-2).toString());
                const now = Date.now();
                let millis = now - uploadDate.getTime();
                if (millis < 0 && ownerSigningMsg.meta['timestamp']) uploadDate = new Date(info.meta['timestamp']);

                let signDate = new Date(tx.deadline.value.plusHours(-2).toString());
                millis = now - uploadDate.getTime();
                if (millis < 0 && info.meta['timestamp']) uploadDate = new Date(info.meta['timestamp']);

                const isSigned = [owner, ...cosignatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);

                const doc = SiriusSignDocument.create(
                    tx.transactionInfo.id,
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    isSigned,
                    signDate,
                    cosigners,
                    cosignatures,
                    docPublicAcc,
                    tx.transactionInfo.hash,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.isEncrypt,
                    signaturesInfo,
                    signaturesUploadTxHash,
                    verifiers,
                    verifierSignatures,
                    null
                );
                const isSigner = [owner, ...cosigners].includes(this.global.loggedAccount.publicKey);
                const isVerifier = verifiers.includes(this.global.loggedAccount.publicKey);
                if (isSigner) {
                    if (cosignatures.length == cosigners.length) completed.push(doc);
                    else waiting.push(doc);
                }
                if (isVerifier) {
                    if (verifierSignatures.length == verifiers.length) verified.push(doc);
                    else verifying.push(doc);
                }
                // }
            });
        }
        await fetchDocuments();
        this.waiting = waiting;
        this.completed = completed;
        this.verifying = verifying;
        this.verified = verified;
        // this.waiting = [];
        // this.completed = [];
        // waiting.forEach((doc, i) => setTimeout(() => this.waiting.push(doc), 200 * i));
        // completed.forEach((doc, i) => setTimeout(() => this.completed.push(doc), 200 * i));
        this.isCompletedAndWaitingFetching = false;
    }

    /**
     * Fetch a Sirius Sign document by signing transction hash
     * @param hash 
     */
    async fetchDocumentBySigningTxHash(hash: string) {
        const signTx: Transaction = await this.fetchTransaction(hash).catch(err => { return null; });
        if (!signTx) return null;
        if (signTx.type != TransactionType.TRANSFER) return null;
        const tx = <TransferTransaction>signTx;
        const info = <SsDocumentSigningMessage>HelperService.parseSsMessage(tx.message.payload);
        const docPublicAcc = await this.fetchDocumentPublicAccount(<Address>tx.recipient);

        // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
        const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
        const docAggTxs = docOutgoingTxs
            .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
            .map(aggTx => <AggregateTransaction>aggTx);
        const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
        // const signersInfo = await this.fetchDocumentSignersInfo(<Address>tx.recipient);
        console.log(notiAggTxs);
        const notiAggTx = notiAggTxs[0];
        let cosigners = [];
        let verifiers = [];
        let cosignerInfos = [];
        let verifierInfos = [];
        if (notiAggTx) {
            [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
            cosigners = cosignerInfos.map(info => info.publicKey);
            verifiers = verifierInfos.map(info => info.publicKey);
        }
        // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
        const docIncomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);
        const signedInfo = this.getSignedInfo(docIncomingTxs);

        const signatures             = signedInfo.signatures;
        const verifierSignatures     = signedInfo.verifierSignatures;
        const owner                  = signedInfo.owner;
        const ownerSigningTx         = signedInfo.ownerSigningTx;
        const ownerSigningMsg        = signedInfo.ownerSigningMsg;
        const signaturesUploadTxHash = signedInfo.signaturesUploadTxHash;

        // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
        const cosignatures = signatures.filter(cosignature => cosignature != owner);

        // Get signatures info
        const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

        let uploadDate = new Date(ownerSigningTx.deadline.value.plusHours(-2).toString());
        const now = Date.now();
        let millis = now - uploadDate.getTime();
        if (millis < 0 && ownerSigningMsg.meta['timestamp']) uploadDate = new Date(info.meta['timestamp']);

        let signDate = new Date(tx.deadline.value.plusHours(-2).toString());
        millis = now - uploadDate.getTime();
        if (millis < 0 && info.meta['timestamp']) uploadDate = new Date(info.meta['timestamp']);

        const isSigned = [owner, ...cosignatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);

        const doc = SiriusSignDocument.create(
            tx.transactionInfo.id,
            ownerSigningMsg.body.fileName,
            ownerSigningMsg.body.fileHash,
            uploadDate,
            owner,
            isSigned,
            signDate,
            cosigners,
            cosignatures,
            docPublicAcc,
            tx.transactionInfo.hash,
            ownerSigningMsg.body.uploadTxHash,
            ownerSigningMsg.body.isEncrypt,
            signaturesInfo,
            signaturesUploadTxHash,
            verifiers,
            verifierSignatures,
            null
        );
        return doc;
    }

    /**
     * Fetch a Sirius Sign document by signing transction hash
     * @param hash 
     */
    async fetchDocumentByDocumentAccount(documentPublicKey: string) {
        const docPublicAcc = PublicAccount.createFromPublicKey(documentPublicKey, this.global.networkType);

        // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
        const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
        const docAggTxs = docOutgoingTxs
            .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
            .map(aggTx => <AggregateTransaction>aggTx);
        const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
        // const signersInfo = await this.fetchDocumentSignersInfo(<Address>tx.recipient);
        console.log(notiAggTxs);
        const notiAggTx = notiAggTxs[0];
        let cosigners = [];
        let verifiers = [];
        let cosignerInfos = [];
        let verifierInfos = [];
        if (notiAggTx) {
            [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
            cosigners = cosignerInfos.map(info => info.publicKey);
            verifiers = verifierInfos.map(info => info.publicKey);
        }
        // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
        const docIncomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);
        const signedInfo = this.getSignedInfo(docIncomingTxs);

        const signatures             = signedInfo.signatures;
        const verifierSignatures     = signedInfo.verifierSignatures;
        const owner                  = signedInfo.owner;
        const ownerSigningTx         = signedInfo.ownerSigningTx;
        const ownerSigningMsg        = signedInfo.ownerSigningMsg;
        const signaturesUploadTxHash = signedInfo.signaturesUploadTxHash;
        const userSigningTx          = signedInfo.userSigningTx;

        // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
        const cosignatures = signatures.filter(cosignature => cosignature != owner);

        // Get signatures info
        const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

        let uploadDate = new Date(ownerSigningTx.deadline.value.plusHours(-2).toString());
        const now = Date.now();
        let millis = now - uploadDate.getTime();
        if (millis < 0 && ownerSigningMsg.meta['timestamp']) uploadDate = new Date(ownerSigningMsg.meta['timestamp']);

        let signDate = new Date(ownerSigningTx.deadline.value.plusHours(-2).toString());
        millis = now - uploadDate.getTime();
        if (millis < 0 && ownerSigningMsg.meta['timestamp']) uploadDate = new Date(ownerSigningMsg.meta['timestamp']);

        const isSigned = [owner, ...cosignatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);

        const doc = SiriusSignDocument.create(
            ownerSigningTx.transactionInfo.id,
            ownerSigningMsg.body.fileName,
            ownerSigningMsg.body.fileHash,
            uploadDate,
            owner,
            isSigned,
            signDate,
            cosigners,
            cosignatures,
            docPublicAcc,
            (userSigningTx) ? userSigningTx.transactionInfo.hash : null,
            ownerSigningMsg.body.uploadTxHash,
            ownerSigningMsg.body.isEncrypt,
            signaturesInfo,
            signaturesUploadTxHash,
            verifiers,
            verifierSignatures,
            null
        );
        return doc;
    }

    /**
     * Classify a fetched Sirius Sign Document
     * @param document 
     */
    classify(document: SiriusSignDocument) {
        const isVerifier = document.verifiers.includes(this.global.loggedAccount.publicKey);
        if (isVerifier) {
            if (!document.isSigned) this.needVerify.push(document);
            else if (document.verifierSignatures.length < document.verifiers.length) this.verifying.push(document);
            if (document.verifierSignatures.length == document.verifiers.length) this.verified.push(document);
        }
        else {
            if (!document.isSigned) this.needSign.push(document);
            else if (document.cosignatures.length < document.cosigners.length) this.waiting.push(document);
            if (document.cosignatures.length == document.cosigners.length) this.completed.push(document);
        }
    }

    /**
     * Get document by Id
     * @param id
     */
    getDocById(id: string, flag: DocumentSigningStatus) {
        const documents =
            (flag == DocumentSigningStatus.COMPLETED) ? this.completed :
                (flag == DocumentSigningStatus.WAITING) ? this.waiting :
                    (flag == DocumentSigningStatus.NEEDSIGN) ? this.needSign :
                        [...this.completed, ...this.waiting, ...this.needSign];
        let ids = documents.map(doc => doc.id);
        let index = ids.indexOf(id);
        if (index != -1) return documents[index];
        else throw new Error("No documents have the given id.")
    }

    /**
     * Get document by index
     * @param index
     */
    getDocByIndex(index: number, flag: DocumentSigningStatus) {
        const documents =
            (flag == DocumentSigningStatus.COMPLETED) ? this.completed :
                (flag == DocumentSigningStatus.WAITING) ? this.waiting :
                    (flag == DocumentSigningStatus.NEEDSIGN) ? this.needSign :
                        [...this.completed, ...this.waiting, ...this.needSign];
        if (index <= documents.length) return documents[index];
        else throw new Error("Out of range index.")
    }

    /**
     * Get document by Document account public key
     * @param publicKey
     */
    getDocByDocumentAccount(publicKey: string, flag: DocumentSigningStatus) {
        const documents =
            (flag == DocumentSigningStatus.COMPLETED) ? this.completed :
                (flag == DocumentSigningStatus.WAITING) ? this.waiting :
                    (flag == DocumentSigningStatus.NEEDSIGN) ? this.needSign :
                        [...this.completed, ...this.waiting, ...this.needSign];
        let publicKeys = documents.map(doc => doc.documentAccount.publicKey);
        let index = publicKeys.indexOf(publicKey);
        if (index != -1) return documents[index];
        else throw new Error("No documents have the given id.")
    }

    /**
     * Get document by index
     * @param index
     */
    getVerifyDocByIndex(index: number, flag: DocumentVerifyingStatus) {
        const documents =
            (flag == DocumentVerifyingStatus.VERIFIED) ? this.verified :
                (flag == DocumentVerifyingStatus.VERIFYING) ? this.verifying :
                    (flag == DocumentVerifyingStatus.NEEDVERIFY) ? this.needVerify :
                        [...this.verified, ...this.verifying, ...this.needVerify];
        if (index <= documents.length) return documents[index];
        else throw new Error("Out of range index.")
    }

    /**
     * Sort document in descending order of id/time
     */
    sortDocs(flag: 'completed' | 'waiting' | 'needSign' | 'all' | 'compledAndWaiting') {
        const sortCondition = (a: SiriusSignDocument, b: SiriusSignDocument) => {
            if (a.id > b.id) return -1;
            else return 1;
        }
        if (flag == 'all' || flag == 'needSign')
            this.needSign.sort((a, b) => sortCondition(a, b));
        if (flag == 'all' || flag == 'waiting' || flag == 'compledAndWaiting')
            this.waiting.sort((a, b) => sortCondition(a, b));
        if (flag == 'all' || flag == 'completed' || flag == 'compledAndWaiting')
            this.completed.sort((a, b) => sortCondition(a, b));
    }

    /**
     * Clear document lists
     */
    clearDocs() {
        this.completed = [];
        this.waiting = [];
        this.needSign = [];
        this.verified = [];
        this.verifying = [];
        this.needVerify = [];
    }
}
