import { Injectable } from '@angular/core';

import { SiriusSignSigningTask, SiriusSignTask } from '../model/siriussign-task.model';

@Injectable({
    providedIn: 'root'
})
export class MultitaskService {

    selectedTask: SiriusSignTask = null;
    tasks: SiriusSignTask[] = [];

    constructor() { }
}
