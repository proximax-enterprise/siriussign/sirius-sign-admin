import { Injectable } from '@angular/core';
import { Stripe } from '@ionic-native/stripe/ngx';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { LoadingProcessService } from './loading-process.service';

@Injectable({
    providedIn: 'root'
})
export class BuyCryptoService {
    // host = 'http://localhost:4242';
    host = 'http://159.65.82.226:4242';
    isDone: boolean;
    isFree: boolean;
    tokenID: any;
    usdAmount: number;
    xpxAmount: number;
    stripeKey = 'pk_test_UVIcjJ0qYLtzM7NZ8KanimqN00ScilA6SK';
    rate: number = 0;
    bankAccount: any = {};
    test: any = {};
    currency: any = 'USD';

    cardInfo = {
        email: '',
        nameOfCard: '',
        country: '',
        number: '',
        expMonth: 12,
        expYear: 2020,
        cvc: ''
    };
    // cardInfo: any = {
    //   email : '',
    //   nameOfCard: '',
    //   cardNumber: '4242424242424242',
    //   expYearCard: 2022,
    //   expMonthCard: 12,
    //   country: '',
    //   cvc: '',
    // };
    cardDetails: any = {};
    constructor(
        private stripe: Stripe,
        private http: HttpClient,
        private globalService: GlobalService,
        private loadingProcess: LoadingProcessService,
    ) { }
    public setUsdAmount(email: string) {
        this.cardInfo.email = email;
    }
    public getUsdAmount() {
        return this.cardInfo.email;
    }
    public setXpxAmount(email: string) {
        this.cardInfo.email = email;
    }
    public getXpxAmount() {
        return this.cardInfo.email;
    }
    public setEmail(email: string) {
        this.cardInfo.email = email;
    }
    public getEmail() {
        return this.cardInfo.email;
    }
    public setCardNumber(cardNumber: string) {
        this.cardInfo.number = cardNumber;
    }
    public getCardNumber() {
        return this.cardInfo.number;
    }
    public setExpYearCard(expYear: number) {
        this.cardInfo.expYear = expYear;
    }
    public getExpYearCard() {
        return this.cardInfo.expYear;
    }
    public setExpMonthCard(expMonth: number) {
        this.cardInfo.expMonth = expMonth;
    }
    public getExpMonthCard() {
        return this.cardInfo.expMonth;
    }
    public setCVC(cvc: string) {
        this.cardInfo.cvc = cvc;
    }
    public getCVC() {
        return this.cardInfo.cvc;
    }
    public setNameOfCard(name: string) {
        this.cardInfo.nameOfCard = name;
    }
    public getNameOfCard() {
        return this.cardInfo.nameOfCard;
    }

    async createStripe() {
        const result = await fetch(this.host + '/stripe/stripe-key');
        console.log("Fetched Stripe Key");
        console.log(result);
        const data = await result.json();
        console.log(data);
        this.stripeKey = data.publicKey;
    }

    async getRate() {
        const result = await fetch(this.host + '/stripe/exchange');
        console.log("Fetched Rate");
        console.log(result);
        const data = await result.json();
        console.log(data);
        this.rate = (data.rate as number);
        return this.rate;
    }

    async payWithStripe(publickey): Promise<any> {
        await this.stripe.setPublishableKey(this.stripeKey)
            .catch(e => {
                console.log(e);
            });

        const token = await this.stripe.createCardToken(this.cardInfo);

        console.log('test Hereeeeeeeeeeeeeeeee');
        console.log(token);
        this.tokenID = token.id;
        console.log(token.id);
        console.log('*** Create token Success ***');
        let orderData = {
            currency: "usd",
            token: null,
            publicKey: publickey,
            usdAmount: this.usdAmount,
            xpxAmount: 0
        };
        orderData.token = token.id;

        const result = await fetch(this.host + "/stripe/pay", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(orderData)
        });
        // this.makePayment(token.id);
        // console.log(this.makePayment(token.id));
        // return(token);
        console.log('*** POST response ***');
        console.log(result);
        const data = await result.json();
        console.log(data);
        return data;
    }

    tryFree() {
        let orderData = {
            publicKey: this.globalService.loggedAccount.publicKey,
        };

        return fetch(this.host + "/xpx/free", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(orderData)
        }).then(result => {
            return result.json();
        });
    }

    ////////////////////////////
    onSuccess(tokenID) {
        console.log('Got token' + tokenID);
    }
    onError(errorMessage) {
        console.log('Error get card token' + errorMessage);
    }

    bankAccounts() {
        this.bankAccount = {
            routing_number: '11000000',
            account_number: '000123456789',
            account_holder_name: 'John Smith', // optional
            account_holder_type: 'individual', // optional
            currency: 'CAD',
            country: 'CA'
        };

        this.stripe.createBankAccountToken(this.bankAccount)
            .then(ac => {
                console.log('test1 Hereeeeee');
                this.test = ac;
                console.log(this.test.created);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    validate() {
        this.stripe.validateCardNumber(this.cardDetails.number)
            .then((va) => {
                console.log(va);
            })
            .catch((e) => {
                console.log(e);

            });
    }

    postInfoPayment(token) {
        this.http
            .post('https://us-central1-shoppr-c97a7.cloudfunctions.net/payWithStripe', {
                token: token.id
            })
            .subscribe(data => {
                console.log(data);
            });
    }
    getInfoPayment(token) {
        this.http
            .get('https://us-central1-shoppr-c97a7.cloudfunctions.net/payWithStripe', {
            })
            .subscribe(data => {
                console.log(data);
            });
    }
}
