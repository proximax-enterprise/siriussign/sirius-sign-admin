import { SiriusSignDocument } from './../model/siriussign-document.model';
import { HelperService } from './helper.service';
import { Injectable } from '@angular/core';

import {
    Account,
    Deadline,
    TransferTransaction,
    PlainMessage,
    TransactionHttp,
    SignedTransaction,
    Address,
    UInt64,
    PublicAccount,
    AggregateTransaction,
    RestrictionModificationType,
    AccountRestrictionModification,
    AccountAddressRestrictionModificationTransaction,
    RestrictionType,
} from 'tsjs-xpx-chain-sdk';

import {
    SsDocumentVerifyMessage,
    SsDocumentSigningMessage,
    SsNotifyDocumentCosignMessage,
    SsNotifyDocumentVerifyMessage,
    SiriusMessageType
} from './../model/siriussign-message.model';

import { SignatureInfo, VerifyInfo } from './draw-signature.service';
import { GlobalService } from './global.service';
import { SignDocument } from './sign-document.service';

@Injectable({
    providedIn: 'root'
})
export class SigningWithoutMultisigService {

    constructor(
        private global: GlobalService
    ) { }

    /**
     * Generate an account for a document
     */
    createDocumentAccount() {
        return Account.generateNewAccount(this.global.networkType);
    }

    /**
     * Create account restriction transaction to allow signers and verifiers to send transaction to document account
     * @param document 
     */
    createRestriction(document: SignDocument) {
        const signers = [this.global.loggedAccount.publicKey, ...document.cosigners, ...document.verifiers];
        const restrictionMods = signers.map(signer => {
            return AccountRestrictionModification.createForAddress(RestrictionModificationType.Add, Address.createFromPublicKey(signer, this.global.networkType));
        });
        console.log(restrictionMods);
        const accountRestrictionTransaction = AccountAddressRestrictionModificationTransaction.create(
            Deadline.create(),
            RestrictionType.AllowAddress,
            restrictionMods,
            this.global.networkType,
            new UInt64([0, 0])
        );
        console.log(accountRestrictionTransaction);
        const signedAccountRestrictionTransaction = document.documentAcc.sign(accountRestrictionTransaction, this.global.networkGenerationHash);
        console.log(signedAccountRestrictionTransaction);
        return signedAccountRestrictionTransaction;
    }

    /**
     * Create SIGN transaction to sign a document
     * @param document 
     * @param isOwner 
     * @param signatureInfo 
     */
    createDocSigningTransaction(document: SignDocument | SiriusSignDocument, isOwner: boolean, signatureInfo: SignatureInfo) {
        let fileHash: string;
        let fileName: string;
        let signatureUploadTxHash: string;
        let documentAcc: PublicAccount;
        if (document instanceof SiriusSignDocument) {
            const ssDoc = <SiriusSignDocument>document;
            fileHash = ssDoc.fileHash.sha256Hash;
            fileName = ssDoc.name;
            signatureUploadTxHash = ssDoc.signaturesUploadTxHash[ssDoc.signaturesUploadTxHash.length - 1];
            documentAcc = ssDoc.documentAccount;
        }
        else {
            fileHash = document.fileHash;
            fileName = document.file.name;
            signatureUploadTxHash = document.signatureUploadTxHash;
            documentAcc = document.documentAcc.publicAccount;
        }
        const ssMessage = new SsDocumentSigningMessage(
            this.global.appCodeName,
            this.global.appVersion,
            fileHash,
            fileName,
            isOwner,
            document.uploadTxHash,
            document.isEncrypt,
            signatureUploadTxHash,
            signatureInfo.signaturePosition.x,
            signatureInfo.signaturePosition.y,
            signatureInfo.signaturePosition.page,
            signatureInfo.signaturePosition.name,
            signatureInfo.signaturePosition.namePosition
        );

        const message = PlainMessage.create(ssMessage.toMessage());

        const docSigningTransaction = TransferTransaction.create(
            Deadline.create(),
            documentAcc.address,
            [],
            message,
            this.global.networkType,
            new UInt64([0, 0])
        );

        const networkGenerationHash = this.global.networkGenerationHash;
        const signedDocSigningTransaction = this.global.loggedAccount.sign(docSigningTransaction, networkGenerationHash);

        return signedDocSigningTransaction;
    }

    createDocSigningTransactionWithNamespace(document: SignDocument | SiriusSignDocument, isOwner: boolean, signatureInfo: SignatureInfo, namespaceAcc: Account) {
        let fileHash: string;
        let fileName: string;
        let signatureUploadTxHash: string;
        let documentAcc: PublicAccount;
        if (document instanceof SiriusSignDocument) {
            const ssDoc = <SiriusSignDocument>document;
            fileHash = ssDoc.fileHash.sha256Hash;
            fileName = ssDoc.name;
            signatureUploadTxHash = ssDoc.signaturesUploadTxHash[ssDoc.signaturesUploadTxHash.length - 1];
            documentAcc = ssDoc.documentAccount;
        }
        else {
            fileHash = document.fileHash;
            fileName = document.file.name;
            signatureUploadTxHash = document.signatureUploadTxHash;
            documentAcc = document.documentAcc.publicAccount;
        }
        const ssMessage = new SsDocumentSigningMessage(
            this.global.appCodeName,
            this.global.appVersion,
            fileHash,
            fileName,
            isOwner,
            document.uploadTxHash,
            document.isEncrypt,
            signatureUploadTxHash,
            signatureInfo.signaturePosition.x,
            signatureInfo.signaturePosition.y,
            signatureInfo.signaturePosition.page,
            signatureInfo.signaturePosition.name,
            signatureInfo.signaturePosition.namePosition
        );

        const message = PlainMessage.create(ssMessage.toMessage());

        const docSigningTransaction = TransferTransaction.create(
            Deadline.create(),
            documentAcc.address,
            [],
            message,
            this.global.networkType,
            new UInt64([0, 0])
        );

        const networkGenerationHash = this.global.networkGenerationHash;
        const signedDocSigningTransaction = namespaceAcc.sign(docSigningTransaction, networkGenerationHash);

        return signedDocSigningTransaction;
    }

    /**
     * Create aggregate complete transaction that contains NOTIFY transfer transaction to cosigners and verifiers
     * @param document 
     * @param signersSignaturesInfo 
     */
    createMultiSigningNotificationTransaction(
        document: SignDocument,
        signersSignaturesInfo: SignatureInfo[],
    ) {
        const cosignersSignaturePositions = signersSignaturesInfo.filter(
            signer => signer.publicKey != this.global.loggedAccount.publicKey
        );

        // Create cosign notify
        const ssCosignMessages = cosignersSignaturePositions.map((cosigner, index) => {
            return new SsNotifyDocumentCosignMessage(
                this.global.appCodeName,
                this.global.appVersion,
                document.uploadTxHash,
                cosigner.signaturePosition.x,
                cosigner.signaturePosition.y,
                cosigner.signaturePosition.page,
                cosigner.signaturePosition.name,
                cosigner.signaturePosition.namePosition
            );
        });

        const messages = ssCosignMessages.map(ssMessage => {
            return PlainMessage.create(ssMessage.toMessage());
        });

        const aggCosignNotificationTransactions = messages.map((message, index) => {
            const cosignerPublicKey = cosignersSignaturePositions[index].publicKey;
            return TransferTransaction.create(
                Deadline.create(),
                Address.createFromPublicKey(cosignerPublicKey, this.global.networkType),
                [],
                message,
                this.global.networkType,
                new UInt64([0, 0])
            ).toAggregate(document.documentAcc.publicAccount);
        });

        // Create verify notify
        const ssVerifyMessage = new SsNotifyDocumentVerifyMessage(
            this.global.appCodeName,
            this.global.appVersion,
            document.uploadTxHash,
        );

        const verifyMessage = PlainMessage.create(ssVerifyMessage.toMessage());

        const aggVerifiedNotificationTransactions = document.verifiers.map(verifier =>
            TransferTransaction.create(
                Deadline.create(),
                Address.createFromPublicKey(verifier, this.global.networkType),
                [],
                verifyMessage,
                this.global.networkType,
                new UInt64([0, 0])
            ).toAggregate(document.documentAcc.publicAccount)
        );

        // Notify transactions
        const aggNotificationTransactions = [...aggCosignNotificationTransactions, ...aggVerifiedNotificationTransactions];
        const aggTransaction = AggregateTransaction.createComplete(
            Deadline.create(),
            aggNotificationTransactions,
            this.global.networkType,
            [],
            new UInt64([0, 0])
        );

        const networkGenerationHash = this.global.networkGenerationHash;
        const signedAggTransaction = document.documentAcc.sign(aggTransaction, networkGenerationHash);
        return signedAggTransaction;
    }

    /**
     * Announce a transaction to network
     * @param signedTx
     */
    announceTransaction(signedTx: SignedTransaction) {
        // const transactionHttp = new TransactionHttp(this.global.apiNode);
        const transactionHttp = new TransactionHttp('https://demo-sc-api-1.ssi.xpxsirius.io');
        transactionHttp
            .announce(signedTx)
            .subscribe(
                txAnnounceRes => {
                    console.log('[TransactionHttp] Announced Transaction');
                    console.log(signedTx.hash);
                },
                err => console.log('[TransactionHttp] Announce Error' + err),
                () => console.log('[TransactionHttp] Announce Done')
            );
    }

    /**
     * Fetch a transaction by its hash
     * @param hash 
     */
    fetchTransaction(hash: string) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.getTransaction(hash).toPromise();
    }
}
