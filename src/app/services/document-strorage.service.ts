import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { SiriusSignDocument } from './../model/siriussign-document.model';

import { GlobalService } from './global.service';

@Injectable({
    providedIn: 'root'
})
export class DocumentStorageService {

    loggedAccount: string;
    queryKey: string;
    querykeyNamespace: string;

    constructor(
        private storage: Storage,
        private global: GlobalService
    ) { }

    setAccount() {
        this.loggedAccount = this.global.loggedWallet.name;
        this.queryKey = 'ssdocs-' + this.loggedAccount;
    }
    setqueryKeyNamespace(namespaceName: string) {
        this.querykeyNamespace = 'ssAdmin-' + namespaceName;
    }

    /**
     * Store a new sirius sign document
     * @param document 
     */
    async storeDocument(document: SiriusSignDocument) {
        const documents = await this.storage.get(this.queryKey)
            .catch(err => {
                return null;
            });
        if (!documents) return this.storage.set(this.queryKey, [document]);
        let addedDocuments = [...documents, document];
        const sortCondition = (a: SiriusSignDocument, b: SiriusSignDocument) => {
            if (a.id > b.id) return -1;
            else return 1;
        }
        addedDocuments = addedDocuments.sort((a, b) => sortCondition(a, b));
        return this.storage.set(this.queryKey, addedDocuments);
    }

    // Store document with namespace

    async storeDocumentWithNamespace(document: SiriusSignDocument, querykeyDoc: string) {
        const documents = await this.storage.get(querykeyDoc)
            .catch(err => {
                return null;
            });
        if (!documents) return this.storage.set(querykeyDoc, [document]);
        let addedDocuments = [...documents, document];
        const sortCondition = (a: SiriusSignDocument, b: SiriusSignDocument) => {
            if (a.id > b.id) return -1;
            else return 1;
        }
        addedDocuments = addedDocuments.sort((a, b) => sortCondition(a, b));
        return this.storage.set(querykeyDoc, addedDocuments);
    }

    /**
     * Fetch documents from storage
     */
    async fetchDocument() {
        const documents: any[] = await this.storage.get(this.queryKey)
            .catch(err => {
                return [];
            });
        if (!documents) return [];
        const documentObjs = documents.map((doc: SiriusSignDocument) => {
            return SiriusSignDocument.create(
                doc.id,
                doc.name,
                doc.fileHash.sha256Hash,
                new Date(doc.uploadDate),
                doc.owner,
                doc.isSigned,
                new Date(doc.signDate),
                doc.cosigners,
                doc.cosignatures,
                doc.documentAccount,
                doc.signTxHash,
                doc.uploadTxHash,
                doc.isEncrypt,
                doc.signatures,
                doc.signaturesUploadTxHash,
                doc.verifiers,
                doc.verifierSignatures,
                doc.localUrl
            );
        });
        return documentObjs;
    }

    //////////// get doc with namespace ////////////////////
    async fetchDocumentWithSubNamespace(name: string) {
        const documents: any[] = await this.storage.get(name)
            .catch(err => {
                return [];
            });
        if (!documents) return [];
        const documentObjs = documents.map((doc: SiriusSignDocument) => {
            return SiriusSignDocument.create(
                doc.id,
                doc.name,
                doc.fileHash.sha256Hash,
                new Date(doc.uploadDate),
                doc.owner,
                doc.isSigned,
                new Date(doc.signDate),
                doc.cosigners,
                doc.cosignatures,
                doc.documentAccount,
                doc.signTxHash,
                doc.uploadTxHash,
                doc.isEncrypt,
                doc.signatures,
                doc.signaturesUploadTxHash,
                doc.verifiers,
                doc.verifierSignatures,
                doc.localUrl
            );
        });
        return documentObjs;
    }

    /**
     * Update a document
     * @param id 
     * @param fnUpdate 
     */
    async updateDocument(id: string, fnUpdate) {
        const documents = await this.fetchDocument();
        const updateIndex = documents.map(doc => doc.id).indexOf(id);
        let updatedDoc: SiriusSignDocument;
        if (updateIndex > -1) updatedDoc = fnUpdate(<SiriusSignDocument>documents[updateIndex]);
        let updatedDocuments = documents;
        updatedDocuments[updateIndex] = updatedDoc;
        return this.storage.set(this.queryKey, updatedDocuments);
    }

    /**
     * Remove a document by id
     * @param id
     */
    async removeDocument(id: string) {
        const documents = await this.fetchDocument();
        let removedDocuments = documents.filter(doc => doc.id != id);
        return this.storage.set(this.queryKey, removedDocuments);
    }

    /**
     * Remove documents of an account
     * @param name 
     */
    deleteDocuments(name: string) {
        const queryKey = 'ssdocs-' + name;
        this.storage.remove(queryKey);
    }
}
