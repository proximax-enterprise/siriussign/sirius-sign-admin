import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import { GlobalService } from './../services/global.service';
import { GroupService } from '../services/group.service';
import { SigningService } from './../services/signing.service';
import { SignDocumentService } from '../services/sign-document.service';

@Component({
    selector: 'app-group-manage',
    templateUrl: './group-manage.page.html',
    styleUrls: ['./group-manage.page.scss'],
})
export class GroupManagePage implements OnInit {

    constructor(
        private router: Router,
        private toastController: ToastController,
        private global: GlobalService,
        public group: GroupService,
        private signing: SigningService,
        private signDoc: SignDocumentService
    ) { }

    ngOnInit() {
        this.group.setGroups(group => {
            const multisigAddr = group.multisigAcc.address.address;
            this.signing.getMultisigAccountInfo(multisigAddr)
                .subscribe(accInfo => {
                    console.log(accInfo);
                    group.isActive = true;
                    group.numOfAccept = group.members.length;
                },
                    err => {
                        console.log(err);
                        group.isActive = false;
                        const now = Date.now();
                        if (now - group.creationDate > 172800000) {
                            this.group.removeGroup(group.name);
                            const indexOfRemovedGroup = this.group.groups.indexOf(group);
                            this.group.groups.splice(indexOfRemovedGroup, 1);
                        }
                    });
        });
    }

    /**
     * Present a toast
     * @param msg
     */
    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }

    /**
     * Assign selectedGroup and navigate to group info
     * @param index 
     */
    onGroup(index) {
        this.group.isInvite = false;
        if (this.group.mode == 0) {
            this.group.selectedGroup = this.group.groups[index];
            this.router.navigate(['group-info']);
        }
        if (this.group.mode == 1) {
            if (this.group.groups[index].isActive) {
                this.group.selectedGroup = this.group.groups[index];
                this.signDoc.document.multisigAcc = this.group.groups[index].multisigAcc;
                this.group.mode = 0;
                this.router.navigate(['new-with-others']);
            }
            else {
                this.presentToast('Group ' + this.group.groups[index].name + ' is not activated.');
            }
        }
    }

    /**
     * Remove a group
     * @param index 
     */
    onRemove(index) {
        this.group.removeGroup(this.group.groups[index].name);
        this.group.groups.splice(index, 1);
    }
}
