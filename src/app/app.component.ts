import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
// import { Network } from '@ionic-native/network/ngx';

import { environment } from 'src/environments/environment';

import { GlobalService } from './services/global.service';
import { DocumentClassificationService } from './services/document-classification.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    constructor(
        private platform: Platform,
        private device: Device,
        private toastController: ToastController,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private deeplinks: Deeplinks,
        // private network: Network,
        private router: Router,
        private global: GlobalService,
        private documentClassification: DocumentClassificationService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleBlackTranslucent();

            console.log(this.device.platform);
            this.global.isBrowser = (this.device.platform == 'browser') || (this.device.platform == null);

            // Set safe area for notch mobiles
            document.documentElement.style.setProperty('--safe-area-inset-top', 'constant(safe-area-inset-top)');
            document.documentElement.style.setProperty('--safe-area-inset-top', 'env(safe-area-inset-top)');
            document.documentElement.style.setProperty('--safe-area-inset-bottom', 'constant(safe-area-inset-bottom)');
            document.documentElement.style.setProperty('--safe-area-inset-bottom', 'env(safe-area-inset-bottom)');
            if (this.device.platform == 'Android' || this.global.isBrowser)
                document.documentElement.style.setProperty('--safe-area-inset-top', '20px');

            console.log((<any>window).AndroidNotch);
            if ((<any>window).AndroidNotch) {
                const style = document.documentElement.style;
                (<any>window).AndroidNotch.getInsetTop(px => {
                    console.log('safe-top: ', px);
                    // style.setProperty('--safe-area-inset-top', px + 'px');
                    document.documentElement.style.setProperty('--safe-area-inset-top', px + 'px');
                }, (err) => console.error('Failed to get insets top:', err));

                (<any>window).AndroidNotch.getInsetBottom(px => {
                    console.log('safe-bottom: ',px);
                    // style.setProperty('--safe-area-inset-top', px + 'px');
                    document.documentElement.style.setProperty('--safe-area-inset-bottom', px + 'px');
                }, (err) => console.error('Failed to get insets top:', err));
            }

            // Off console.log for production
            if (environment.production) {
                if (window) {
                    window.console.log = function () { };
                }
            }

            // For mobile, not work on browser (?)
            // const disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            //     this.presentToastInternet(false);
            //     this.global.isOnline = false;
            // });

            // const connectSubscription = this.network.onConnect().subscribe(() => {
            //     if(!this.global.isOnline) {
            //         this.presentToastInternet(true);
            //         this.global.isOnline = true;
            //     }
            // });

            // Detect network status. Work in both browser and mobile. Need insatall Network plugin.
            this.global.isOnline = navigator.onLine;
            this.global.observableIsOnline.next(this.global.isOnline);
            window.addEventListener('offline', () => {
                this.presentToastInternet(false);
                this.global.isOnline = false;
                this.global.observableIsOnline.next(this.global.isOnline);
            });

            window.addEventListener('online', () => {
                if (!this.global.isOnline) {
                    this.presentToastInternet(true);
                    this.global.isOnline = true;
                    this.global.observableIsOnline.next(this.global.isOnline);
                }
            });

            this.initRoute();

            this.deeplinks.route({
                '/app/sign-info/:id': {},
            }).subscribe(match => {
                this.documentClassification.universalId = match.$args.id;
                if (this.global.isLoggedIn) {
                    this.router.navigate(['sign-info']);
                }
                else {
                    this.router.navigate(['sign-in']);
                }
                // match.$route - the route we matched, which is the matched entry from the arguments to route()
                // match.$args - the args passed in the link
                // match.$link - the full link data
                console.log('Successfully matched route', match);
            }, nomatch => {
                // nomatch.$link - the full link data
                console.log('Got a deeplink that didn\'t match', nomatch);
            });
        });

        this.splashScreen.hide();
    }

    /**
     * Routing according to log in status when open app
     */
    async initRoute() {
        let signUpState = await this.global.fetchIsSignUp();
        if (signUpState) {
            this.router.navigateByUrl('/sign-in');
        } else {
            this.router.navigateByUrl('/');
        }
    }

    /**
     * Alert No Internet connection
     */
    async presentToastInternet(isOnline: boolean) {
        let msg = '';
        if (isOnline) msg = 'The Internet is connected!'
        else msg = 'The Internet is disconnected!';

        const toast = await this.toastController.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    }
}
