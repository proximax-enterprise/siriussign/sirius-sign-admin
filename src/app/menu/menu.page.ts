import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { GlobalService } from './../services/global.service';
import { MonitorService } from './../services/monitor.service';
import { SignatureService } from './../services/signature.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { MultitaskService } from './../services/multitask.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.page.html',
    styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

    name: string = '';
    email: string = '';

    constructor(
        private menu: MenuController,
        private router: Router,
        private global: GlobalService,
        private monitor: MonitorService,
        private signature: SignatureService,
        private documentClassification: DocumentClassificationService,
        private multitask: MultitaskService
    ) { }

    async ngOnInit() {
        await this.signature.fetchFromStorage();
        this.name = this.signature.name;
        this.email = this.global.loggedWallet.name;

        this.signature.observableSignatureImg.subscribe(res => {
            this.name = this.signature.name;
            this.email = this.global.loggedWallet.name;
        });
    }

    /**
     * Close menu any time leave menu
     */
    ionViewWillLeave() {
        this.menu.close('menu');
    }

    /**
     * Open menu if needed
     */
    ionViewWillEnter() {
        if (this.global.isMenuNeedOpen) {
            this.menu.open('menu');
            this.global.isMenuNeedOpen = false;
        }
    }

    /**
     * Set menu will open when route back from a page
     */
    setMenuNeedOpen() {
        this.global.isMenuNeedOpen = true;
    }

    navigate() {
        this.global.isMenuNeedOpen = true;
        this.router.navigate(['upgrade-choose']);
    }

    /**
     * Navigate to app init
     */
    goSignIn() {
        this.router.navigate(['/sign-in']);
    }

    /**
     * Launch sign out process
     */
    onSignOut() {
        try {
            this.monitor.closeListener();
        }
        catch (err) { }
        this.documentClassification.clearDocs();
        this.multitask.selectedTask = null;
        this.multitask.tasks = [];
        this.menu.close();
        this.global.signOut();
        this.goSignIn();
    }
}
