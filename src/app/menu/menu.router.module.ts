import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuPage } from './menu.page';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: MenuPage,
        children: [
            {
                path: '',
                loadChildren: '../tab-home-v2/tab-home-v2.module#TabHomeV2PageModule'
            },
            {
                path: 'need-sign',
                redirectTo: '/app/tabs/tab-menu/need-sign',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'need-sign',
        children: [
            {
                path: '',
                loadChildren: '../need-sign/need-sign.module#NeedSignPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'wait-others',
        children: [
            {
                path: '',
                loadChildren: '../wait-others/wait-others.module#WaitOthersPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'share-info',
        children: [
            {
                path: '',
                loadChildren: '../share-info/share-info.module#ShareInfoPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'account',
        children: [
            {
                path: '',
                loadChildren: '../account/account.module#AccountPageModule'
            },
            {
                path: 'signature',
                loadChildren: '../signature/signature.module#SignaturePageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'group-manage',
        children: [
            {
                path: '',
                loadChildren: '../group-manage/group-manage.module#GroupManagePageModule'
            },
            {
                path: 'group-create',
                loadChildren: '../group-create/group-create.module#GroupCreatePageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'group-create',
        children: [
            {
                path: '',
                loadChildren: '../group-create/group-create.module#GroupCreatePageModule'
            },
            {
                path: 'group-manage',
                loadChildren: '../group-manage/group-manage.module#GroupManagePageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'help',
        children: [
            {
                path: '',
                loadChildren: '../help/help.module#HelpPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'about',
        children: [
            {
                path: '',
                loadChildren: '../about/about.module#AboutPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'privacy',
        children: [
            {
                path: '',
                loadChildren: '../privacy/privacy.module#PrivacyPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'backup',
        children: [
            {
                path: '',
                loadChildren: '../backup/backup.module#BackupPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'signature',
        children: [
            {
                path: '',
                loadChildren: '../signature/signature.module#SignaturePageModule'
            },
            {
                path: 'account',
                loadChildren: '../account/account.module#AccountPageModule'
            },
            {
                path: 'home',
                redirectTo: '/app/tabs/tab-menu/home',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class MenuRoutingModule { }
