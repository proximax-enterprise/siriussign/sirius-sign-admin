import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CosignProgressPage } from './cosign-progress.page';

xdescribe('CosignProgressPage', () => {
  let component: CosignProgressPage;
  let fixture: ComponentFixture<CosignProgressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CosignProgressPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CosignProgressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
