import { Transaction } from 'tsjs-xpx-chain-sdk';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SiriusSignCosigningTask } from './../model/siriussign-task.model';

import { GlobalService } from '../services/global.service';
import { SigningService } from '../services/signing.service';
import { MonitorService } from './../services/monitor.service';
import { UploadStorageService } from './../services/upload-storage.service';
import { SignatureService } from './../services/signature.service';
import { SigningWithoutMultisigService } from './../services/signing-without-multisig.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { DrawSignatureService } from './../services/draw-signature.service';
import { MultitaskService } from './../services/multitask.service';
import { DocumentStorageService } from './../services/document-strorage.service';

@Component({
    selector: 'app-cosign-progress',
    templateUrl: './cosign-progress.page.html',
    styleUrls: ['./cosign-progress.page.scss'],
})
export class CosignProgressPage implements OnInit {

    percent = 64;
    transactionStatus = {
        isDone: false,
        status: 'unconfirmed'
    };
    waitVal: number;

    constructor(
        private router: Router,
        private global: GlobalService,
        private signing: SigningService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        private signature: SignatureService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private documentClassification: DocumentClassificationService,
        private drawSignature: DrawSignatureService,
        private multitask: MultitaskService,
        private documentStorage: DocumentStorageService
    ) {
        const task = new SiriusSignCosigningTask(
            this.signingWithoutMultisig,
            this.monitor,
            this.uploadStorage,
            this.global.loggedAccount,
            this.drawSignature.cosigner,
            this.signature.signatureImg,
            this.documentClassification.selectedDocInfo
        );
        const selectedDocId = task.selectedDocInfo.id;
        this.documentClassification.needSign = this.documentClassification.needSign
            .filter(doc => doc.id != selectedDocId);
        task.incCosignProgress(task => this.finishCosigning(task));
        this.multitask.tasks.push(task);
        setTimeout(() => {
            this.goTasks();
        }, 10);
    }

    ngOnInit() { }

    /**
     * Fetch and store document after cosigning done
     * @param task
     */
    async finishCosigning(task: SiriusSignCosigningTask) {
        const isSuccess = (task.transactionStatus.status == 'confirmed');
        const oldDocId = task.selectedDocInfo.id;
        if (isSuccess) {
            const newDoc = await this.documentClassification.fetchDocumentBySigningTxHash(task.selectedDocInfo.signTxHash);
            this.documentStorage.updateDocument(oldDocId, oldDoc => newDoc);
            this.documentClassification.classify(newDoc);
            this.documentClassification.sortDocs('compledAndWaiting');
        }
        else {
            const docs = await this.documentStorage.fetchDocument();
            docs.forEach(doc => this.documentClassification.classify(doc));
            this.documentClassification.sortDocs('all');
        }
    }

    /**
     * Navigate to tasks tab
     */
    goTasks() {
        this.global.setIsProgressDone(true);
        this.multitask.selectedTask = null;
        this.router.navigate(['/app/tabs/tab-tasks']);
    }

    /**
     * Calculate stroke-dashoffset value from percent
     * @returns
     */
    calStrokeDashOffset() {
        let c = Math.PI * (60 * 2);

        if (this.percent < 0) { this.percent = 0; }
        if (this.percent > 100) { this.percent = 100; }

        let offset = ((100 - this.percent) / 100) * c;
        return offset + 190;
    }

    goDone() {
        this.global.setIsProgressDone(true);
        this.router.navigate(['cosign-done']);
    }

    /**
     * Check if the transaction is confirmed
     */
    getStatus() {
        const txHash = this.signing.toBeCosignTx.parentHash;
        this.signing.getTransactionStatus(txHash).subscribe(status => {
            if (status.group == 'unconfirmed') {
                if (this.percent < 75) this.percent += 1;
            }
            if (status.group == 'confirmed') {

                this.transactionStatus.isDone = true;
            }
        }, err => {
            if (this.percent < 75) this.percent += 1;
        });
    }

    /**
     * Listen for transaction status
     */
    async listenStatus() {
        const address = this.signing.selectedDocInfo.multisigAccount.address;
        const cosignListen = await this.monitor.listenAggregateBondedCosignature(address,
            () => {
                this.signing.announceAggregateBondedCosignature(this.signing.toBeCosignTx)
                    .subscribe(res => console.log(res), err => console.log(err));
            },
            (tx) => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = 'confirmed';
                this.signing.selectedDocInfo.status = 'Cosigned';
                cosignListen.unsubscribe();
            }
        );
    }

    /**
     * Get status and calculate progress of Sign Only Me flow
     */
    incProgress() {
        const signedTxHash = this.signing.toBeCosignTx.parentHash;
        // this.signDoc.document.status = 'Confirmed';
        // this.listenStatus();
        this.signing.announceAggregateBondedCosignature(this.signing.toBeCosignTx)
            .subscribe(res => {
                console.log(res);
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = 'confirmed';
                this.signing.selectedDocInfo.status = 'Cosigned';
            }, err => {
                console.log(err)
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = 'failed';
                this.signing.selectedDocInfo.status = 'Failed to cosign';
            });

        let reGetStatus = setInterval(() => {
            if (this.percent < 75) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(reGetStatus);
        }, 250);

        let toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    this.goDone();
                }
            }
        }, 50);
    }

    /**
     * Increase progress percentage of new process
     */
    runProgress() {
        let reGetStatus = setInterval(() => {
            if (this.percent < this.waitVal) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(reGetStatus);
        }, 250);

        let toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    this.goDone();
                }
            }
        }, 100);
    }

    async incCosignProgress() {
        this.runProgress();

        // Upload signature
        if (this.drawSignature.cosigner.signaturePosition.name != '') {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signature.signatureImg,
                this.documentClassification.selectedDocInfo.documentAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.documentClassification.selectedDocStatus = 'Failed to upload signature: ' + err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            this.documentClassification.selectedDocInfo.signaturesUploadTxHash.push(signatureUploadTx.transactionHash);
        }
        else {
            this.documentClassification.selectedDocInfo.signaturesUploadTxHash.push('');
        }

        // Sign transaction
        const signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.documentClassification.selectedDocInfo, false, this.drawSignature.cosigner);
        this.documentClassification.selectedDocInfo.signTxHash = signedDocSigningTx.hash;

        const docSigningTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.global.loggedAccount.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.documentClassification.selectedDocStatus = 'Failed to cosign: ' + err.status;
                return null;
            });
        if (!docSigningTx) return;

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.documentClassification.selectedDocStatus = 'Cosigned';
    }
}
