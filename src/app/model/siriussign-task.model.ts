import { Transaction, Account } from 'tsjs-xpx-chain-sdk';
import { UploadResult } from 'tsjs-chain-xipfs-sdk';

import { SiriusSignDocument } from './siriussign-document.model';
import { NamePosition } from './signature-canvas.model';

import { SignDocument } from '../services/sign-document.service'
import { SigningWithoutMultisigService } from './../services/signing-without-multisig.service';
import { MonitorService } from './../services/monitor.service';
import { UploadStorageService } from './../services/upload-storage.service';
import { SignatureService } from './../services/signature.service';
import { SignatureInfo } from '../services/draw-signature.service';

export class SiriusSignTask {
    percent: number;
    transactionStatus: {
        isDone: boolean,
        status: string
    }
    waitVal: number;

    name: string;
    type: 'Single signing' | 'Multi signing' | 'Cosigning';

    constructor() {
        this.percent = 0;
        this.waitVal = Math.floor(Math.random() * 10) + 74;
        this.transactionStatus = {
            isDone: false,
            status: 'undefined'
        }
    }
}

export class SiriusSignSigningTask extends SiriusSignTask {
    account: Account;
    document: SignDocument;
    signers: SignatureInfo[];
    percent: number;
    transactionStatus: {
        isDone: boolean,
        status: string
    }

    processStage: number = 0;
    waitVal: number;

    constructor(
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        private signature: SignatureService,

        account: Account,
        document: SignDocument,
        signers: SignatureInfo[]
    ) {
        super();
        this.processStage = 0;

        this.account = Object.create(account);
        this.document = {
            file: document.file,
            size: document.size,
            signDate: document.signDate,
            cosigners: document.cosigners,
            verifiers: document.verifiers,
            multisigAcc: document.multisigAcc,     //phase 1
            documentAcc: document.documentAcc,     //phase 2
            fileHash: document.fileHash,
            signTxHash: document.signTxHash,
            uploadTxHash: document.uploadTxHash,
            isEncrypt: document.isEncrypt,
            signatureUploadTxHash: document.signatureUploadTxHash,
            status: document.status
        };
        this.signers = signers.map(ins => ins);
        this.name = this.document.file.name;
        this.type = (this.document.cosigners.length > 0) ? 'Multi signing' : 'Single signing';
    }

    /**
     * Increase progress percentage of new process
     * @param fnFinish
     */
    runNewProgress(fnFinish?) {
        let reGetStatus = setInterval(() => {
            const stage = this.processStage + 1;
            if (this.percent < (this.waitVal * stage / 4)) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(reGetStatus);
        }, 250);

        let toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    if (fnFinish) fnFinish(this);
                }
            }
        }, 100);
    }

    /**
     * Upload file
     * 
     * If failed, set process done, status failed by error
     */
    async uploadSignDoc() {
        const isEncrypt = this.document.isEncrypt;
        let uploadRes: UploadResult;
        try {
            if (isEncrypt) {
                uploadRes = await this.uploadStorage.uploadFileEncrypt(this.document);
                return uploadRes;
            }
            else {
                uploadRes = await this.uploadStorage.uploadFile(this.document);
                return uploadRes;
            }
        }
        catch (err) {
            console.log(err);
            this.transactionStatus.isDone = true;
            this.transactionStatus.status = 'Upload failed';

            this.document.status = 'Failed to upload the document: ' + err.status;
        }
    }

    /**
     * Single sign with new process
     */
    test () {
        this.document.documentAcc = this.signingWithoutMultisig.createDocumentAccount();
    }
    async incSingleSign(fnFinish?) {
        // Increase progress percentage        
        this.runNewProgress(fnFinish);

        // Create document account
        this.document.documentAcc = this.signingWithoutMultisig.createDocumentAccount();

        // Set Restriction
        const signedRestrictionTx = this.signingWithoutMultisig.createRestriction(this.document);
        const restrictTx: Transaction = await this.monitor.waitForTransactionConfirmed(
            this.document.documentAcc.address,
            signedRestrictionTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedRestrictionTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
                return null;
            });
        if (!restrictTx) return;
        this.processStage++;
        console.log(this.document);
        

        // Upload file
        const uploadTx = await this.uploadSignDoc();
        if (!uploadTx) return;
        this.document.uploadTxHash = uploadTx.transactionHash;
        this.processStage++;

        // Upload signature
        if (this.signers.length != 0) {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signature.signatureImg,
                this.document.documentAcc.publicAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.document.status = 'Failed to upload signature: ' + err.status;
                return null;
            });
            if (!signatureUploadTx) return
            this.document.signatureUploadTxHash = signatureUploadTx.transactionHash;
        }
        else {
            this.document.signatureUploadTxHash = '';
        }
        console.log(this.document);
        this.processStage++;

        // Sign transaction
        let signatureInfo = this.signers.filter(signer => signer.publicKey == this.account.publicKey);
        console.log('test sign');
        
        console.log(signatureInfo);
        
        // ********************************Temp for 1 instance of signature
        if (signatureInfo.length == 0) {
            const noSigantureInfo: SignatureInfo = {
                publicKey: this.account.publicKey,
                signaturePosition: {
                    name: '',
                    page: 0,
                    x: -1,
                    y: -1,
                    namePosition: NamePosition.NONE
                }
            }
            signatureInfo.push(noSigantureInfo);
        }
        const signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.document, true, signatureInfo[0]);
        this.document.signTxHash = signedDocSigningTx.hash;
        const docSigningTx = await this.monitor.waitForTransactionConfirmed(this.account.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
            });
        if (!docSigningTx) return;

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.document.status = 'Confirmed';
    }

    /**
     * Run multi sign task
     * @param fnFinish 
     */
    async incMultiSign(fnFinish?) {
        console.log('Multi Sign');
        // Increase progress percentage        
        this.runNewProgress(fnFinish);

        // Create document account
        this.document.documentAcc = this.signingWithoutMultisig.createDocumentAccount();

        // Set Restriction
        const signedRestrictionTx = this.signingWithoutMultisig.createRestriction(this.document);
        const restrictTx: Transaction = await this.monitor.waitForTransactionConfirmed(
            this.document.documentAcc.address,
            signedRestrictionTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedRestrictionTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
                return null;
            });
        if (!restrictTx) return;
        this.processStage++;

        // Upload file
        const uploadTx = await this.uploadSignDoc();
        this.document.uploadTxHash = uploadTx.transactionHash;
        if (!uploadTx) return;

        // Upload signature
        if (this.signers.map(signatureInfo => signatureInfo.publicKey).includes(this.account.publicKey)) {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signature.signatureImg,
                this.document.documentAcc.publicAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.document.status = 'Failed to upload signature: ' + err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            this.document.signatureUploadTxHash = signatureUploadTx.transactionHash;
        }
        else {
            this.document.signatureUploadTxHash = '';
        }
        this.processStage++;

        // Sign transaction
        let signatureInfo = this.signers.filter(signer => signer.publicKey == this.account.publicKey);
        // ********************************Temp for 1 instance of signature
        if (signatureInfo.length == 0) {
            const noSigantureInfo: SignatureInfo = {
                publicKey: this.account.publicKey,
                signaturePosition: {
                    name: '',
                    page: 0,
                    x: -1,
                    y: -1,
                    namePosition: NamePosition.NONE
                }
            }
            signatureInfo.push(noSigantureInfo);
        }
        const signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.document, true, signatureInfo[0]);
        this.document.signTxHash = signedDocSigningTx.hash;

        const docSigningTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.account.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
                return null;
            });
        if (!docSigningTx) return;
        this.processStage++;

        // Send notification
        let signers = this.signers;
        const noSignatureSigners = this.document.cosigners
            .filter(cosigner => !signers.map(signer => signer.publicKey).includes(cosigner));
        noSignatureSigners.forEach(signer => {
            const noSigantureInfo: SignatureInfo = {
                publicKey: signer,
                signaturePosition: {
                    name: '',
                    page: 0,
                    x: -1,
                    y: -1,
                    namePosition: NamePosition.NONE
                }
            }
            signers.push(noSigantureInfo);
        });
        const signedNotiTx = this.signingWithoutMultisig.createMultiSigningNotificationTransaction(this.document, signers);
        const aggNotiTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.document.documentAcc.address, signedNotiTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedNotiTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = 'Failed to send notification to cosigners: ' + err;
                return null;
            });;
        if (!aggNotiTx) return;

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.document.status = 'Confirmed';
    }
}

export class SiriusSignCosigningTask extends SiriusSignTask {
    account: Account;
    cosigner: SignatureInfo
    signatureImg: string;
    selectedDocInfo: SiriusSignDocument
    selectedDocStatus: string;

    constructor(
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        account: Account,
        cosigner: SignatureInfo,
        signatureImg: string,
        selectedDocInfo: SiriusSignDocument
    ) {
        super();
        this.account = Object.create(account);
        this.cosigner = {
            publicKey: cosigner.publicKey,
            signaturePosition: cosigner.signaturePosition
        };
        this.signatureImg = signatureImg;
        this.selectedDocInfo = SiriusSignDocument.create(
            selectedDocInfo.id,
            selectedDocInfo.name,
            selectedDocInfo.fileHash.sha256Hash,
            selectedDocInfo.uploadDate,
            selectedDocInfo.owner,
            selectedDocInfo.isSigned,
            selectedDocInfo.signDate,
            selectedDocInfo.cosigners,
            selectedDocInfo.cosignatures,
            selectedDocInfo.documentAccount,
            selectedDocInfo.signTxHash,
            selectedDocInfo.uploadTxHash,
            selectedDocInfo.isEncrypt,
            selectedDocInfo.signatures,
            selectedDocInfo.signaturesUploadTxHash,
            selectedDocInfo.verifiers,
            selectedDocInfo.verifierSignatures,
            selectedDocInfo.localUrl
        );
        this.name = this.selectedDocInfo.name;
        this.type = 'Cosigning';
    }

    /**
     * Increase progress percentage of new process
     */
    runProgress(fnFinish?) {
        let reGetStatus = setInterval(() => {
            if (this.percent < this.waitVal) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(reGetStatus);
        }, 250);

        let toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    if (fnFinish) fnFinish(this);
                }
            }
        }, 100);
    }


    /**
     * Run Cosign task
     */
    async incCosignProgress(fnFinish?) {
        this.runProgress(fnFinish);

        // Upload signature
        if (this.cosigner.signaturePosition.name != '') {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signatureImg,
                this.selectedDocInfo.documentAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.selectedDocStatus = 'Failed to upload signature: ' + err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            this.selectedDocInfo.signaturesUploadTxHash.push(signatureUploadTx.transactionHash);
        }
        else {
            this.selectedDocInfo.signaturesUploadTxHash.push('');
        }

        // Sign transaction
        const signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.selectedDocInfo, false, this.cosigner);
        this.selectedDocInfo.signTxHash = signedDocSigningTx.hash;

        const docSigningTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.account.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.selectedDocStatus = 'Failed to cosign: ' + err;
                return null;
            });
        if (!docSigningTx) return;

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.selectedDocStatus = 'Cosigned';
    }
}