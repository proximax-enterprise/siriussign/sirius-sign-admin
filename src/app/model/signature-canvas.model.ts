export enum NamePosition {
    NONE,
    TOP,
    BOTTOM,
    LEFT,
    RIGHT
}

export class SignatureCanvas {
    x: number;
    y: number;
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    scale: number = 1;
    image: HTMLImageElement;
    offsetX: number;
    offsetY: number;
    canvasSize = {
        width: 0,
        height: 0
    }

    id: string;
    name: string;
    isInit: boolean = false;
    isNamePlace: boolean = false;
    namePosition: NamePosition = NamePosition.NONE;
    pageNumber: number;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    create(canvasId: string, imageObj: HTMLImageElement, scale: number) {
        // Initial settings
        this.pageNumber = 1;
        this.x = 0;
        this.y = 0;
        this.scale = scale;

        // Canvas settings
        this.canvas = <HTMLCanvasElement>document.getElementById(canvasId);
        this.ctx = this.canvas.getContext('2d');

        this.canvas.width = this.canvasSize.width;
        this.canvas.height = this.canvasSize.height;

        this.ctx.scale(this.scale, this.scale);

        // Image setting
        this.image = imageObj
        this.calOffset();
    }

    fixScale() {
        this.ctx.scale(this.scale, this.scale);
    }

    calOffset() {
        const rect = this.canvas.getBoundingClientRect();
        this.offsetX = rect.left;
        this.offsetY = rect.top;
    }


    getPosition() {
        return {
            x: this.x,
            y: this.y
        }
    }

    computeNameRect(namePosition: NamePosition) {
        switch (namePosition) {
            case NamePosition.TOP:
                return {
                    topLeftX: this.x,
                    topLeftY: this.y - 44,
                    bottomRightX: this.x + this.image.width,
                    bottomRightY: this.y
                }
            case NamePosition.BOTTOM:
                return {
                    topLeftX: this.x,
                    topLeftY: this.y + this.image.height,
                    bottomRightX: this.x + this.image.width,
                    bottomRightY: this.y + this.image.height + 44
                }
            case NamePosition.LEFT:
                return {
                    topLeftX: this.x - this.image.width,
                    topLeftY: this.y + this.image.height / 2 - 22,
                    bottomRightX: this.x,
                    bottomRightY: this.y + this.image.height / 2 + 22
                }
            case NamePosition.RIGHT:
                return {
                    topLeftX: this.x + this.image.width,
                    topLeftY: this.y + this.image.height / 2 - 22,
                    bottomRightX: this.x + 2 * this.image.width,
                    bottomRightY: this.y + this.image.height / 2 + 22
                }
            default: throw new Error('Invalid Name Position');
        }
    }

    drawRoundRect(x: number, y: number, width: number, height: number, radius: number) {
        if (width < 2 * radius) radius = width / 2;
        if (height < 2 * radius) radius = height / 2;
        this.ctx.beginPath();
        this.ctx.moveTo(x + radius, y);
        this.ctx.arcTo(x + width, y, x + width, y + height, radius);
        this.ctx.arcTo(x + width, y + height, x, y + height, radius);
        this.ctx.arcTo(x, y + height, x, y, radius);
        this.ctx.arcTo(x, y, x + width, y, radius);
        this.ctx.closePath();
        this.ctx.stroke();
        return this;
    }

    drawNameAtPosition(namePosition: NamePosition, isButton: boolean) {
        const rect = this.computeNameRect(namePosition);
        this.ctx.font = "14px Helvetica";
        if (namePosition == NamePosition.TOP || namePosition == NamePosition.BOTTOM) {
            this.ctx.textAlign = "center";
            this.ctx.fillText(this.name, (rect.topLeftX + rect.bottomRightX) / 2, rect.bottomRightY - 16);
        }
        else if (namePosition == NamePosition.RIGHT) {
            this.ctx.textAlign = "start";
            this.ctx.fillText(this.name, rect.topLeftX + 5, rect.bottomRightY - 16);
        }
        else if (namePosition == NamePosition.LEFT) {
            this.ctx.textAlign = "end";
            this.ctx.fillText(this.name, rect.bottomRightX - 5, rect.bottomRightY - 16);
        }

        if (isButton) this.drawRoundRect(rect.topLeftX, rect.topLeftY, (rect.bottomRightX - rect.topLeftX), (rect.bottomRightY - rect.topLeftY), 10);
    }

    drawNameButton() {
        const isButton = true;
        this.ctx.fillStyle = "#979797";
        this.ctx.strokeStyle = "#979797";
        this.drawNameAtPosition(NamePosition.TOP, isButton);
        this.drawNameAtPosition(NamePosition.RIGHT, isButton);
        this.drawNameAtPosition(NamePosition.BOTTOM, isButton);
        this.drawNameAtPosition(NamePosition.LEFT, isButton);
    }

    drawName() {
        this.ctx.fillStyle = "#000000";
        const isButton = false;
        this.drawNameAtPosition(this.namePosition, isButton);
    }

    init(fnEndDrawAfterInit) {
        let isMoved = false;
        const startInit = (e) => {
            isMoved = false
            const move = (e) => {
                isMoved = true;
            }
            this.canvas.onmousemove = move;
            this.canvas.ontouchmove = move;
        }

        const endInit = (e) => {
            if (!isMoved) {
                this.isInit = true;
                this.canvas.width = this.canvasSize.width
                this.canvas.height = this.canvasSize.height

                this.calOffset();
                const pageX = e.pageX ? e.pageX - this.offsetX : e.touches[0].clientX - this.offsetX;
                const pageY = e.pageY ? e.pageY - this.offsetY : e.touches[0].clientY - this.offsetY;
                this.x = pageX;
                this.y = pageY;

                this.drawSignature(fnEndDrawAfterInit);
            }

            this.canvas.onmousemove = null;
            this.canvas.ontouchmove = null;
        }

        this.canvas.onmousedown = startInit;
        this.canvas.ontouchstart = startInit;

        this.canvas.onmouseup = endInit;
        this.canvas.ontouchend = endInit;
    }

    drawSignature(fnFinish?) {
        // init
        let start = {
            x: 0,
            y: 0
        };
        let end = {
            x: 0,
            y: 0
        };
        var completed = false;

        this.canvas.width = this.canvasSize.width
        this.canvas.height = this.canvasSize.height

        // default signature
        this.fixScale();
        this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
        if (!this.isNamePlace) this.drawNameButton();
        else this.drawName();

        let isNeedRedraw = false;
        let holdFunction;

        const startDraw = (e) => {
            this.calOffset();
            const pageX = e.pageX ? e.pageX - this.offsetX : e.touches[0].clientX - this.offsetX;
            const pageY = e.pageY ? e.pageY - this.offsetY : e.touches[0].clientY - this.offsetY;

            // console.log(this.offsetX, this.offsetY);
            // console.log(pageX, pageY);
            // console.log(this.x, this.y);
            // console.log(this.offsetX + this.x, this.offsetX + this.x + this.image.width, this.offsetY + this.y, this.offsetY + this.y + this.image.height);
            // console.log('-----');

            // Check if user start click on image
            if (
                pageX > this.x &&
                pageX < this.x + this.image.width &&
                pageY > this.y &&
                pageY < this.y + this.image.height
            ) {
                e.preventDefault();
                start.x = pageX;
                start.y = pageY;
                completed = true;

                holdFunction = setTimeout(() => {
                    console.log('Replace name')
                    if (this.isNamePlace) {
                        this.isNamePlace = false;
                        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                        this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
                        this.drawNameButton();
                    }
                }, 1000);

                const moveDraw = (e) => {
                    clearTimeout(holdFunction);

                    const pageX = e.pageX ? e.pageX - this.offsetX : e.touches[0].clientX - this.offsetX;
                    const pageY = e.pageY ? e.pageY - this.offsetY : e.touches[0].clientY - this.offsetY;

                    if (completed) {
                        end.x = pageX;
                        end.y = pageY;

                        const newX = this.x + (end.x - start.x);
                        const newY = this.y + (end.y - start.y);

                        this.x = newX >= 0 ? newX : 0;
                        this.y = newY >= 0 ? newY : 0;

                        this.x = (newX + this.image.width) <= this.canvas.width ? this.x : (this.canvas.width - this.image.width);
                        this.y = (newY + this.image.height) <= this.canvas.height ? this.y : (this.canvas.height - this.image.height);

                        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                        this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
                        start.x = end.x;
                        start.y = end.y;

                        if (this.isNamePlace) {
                            this.drawName();
                            isNeedRedraw = false;
                        }
                        else isNeedRedraw = true;
                    }
                }

                this.canvas.onmousemove = moveDraw;
                this.canvas.ontouchmove = moveDraw;
            }

            if (!this.isNamePlace) {
                let flag = {
                    isNeedRedraw: isNeedRedraw,
                    completed: completed
                }
                this.selectNamePosition(pageX, pageY, e, start, flag);
                isNeedRedraw = flag.isNeedRedraw;
                completed = flag.completed;
            }
        }

        const endDraw = () => {
            clearTimeout(holdFunction);
            if (isNeedRedraw) {
                if (!this.isNamePlace) this.drawNameButton();
                else this.drawName();
                isNeedRedraw = false;
            }
            if (fnFinish) fnFinish(this);
            completed = false;
            this.canvas.onmousemove = null;
            this.canvas.ontouchmove = null;
        }

        this.canvas.onmousedown = startDraw;
        this.canvas.ontouchstart = startDraw;

        this.canvas.onmouseup = endDraw;
        this.canvas.ontouchend = endDraw;
    }

    drawInactive(page: number) {
        if (page != this.pageNumber) this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        else {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            setTimeout(() => {
                this.ctx.beginPath();
                this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
                this.ctx.closePath();
            }, 100);
            if (this.isNamePlace) this.drawName();
        }
    }

    drawInactiveWithAnimation(page: number) {
        if (page != this.pageNumber) this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        else {
            let width = 0;
            let height = 0;
            const draw = () => {
                this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                this.ctx.beginPath();
                this.ctx.drawImage(this.image, this.x, this.y, width, height);
                this.ctx.closePath();
                if (this.isNamePlace) this.drawName();
                if (width < this.image.width || height < this.image.height) {
                    width += 10;
                    height = (width / this.image.width) * this.image.height;
                    requestAnimationFrame(() => draw());
                }
            }
            requestAnimationFrame(() => draw());
            if (this.isNamePlace) this.drawName();
        }
    }

    checkAndSelectNamePosition(
        pageX: number,
        pageY: number,
        e,
        namePosition: NamePosition,
        flag: { isNeedRedraw: boolean, completed: boolean }
    ) {
        const nameRect = this.computeNameRect(namePosition);
        if (
            pageX > nameRect.topLeftX &&
            pageX < nameRect.bottomRightX &&
            pageY > nameRect.topLeftY &&
            pageY < nameRect.bottomRightY
        ) {
            e.preventDefault();

            this.namePosition = namePosition;
            this.isNamePlace = true;

            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);

            flag.isNeedRedraw = true;
            flag.completed = true;
        }
    }

    selectNamePosition(pageX: number, pageY: number, e, start: { x: number, y: number }, flag: { isNeedRedraw: boolean, completed: boolean }) {
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.TOP, flag);
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.BOTTOM, flag);
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.LEFT, flag);
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.RIGHT, flag);
    }

    onSignHere(fnFinish?) {
        let onSignHere = false;
        const startDraw = (e) => {
            this.calOffset();
            const pageX = e.pageX ? e.pageX - this.offsetX : e.touches[0].clientX - this.offsetX;
            const pageY = e.pageY ? e.pageY - this.offsetY : e.touches[0].clientY - this.offsetY;

            // Check if user start click on image
            if (
                pageX > this.x &&
                pageX < this.x + this.image.width &&
                pageY > this.y &&
                pageY < this.y + this.image.height
            ) {
                e.preventDefault();
                onSignHere = true;
            }
        }

        const endDraw = () => {
            if (fnFinish && onSignHere) {
                fnFinish(this);
                onSignHere = false;
            }
        }

        this.canvas.onmousedown = startDraw;
        this.canvas.ontouchstart = startDraw;

        this.canvas.onmouseup = endDraw;
        this.canvas.ontouchend = endDraw;
    }
}
