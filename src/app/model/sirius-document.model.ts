import { PublicAccount } from 'tsjs-xpx-chain-sdk';

/**
 * SiriusDocument is a model of signing document used in SiriusSign.
 * It contains file and its notarization transaction infomation.
 */
export class SiriusDocument {
    /** Document ID (= transaction ID) */
    id: string;
    /** File name */
    name: string;
    /** File hash */
    fileHash: {
        sha256Hash: string,
        md5Hash: string
    }
    /** File upload and sign date */
    uploadDate: Date;
    /**
     * Transaction status
     * - Unconfirmed
     * - Confirmed
     */
    status: string;
    /** File owner address, who sign this document first */
    owner: string;
    /**
     * Sign status of current account
     * - 0: Not signed yet
     * - 1: Signed
     * - -1: Cancelled (for future work)
     */
    isSigned: number;
    /** Transaction cosigners */
    cosigners: [];
    cosignatures: [];
    /** Multisig Account that cosigners belong */
    multisigAccount: PublicAccount;
    /** Document Account - account that represents the document */
    documentAccount: PublicAccount;
    /** Sign transaction hash */
    signTxHash: string;
    /** Upload transaction hash */
    uploadTxHash: string;
    /** File is encrypt */
    isEncrypt: boolean

    constructor() {
        this.id = null;
        this.name = null;
        this.fileHash = {
            sha256Hash: null,
            md5Hash: null
        }
        this.uploadDate = null;
        this.status = null;
        this.owner = null;
        this.isSigned = null;
        this.cosigners = [];
        this.cosignatures = [];
        this.multisigAccount = null;
        this.documentAccount = null;
        this.signTxHash = null;
        this.uploadTxHash = null;
        this.isEncrypt = null
    }

    /**
     * Create a SiriusDocument from document info
     * @param name 
     * @param fileHash 
     * @param uploadDate
     * @param status 
     * @param owner 
     * @param isSigned 
     * @param cosigners 
     * @param transactionHash 
     */
    static create(id: string, name: string, fileHash: string, uploadDate: Date, status: string, owner: string, isSigned: number, cosigners: any, cosignatures: any, multisigAccount: PublicAccount, documentAccount: PublicAccount, signTxHash: string, uploadTxHash: string, isEncrypt: boolean) {
        let doc = new SiriusDocument();
        doc.id = id;    // transactionHash.substr(transactionHash.length-5);
        doc.name = name;
        doc.fileHash = {
            sha256Hash: fileHash,
            md5Hash: null
        }
        doc.uploadDate = uploadDate;
        doc.status = status;
        doc.owner = owner;
        doc.isSigned = isSigned;
        doc.cosigners = cosigners;
        doc.cosignatures = cosignatures;
        doc.multisigAccount = multisigAccount;
        doc.documentAccount = documentAccount;
        doc.signTxHash = signTxHash;
        doc.uploadTxHash = uploadTxHash;
        doc.isEncrypt = isEncrypt;
        return doc;
    }
}