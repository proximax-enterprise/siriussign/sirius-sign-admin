import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

import { HelperService } from './../services/helper.service';
import { GlobalService } from './../services/global.service';
import { SignDocumentService } from '../services/sign-document.service';
import { GroupService } from '../services/group.service';

@Component({
    selector: 'app-new-with-others',
    templateUrl: './new-with-others.page.html',
    styleUrls: ['./new-with-others.page.scss'],
})
export class NewWithOthersPage implements OnInit {

    file = {
        name: '',
        size: 'Unknown',
        uploadDate: '',
        cosignAcc: '',
        hashFunc: 'SHA256',
        fileHash: '',
        storage: 'Public',
        fee: 'No fee'
    };

    labels = {
        name: 'File name',
        size: 'Size',
        uploadDate: 'Upload date',
        cosignAcc: 'Group of Cosigners',
        hashFunc: 'Hash function',
        fileHash: 'File Hash (SHA256)',
        storage: 'Storage',
        fee: 'Network fee'
    };

    fields = ['name', 'size', 'uploadDate', 'cosignAcc', 'fileHash', 'storage', 'fee'];

    isProcessing: boolean = false;

    constructor(
        private toastController: ToastController,
        private router: Router,
        private global: GlobalService,
        private signDoc: SignDocumentService,
        private group: GroupService
    ) { }

    ngOnInit() {
        this.isProcessing = false;
        const doc = this.signDoc.getDocmument();
        this.file.name = doc.file.name;
        this.file.uploadDate = HelperService.dateToShortString(doc.signDate);
        this.file.fileHash = doc.fileHash;
        this.file.size = HelperService.sizeToString(this.signDoc.document.size);
        if (this.group.selectedGroup) this.file.cosignAcc = this.group.selectedGroup.name;
        else this.file.cosignAcc = '';
    }

    /**
     * Naviagte to new-only-me
     */
    goOnlyMe() {
        this.router.navigate(['new-only-me']);
    }

    /**
     * Navigate to progress page
     */
    goProgress() {
        this.global.setIsProgressDone(false);
        this.isProcessing = false;
        this.router.navigate(['new-progress']);
    }

    /**
     * This function help the template use ngFor, input and Array together.
     * Because of input-array binding, the view (ngFor) is reloaded after type
     * a character. This ngFor must track content by index instead of array content.
     * @param index
     * @param obj
     * @return index
     */
    trackByIndex(index: number, obj: any): any {
        return index;
    }
    
    /**
     * Alert no Internet access
     */
    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Navigate to group manage page with mode 1
     */
    onChooseGroup() {
        this.group.mode = 1;
        this.router.navigateByUrl('app/tabs/tab-menu/group-manage');
    }

    /**
     * Launch sign document with others process
     */
    onSignDocument() {
        this.isProcessing = true;

        if (this.file.cosignAcc == '') {
            this.presentToast('Pleasse choose a group');
            this.isProcessing = false;
            return;
        }

        if (!this.global.isOnline) {
            this.presentToast('No internet access!');
            this.isProcessing = false;
            return;
        }
        
        this.group.selectedGroup = null;
        this.goProgress();
    }
}
