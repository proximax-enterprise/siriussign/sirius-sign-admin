import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import * as ClipboardJS from 'clipboard/dist/clipboard.min.js';

import { Transaction } from 'tsjs-xpx-chain-sdk';
import { DownloadResult } from 'tsjs-chain-xipfs-sdk';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
import * as pdfjsLib from 'pdfjs-dist';
import * as pdfWorker from 'pdfjs-dist/build/pdf.worker';
import * as PDF from 'pdf-lib';
import { rgb } from 'pdf-lib';

import { SignatureCanvas, NamePosition } from '../model/signature-canvas.model';
// import { SiriusDocument } from '../model/sirius-document.model';
import { SiriusSignDocument } from '../model/siriussign-document.model';
import { SignDocument } from '../services/sign-document.service'

// import { SigningService } from './../services/signing.service';
import { GlobalService } from '../services/global.service';
import { UploadStorageService } from './../services/upload-storage.service';
import { DocumentClassificationService, DocumentSigningStatus } from './../services/document-classification.service';
import { DrawSignatureService, SignatureImageInfo, SignatureInfo } from './../services/draw-signature.service';
import { SigningWithoutMultisigService } from './../services/signing-without-multisig.service';
import { HelperService } from './../services/helper.service';
import { DocumentStorageService } from './../services/document-strorage.service';
import { VerifyService } from './../services/verify.service';

@Component({
    selector: 'app-sign-info',
    templateUrl: './sign-info.page.html',
    styleUrls: ['./sign-info.page.scss'],
})
export class SignInfoPage implements OnInit {
    docInfo: SiriusSignDocument = new SiriusSignDocument();
    docInfos: SignDocument;
    transactionHashQr: string = '';
    isCompleted: boolean;
    isVerifier: boolean;
    isVerified: boolean;
    uniLink = '';

    isDownloading: boolean = false;
    isProcessing: boolean = false;

    // Export signed pdf vars
    pdfDoc = null;
    docUint8Array: Uint8Array;
    pageNum: number = 1;
    pageRendering: boolean = false;
    pageNumPending: number = null;
    scale: number = 1;
    pdfCanvas: HTMLCanvasElement;
    pdfCtx: CanvasRenderingContext2D;

    signaturesInfo: SignatureInfo[];
    signCanvases: SignatureCanvas[] = [];
    selectedSignatureIndex: number = 1;

    isInit: boolean = false;

    constructor(
        private location: Location,
        private alertController: AlertController,
        private toastController: ToastController,
        private router: Router,
        private platform: Platform,
        private socialSharing: SocialSharing,
        private clipboard: Clipboard,
        private file: File,
        private fileOpener: FileOpener,
        private global: GlobalService,
        // private signing: SigningService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private documentClassification: DocumentClassificationService,
        private uploadStorage: UploadStorageService,
        private drawSignature: DrawSignatureService,
        private documentStorage: DocumentStorageService,
        private verify: VerifyService
    ) {
        pdfjsLib.GlobalWorkerOptions.workerSrc = pdfWorker;
        console.log(this.documentClassification.selectedDocInfo);
    }

    ngOnInit() { }

    ionViewWillEnter() {
        console.log(this.docInfo);
        console.log(this.documentClassification.selectedDocInfo);
        
        // if (this.documentClassification.universalId != '')
        //     this.documentClassification.selectedDocInfo = this.documentClassification.getDocByDocumentAccount(this.documentClassification.universalId, DocumentSigningStatus.ANY);
        this.docInfo = this.documentClassification.selectedDocInfo;
        this.transactionHashQr = this.docInfo.signTxHash ? HelperService.getQr(this.docInfo.signTxHash, 4) : '';
        this.isCompleted = this.docInfo.cosignatures.length == this.docInfo.cosigners.length;
        this.isVerifier = this.docInfo.verifiers.includes(this.global.loggedAccount.publicKey);
        this.isVerified = this.docInfo.verifierSignatures.length == this.docInfo.verifiers.length;
        if (this.docInfo.id == '') this.onBack();
        this.documentClassification.universalId = '';
        this.uniLink = this.getUniLink();
        var clipboardjs = new ClipboardJS('.btnCopy');
        clipboardjs.on('success', (event: ClipboardJS.Event) => {
            setTimeout(() => { event.clearSelection() }, 200);
        });
        console.log(this.docInfo);
        this.isDownloading = false;
        this.updateDoc();
    }

    /**
     * Update document status
     */
    async updateDoc() {
        if (!this.isCompleted) {
            const doc = await this.documentClassification.fetchDocumentByDocumentAccount(this.docInfo.documentAccount.publicKey);
            doc.localUrl = this.docInfo.localUrl;
            await this.documentStorage.updateDocument(this.docInfo.id, oldDoc => doc);
            this.docInfo = doc;
            this.isCompleted = this.docInfo.cosignatures.length == this.docInfo.cosigners.length;
            this.fetchFromStorage();
        }
    }

    /**
     * Fetch from storage
     */
    async fetchFromStorage() {
        this.documentClassification.clearDocs();
        const docs = await this.documentStorage.fetchDocument();
        if (docs) docs.forEach(doc => this.documentClassification.classify(doc));
        this.documentClassification.sortDocs('compledAndWaiting');
    }

    /**
     * Navigate to previuos page
     */
    onBack() {
        // if (this.isCompleted)
        this.router.navigateByUrl('home');
        // else this.location.back();
    }

    /**
     * Confirm signing alert
     */
    // async onCoSign() {
    //     const alert = await this.alertController.create({
    //         message: 'Confirm Cosign this Document?',
    //         buttons: [{
    //             text: 'Confirm',
    //             handler: () => {
    //                 console.log('Confirm');
    //                 this.cosignDocument();
    //             }
    //         }]
    //     });

    //     await alert.present();
    // }

    /**
     * Navigate to progress page
     */
    goProgress() {
        this.global.setIsProgressDone(false);
        this.router.navigate(['cosign-progress']);
    }

    /**
     * Cosign the selected document
     * version 2 for new signing process
     */
    async onNewCosign() {
        this.isProcessing = true;
        // Download the document
        this.drawSignature.pdfUri = await this.uploadStorage.downloadFileToDataUri(this.docInfo.uploadTxHash)
            .catch(err => {
                this.alertMessage('Failed to download the document', err.message);
                this.isProcessing = false;
                return null;
            });
        if (!this.drawSignature.pdfUri) return;

        console.log(this.drawSignature.pdfUri);

        // Download signatures
        let signatureImgsInfo: SignatureImageInfo[] = [];
        const fetchSignatures = async () => {
            await HelperService.asyncForEach(this.docInfo.signaturesUploadTxHash, async hash => {
                if (hash == '') return;
                const signatureDataUri = await this.uploadStorage.downloadFileToDataUri(hash)
                    .catch(err => {
                        this.alertMessage('Failed to dowload signatures', err.message);
                        this.isProcessing = false;
                        return null;
                    });
                if (!signatureDataUri) return;
                const signatureUploadTx: Transaction = await this.signingWithoutMultisig.fetchTransaction(hash)
                    .catch(err => {
                        this.alertMessage('Failed to dowload signatures', err.message);
                        this.isProcessing = false;
                        return null;
                    });
                if (!signatureUploadTx) return;
                signatureImgsInfo.push({
                    publicKey: signatureUploadTx.signer.publicKey,
                    signatureImg: signatureDataUri
                });
            });
        }
        await fetchSignatures();
        this.drawSignature.signatureImgs = signatureImgsInfo;
        this.drawSignature.signers = this.docInfo.signatures.filter(signature => signature.signaturePosition.name != '');
        this.drawSignature.selectedSignerIndex = this.docInfo.signatures
            .map(signer => signer.publicKey)
            .indexOf(this.global.loggedAccount.publicKey);
        // Case: completely new account with first signing is a cosigner, public key is unknown on network
        if (this.drawSignature.selectedSignerIndex < 0) {
            this.drawSignature.selectedSignerIndex = this.docInfo.signatures
                .map(signer => signer.publicKey)
                .indexOf('0000000000000000000000000000000000000000000000000000000000000000');
        }
        this.drawSignature.cosigner = this.docInfo.signatures[this.drawSignature.selectedSignerIndex];

        this.isProcessing = false;
        this.router.navigate(['cosign-interactive']);
    }

    onVerify() {
        this.verify.clearVerifyResult();
        this.verify.isRealVerify = true;
        this.global.setIsProgressDone(false);
        this.router.navigate(['verify-progress']);
    }

    /**
    * Fetch transaction to get uniLink
    */
    getUniLink() {
        return 'siriussign://siriussign.com/app/sign-info/' + this.docInfo.documentAccount.publicKey;
    }

    /**
     * Invaite cosigner to sign via social apps
     */
    regularShare() {
        let message = 'I remind you that you has not signed the document ' + this.docInfo.name + ' yet. Please open this link in browser to sign it.\n';
        this.socialSharing.share(message, 'SiriusSign cosign invitation', null, this.uniLink);
        if (this.platform.is('desktop')) alert('Copied cosigner invitation link!\n' + this.uniLink);
    }

    /**
     * Present Alert
     * @param message 
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }

    /**
     * Display toast that inform copied message
     */
    async presentToastCopied() {
        const toast = await this.toastController.create({
            message: 'Copied!',
            duration: 1000,
            translucent: true,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Copy transaction hash using cordova plugin
     */
    copyByPlugin(value) {
        this.clipboard.copy(value);
        this.presentToastCopied();
    }

    /**
     * Download pdf file with data uri
     * @param data 
     * @param fileName 
     */
    downloadFileWithDataUri(data, fileName) {
        // Construct the <a> element
        var link = document.createElement("a");
        link.download = fileName;
        // Construct the uri
        var uri = 'data:application/pdf;base64,' + encodeURI(data);
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        // Cleanup the DOM
        document.body.removeChild(link);
    }

    /**
     * Create HTML web view or write file and call opner on mobile to view pdf
     */
    createDocumentView(writeFileName: string, dataUri: string, blob: Blob) {
        if (this.platform.is('desktop')) {
            let pdfWindow = window.open('');
            setTimeout(() => {
                pdfWindow.document.write(`
                            <!DOCTYPE html>
                            <html>
                            <head>
                            <style>
                            .button {
                                background-color: blue;
                                border: none;
                                color: white;
                                padding: 5px 15px;
                                margin-right: 10px;
                                text-align: center;
                                font-size: 14px;
                                cursor: pointer;
                            }

                            .button:hover {
                                background-color: #4CAF50;
                            }

                            a {
                                margin-bottom: 15px;
                                text-decoration: none;
                            }

                            iframe {
                                margin-top: 10px;
                                height: 90vh;
                            }
                            </style>
                            </head>
                            <body>
                            <a 
                                download='` + writeFileName + `'
                                href='` + dataUri + `' >
                                    <button class="button">Download</button>` + writeFileName + ` 
                            </a>
                            <iframe 
                                width='100%' height='100%' 
                                src='` + dataUri + `'></iframe>
                            </body>
                            </html>
                        `);
                // pdfWindow.document.write('<html><body><object width="100%" height="100%" data="data:application/pdf;base64,' + encodeURI(data) + '" type = "application/pdf" ><embed src="data:application/pdf;base64,' + encodeURI(data) + '" type = "application/pdf" /></object></body></html>');
                pdfWindow.document.title = writeFileName;
            }, 100);
            pdfWindow.document.close();

        }
        else {
            let path = '';
            if (this.platform.is('ios')) path = this.file.documentsDirectory;
            else if (this.platform.is('android')) path = this.file.externalDataDirectory;
            const option = {
                replace: true
            }
            this.file.checkFile(path, writeFileName)
                .then(isExist => { if (isExist) console.log('File is exist') });

            this.file.writeFile(path, writeFileName, blob, option)
                .then(res => {
                    console.log(res);
                    this.fileOpener.open(res.nativeURL, 'application/pdf')
                        .then(() => console.log('File is opened'))
                        .catch(err => console.log('Error opening file', err));
                })
                .catch(err => console.log(err));
        }
    }

    /**
     * Lauch file opener on mobile or open decument in new tab on web browser
     */
    async viewDocument() {
        this.isDownloading = true;
        let downloadResult: DownloadResult;
        let buffer: Buffer = null;
        if (this.docInfo.localUrl && this.docInfo.localUrl != '') {
            const seperatorIdx = this.docInfo.localUrl.lastIndexOf('/');
            const path = this.docInfo.localUrl.substring(0, seperatorIdx + 1);
            const file = this.docInfo.localUrl.substring(seperatorIdx + 1)
            buffer = HelperService.convertArrayBufferToBuffer(
                await this.file.readAsArrayBuffer(path, file)
                    .catch(err => {
                        return null;
                    })
            );
        }

        if (!buffer) {
            try {
                downloadResult = this.docInfo.isEncrypt ? await this.uploadStorage.downloadFileEncrypt(this.docInfo.uploadTxHash) :
                    await this.uploadStorage.downloadFile(this.docInfo.uploadTxHash);
            }
            catch (err) {
                this.alertMessage('Failed to download file', err.message);
                this.isDownloading = false;
            }

            buffer = await downloadResult.data.getContentAsBuffer()
                .catch(err => {
                    this.alertMessage('Failed to open file', err.message);
                    this.isDownloading = false;
                    return null;
                });
        }

        const data = buffer.toString('base64');
        const fileName = this.docInfo.name.substring(0, this.docInfo.name.lastIndexOf('.'));
        const fileType = this.docInfo.name.substr(this.docInfo.name.lastIndexOf('.'));
        const sda = ' - SDA' + this.docInfo.documentAccount.publicKey.toUpperCase();
        const writeFileName = fileName + sda + fileType;
        console.log(writeFileName);
        const dataUri = 'data:application/pdf;base64,' + encodeURI(data);
        const blob = new Blob([new Uint8Array(buffer.buffer, buffer.byteOffset, buffer.byteLength)]);
        this.createDocumentView(writeFileName, dataUri, blob);
        this.isDownloading = false;
    }

    /**
     * Render pdf with signature and export yo save or download
     */
    async viewSignedDocument() {
        this.isDownloading = true;

        if (this.docInfo.localUrl || this.docInfo.localUrl != '') {
            const seperatorIdx = this.docInfo.localUrl.lastIndexOf('/');
            const path = this.docInfo.localUrl.substring(0, seperatorIdx + 1);
            const file = this.docInfo.localUrl.substring(seperatorIdx + 1);

            this.drawSignature.pdfUri = await this.file.readAsDataURL(path, file).catch(e => {
                return null;
            });
        }

        if (!this.drawSignature.pdfUri) {
            // Download the document
            this.drawSignature.pdfUri = await this.uploadStorage.downloadFileToDataUri(this.docInfo.uploadTxHash, this.docInfo.isEncrypt)
                .catch(err => {
                    this.alertMessage('Failed to download the document', err.message);
                    this.isProcessing = false;
                    return null;
                });
            if (!this.drawSignature.pdfUri) return;
        }

        console.log(this.drawSignature.pdfUri);

        // Download signatures
        let signatureImgsInfo: SignatureImageInfo[] = [];
        const fetchSignatures = async () => {
            await HelperService.asyncForEach(this.docInfo.signaturesUploadTxHash, async hash => {
                if (hash == '') return;
                const signatureDataUri = await this.uploadStorage.downloadFileToDataUri(hash)
                    .catch(err => {
                        this.alertMessage('Failed to dowload signatures', err.message);
                        this.isProcessing = false;
                        return null;
                    });
                if (!signatureDataUri) return;
                const signatureUploadTx: Transaction = await this.signingWithoutMultisig.fetchTransaction(hash)
                    .catch(err => {
                        this.alertMessage('Failed to dowload signatures', err.message);
                        this.isProcessing = false;
                        return null;
                    });
                if (!signatureUploadTx) return;
                signatureImgsInfo.push({
                    publicKey: signatureUploadTx.signer.publicKey,
                    signatureImg: signatureDataUri
                });
            });
        }
        await fetchSignatures();
        this.drawSignature.signatureImgs = signatureImgsInfo;
        this.drawSignature.signers = this.docInfo.signatures.filter(signature => signature.signaturePosition.name != '');
        this.drawSignature.name = this.docInfo.name;

        // const signedDocBlob = await this.exportSignedDoc();
        // const data = window.URL.createObjectURL(signedDocBlob);

        // const fileName = this.docInfo.name.substring(0, this.docInfo.name.lastIndexOf('.'));
        // const fileType = this.docInfo.name.substr(this.docInfo.name.lastIndexOf('.'));
        // const writeFileName = fileName + '-signed' + fileType;
        // console.log(writeFileName);
        // this.createDocumentView(writeFileName, data, signedDocBlob);
        // this.isDownloading = false;

        this.isDownloading = false;
        this.router.navigate(['signed-doc-view']);
    }

    /**
     * Draw pdf and signature image on canvases
     */
    async exportSignedDoc() {
        document.documentElement.style.setProperty('--width-container', '595px');
        this.signaturesInfo = this.drawSignature.signers;
        this.selectedSignatureIndex = this.drawSignature.selectedSignerIndex;
        this.isInit = true;

        this.pdfCanvas = <HTMLCanvasElement>document.getElementById('pdf');
        this.pdfCtx = this.pdfCanvas.getContext('2d');

        // Device Pixel Ratio
        const dpr = window.devicePixelRatio;

        // Set pdf
        this.docUint8Array = HelperService.convertDataURIToBinary(this.drawSignature.pdfUri);
        this.pdfDoc = await pdfjsLib.getDocument(this.docUint8Array).promise;

        // Initial/first page rendering
        await this.renderPage(this.pageNum);

        // Generate signature canvases
        this.signaturesInfo.forEach((signatureInfo, index) => {
            this.signCanvases[index] = new SignatureCanvas(signatureInfo.publicKey, signatureInfo.signaturePosition.name);
        });

        const signatureImgPublicKeys = this.drawSignature.signatureImgs.map(imgInfo => imgInfo.publicKey);
        const createCanvases = async () => HelperService.asyncForEach(this.signCanvases, async (canvas, index) => {
            canvas.canvasSize.width = this.pdfCanvas.width;
            canvas.canvasSize.height = this.pdfCanvas.height;

            // Check and get signed signature image
            const indexOfImg = signatureImgPublicKeys.indexOf(canvas.id);
            const signatureImgSrc = await HelperService.getPngSignature(this.drawSignature.signatureImgs[indexOfImg].signatureImg);
            const otherSignatureImgObj: HTMLImageElement = document.createElement('img');
            otherSignatureImgObj.src = signatureImgSrc;
            otherSignatureImgObj.height = 44;
            otherSignatureImgObj.width = 188;
            // Setup canvas
            canvas.create('signature-' + index, otherSignatureImgObj, this.scale * dpr);
            canvas.pageNumber = this.drawSignature.signers[index].signaturePosition.page;
            canvas.x = this.drawSignature.signers[index].signaturePosition.x;
            canvas.y = this.drawSignature.signers[index].signaturePosition.y;
            canvas.namePosition = this.drawSignature.signers[index].signaturePosition.namePosition;
            canvas.isNamePlace = canvas.namePosition != NamePosition.NONE;
            canvas.isInit = true;

            if (!(this.global.isBrowser) || (dpr != 1)) {
                const canvasElement = document.getElementById('signature-' + index);
                canvasElement.style.width = '100%';
            }
        });
        await createCanvases();

        this.signCanvases.forEach((canvas, index) => {
            // Draw signature image
            canvas.drawInactive(this.pageNum);

            // Set z-index for canvas
            const canvasElement = document.getElementById('signature-' + index);
            canvasElement.style.zIndex = (index == this.selectedSignatureIndex) ? '2' : '1';
        });

        return await this.embedPdf();
    }

    /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param num Page number.
    */
    async renderPage(num) {
        this.pageRendering = true;
        // Using promise to fetch the page
        const page = await this.pdfDoc.getPage(num)

        // const pdfScreenRatio = window.innerWidth / page.view[2];
        // this.scale = pdfScreenRatio > 1 ? 1 : pdfScreenRatio;
        if (this.scale == 1) document.documentElement.style.setProperty('--width-container', page.view[2] + 'px');

        // Scale up for crystal clear image
        const dpr = window.devicePixelRatio;
        var viewport = page.getViewport({ scale: this.scale * dpr });
        this.pdfCanvas.height = viewport.height;
        this.pdfCanvas.width = viewport.width;

        // Scale down for right scale ratio
        const canvas = document.getElementById(this.pdfCanvas.id);
        if ((!this.global.isBrowser) || (dpr != 1))
            canvas.style.width = '100%';

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: this.pdfCtx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(() => {
            this.pageRendering = false;
            if (this.pageNumPending !== null) {
                // New page rendering is pending
                this.renderPage(this.pageNumPending);
                this.pageNumPending = null;
            }
        });

        // Update signature
        this.signCanvases.forEach(canvas => canvas.drawInactive(this.pageNum));
    }

    /**
     * Embed signature image to pdf
     */
    async embedPdf() {
        // Load
        const pdfDoc = await PDF.PDFDocument.load(this.docUint8Array);
        const pages = pdfDoc.getPages();
        // const canvasPngImage = await pdfDoc.embedPng(canvasImageBuffer);
        const dpr = window.devicePixelRatio;

        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(PDF.StandardFonts.Helvetica);

        // Draw
        const embedImg = async () => HelperService.asyncForEach(this.signCanvases, async (signCanvas: SignatureCanvas, index) => {
            const embedPage = pages[signCanvas.pageNumber - 1];
            const { width, height } = embedPage.getSize();
            embedPage.setFont(helveticaFont);
            embedPage.setFontSize(14);  //px

            if (signCanvas.isNamePlace) {
                const namePosition = signCanvas.namePosition;
                const nameRect = signCanvas.computeNameRect(namePosition);

                const isNameCenter = (namePosition == NamePosition.TOP) || (namePosition == NamePosition.BOTTOM);
                let nameX = nameRect.topLeftX;
                const nameY = this.pdfCanvas.height / dpr - nameRect.bottomRightY + 16;

                if (isNameCenter) {
                    this.pdfCtx.font = "14px Helvetica";
                    const textMetric = this.pdfCtx.measureText(signCanvas.name);
                    const centerNameX = (nameRect.topLeftX + nameRect.bottomRightX) / 2 - textMetric.width / 2;
                    nameX = centerNameX;
                }

                if (namePosition == NamePosition.LEFT) {
                    this.pdfCtx.font = "14px Helvetica";
                    const textMetric = this.pdfCtx.measureText(signCanvas.name);
                    const rightAlignNameX = nameRect.bottomRightX - textMetric.width - 5;
                    nameX = rightAlignNameX;
                }

                embedPage.drawText(signCanvas.name, {
                    x: nameX,
                    y: nameY,
                    color: rgb(0, 0, 0)
                });
            }

            const signatureImgBuffer = HelperService.convertDataURIToBinary(signCanvas.image.src);
            const signaturePngImg = await pdfDoc.embedPng(signatureImgBuffer);

            const position = signCanvas.getPosition();
            const embedImg = signaturePngImg;
            embedPage.drawImage(embedImg, {
                x: position.x,
                y: height - position.y - signCanvas.image.height,
                height: signCanvas.image.height,
                width: signCanvas.image.width
            });
        });
        await embedImg();

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();
        let blob = new Blob([pdfBytes], { type: "application/pdf" });
        return blob;
    }
}