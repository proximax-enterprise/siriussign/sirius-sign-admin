import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInfoPage } from './sign-info.page';

xdescribe('SignInfoPage', () => {
    let component: SignInfoPage;
    let fixture: ComponentFixture<SignInfoPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignInfoPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignInfoPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
