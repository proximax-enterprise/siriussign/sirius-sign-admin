import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CodeVerifyPage } from './code-verify.page';

const routes: Routes = [
    {
        path: '',
        component: CodeVerifyPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CodeVerifyPage]
})
export class CodeVerifyPageModule { }
