import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyProgressPage } from './verify-progress.page';

xdescribe('VerifyProgressPage', () => {
    let component: VerifyProgressPage;
    let fixture: ComponentFixture<VerifyProgressPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VerifyProgressPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VerifyProgressPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
