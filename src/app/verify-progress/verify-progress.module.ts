import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VerifyProgressPage } from './verify-progress.page';

const routes: Routes = [
    {
        path: '',
        component: VerifyProgressPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [VerifyProgressPage]
})
export class VerifyProgressPageModule { }
