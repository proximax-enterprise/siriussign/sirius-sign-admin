import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitOthersPage } from './wait-others.page';

xdescribe('WaitOthersPage', () => {
    let component: WaitOthersPage;
    let fixture: ComponentFixture<WaitOthersPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WaitOthersPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WaitOthersPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
