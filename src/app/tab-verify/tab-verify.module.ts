import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabVerifyPage } from './tab-verify.page';

import { ComponentModule } from './../components/component.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ComponentModule,
        RouterModule.forChild([{ path: '', component: TabVerifyPage }])
    ],
    declarations: [TabVerifyPage]
})
export class TabVerifyPageModule { }
