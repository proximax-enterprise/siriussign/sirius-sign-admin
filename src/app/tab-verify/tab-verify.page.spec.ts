import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabVerifyPage } from './tab-verify.page';

xdescribe('Tab2Page', () => {
    let component: TabVerifyPage;
    let fixture: ComponentFixture<TabVerifyPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TabVerifyPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TabVerifyPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
