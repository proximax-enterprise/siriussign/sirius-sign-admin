import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyFileViewPage } from './verify-file-view.page';

xdescribe('VerifyFileViewPage', () => {
    let component: VerifyFileViewPage;
    let fixture: ComponentFixture<VerifyFileViewPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VerifyFileViewPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VerifyFileViewPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
