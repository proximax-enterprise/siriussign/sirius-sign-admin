import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from '../services/helper.service';

@Component({
    selector: 'app-verify-file-view',
    templateUrl: './verify-file-view.page.html',
    styleUrls: ['./verify-file-view.page.scss'],
})
export class VerifyFileViewPage implements OnInit {

    docs = [
        {
            name: 'Demo-waiting.pdf',
            lastModifiedDate: new Date(2019, 6, 27)
        }
    ];

    constructor(private router: Router) { }

    ngOnInit() {
    }

    dateToShortString = HelperService.dateToShortString;

    /**
     * Navigate to audit progress
     */
    goProgress() {
        this.router.navigate(['verify-progress']);
    }
}
