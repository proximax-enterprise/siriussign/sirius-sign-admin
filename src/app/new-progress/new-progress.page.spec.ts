import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProgressPage } from './new-progress.page';

xdescribe('NewProgressPage', () => {
    let component: NewProgressPage;
    let fixture: ComponentFixture<NewProgressPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NewProgressPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewProgressPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
