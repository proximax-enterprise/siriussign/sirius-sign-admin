import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CosignDonePage } from './cosign-done.page';

xdescribe('CosignDonePage', () => {
  let component: CosignDonePage;
  let fixture: ComponentFixture<CosignDonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CosignDonePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CosignDonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
