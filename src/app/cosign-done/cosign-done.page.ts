import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SiriusSignCosigningTask } from '../model/siriussign-task.model';

import { HelperService } from '../services/helper.service';
import { MultitaskService } from './../services/multitask.service';

@Component({
    selector: 'app-cosign-done',
    templateUrl: './cosign-done.page.html',
    styleUrls: ['./cosign-done.page.scss'],
})
export class CosignDonePage implements OnInit {

    file = {
        name: '',
        size: 'unknow',
        uploadDate: '',
        hashFunc: 'SHA256',
        storage: 'Private, transferable',
        fee: 0.2,
        status: 'Confirmed',
        owner: '',
        fileHash: '',
        transactionHash: ''
    };

    constructor(
        private router: Router,
        private multitask: MultitaskService
    ) { }

    ngOnInit() {
        const doc                 = <SiriusSignCosigningTask>this.multitask.selectedTask;
        this.file.name            = doc.name;
        this.file.uploadDate      = HelperService.dateToShortString(doc.selectedDocInfo.uploadDate);
        this.file.owner           = doc.selectedDocInfo.owner;
        this.file.fileHash        = doc.selectedDocInfo.fileHash.sha256Hash;
        this.file.transactionHash = doc.selectedDocInfo.signTxHash;
        this.file.status          = doc.selectedDocStatus;
        
    }

    /*
     * Naviagte to home
     */
    goHome() {
        const taskIndex = this.multitask.tasks.indexOf(this.multitask.selectedTask);
        this.multitask.tasks.splice(taskIndex, 1);
        this.multitask.selectedTask = null;
        this.router.navigate(['app']);
    }
}
