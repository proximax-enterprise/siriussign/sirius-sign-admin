import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrganizationsService } from '../services/organizations.service';
import { Transaction } from 'tsjs-xpx-chain-sdk';
import { SignDocumentService } from '../services/sign-document.service';
import { MenuController, Platform, AlertController } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { HelperService } from './../services/helper.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { ToastController } from '@ionic/angular';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { GlobalService } from './../services/global.service';
import { GroupService } from '../services/group.service';
import { DrawSignatureService } from './../services/draw-signature.service';
import { SignatureService } from './../services/signature.service';

import { SiriusSignTask, SiriusSignSigningTask, SiriusSignCosigningTask } from './../model/siriussign-task.model';

import { MultitaskService } from './../services/multitask.service';

interface ToSignFile {
  name: string;
  size: string;
  uploadDate: string;
  owner: {
      publicKey: string,
      name: string,
      isRequireSignatureImg: boolean
  };
  cosigners: string[];
  verifiers: string[];
  hashFunc: 'SHA256';
  fileHash: string;
  storage: 'Public' | 'Private';
  fee: 'No fee' | number;
}

@Component({
  selector: 'app-create-organizations',
  templateUrl: './create-organizations.page.html',
  styleUrls: ['./create-organizations.page.scss'],
})
export class CreateOrganizationsPage implements OnInit {
  isPutAllSignatures: boolean = false;
  doc = {
    name: this.signDoc.document.file.name,
    signDate: this.signDoc.document.signDate
  };

  uri = this.signDoc.document.file.dataURI;
  data = this.signDoc.document.file.data.toString();
  onChooser = false;
  namespaceNames: string[]; // check namespaces of owner created
  signatories: string[] = [];
  signatory: string;
  documents: any[];
  isDone: boolean;

  ////////// Multi sign/////////////////
  file: ToSignFile = {
    name: '',
    size: 'Unknown',
    uploadDate: '',
    owner: {
        publicKey: '',
        name: '',
        isRequireSignatureImg: false
    },
    cosigners: [],
    verifiers: [],
    hashFunc: 'SHA256',
    fileHash: '',
    storage: 'Public',
    fee: 'No fee'
  };

  labels = {
      name: 'File name',
      size: 'Size',
      uploadDate: 'Upload date',
      owner: 'Your information',
      cosigners: 'Cosigners',
      verifiers: 'Verifiers',
      hashFunc: 'Hash function',
      fileHash: 'File Hash (SHA256)',
      storage: 'Storage',
      fee: 'Network fee'
  };

  fields = ['name', 'size', 'uploadDate', 'owner', 'cosigners', 'verifiers', 'fileHash', 'storage', 'fee'];
  isProcessing: boolean = false;

  constructor(
    private router: Router,
    private organizationsService: OrganizationsService,
    private signDoc: SignDocumentService,
    private menu: MenuController,
    private alertController: AlertController,
    private chooser: Chooser,
    private platform: Platform,
    private device: Device,
    private filePath: FilePath,
    private fileOpener: FileOpener,
    private toastController: ToastController,
    private barcodeScanner: BarcodeScanner,
    private global: GlobalService,
    private group: GroupService,
    private drawSignature: DrawSignatureService,
    private signature: SignatureService,
    public multitasks: MultitaskService
  ) {
    console.log(this.platform);
    this.onChooser = this.device.platform != 'browser' && this.device.platform != null;
    //!this.platform.is('desktop') || !this.platform.is('mobileweb');
    console.log(this.onChooser);
    // this.a();
   }



  ngOnInit() {
  }
  async getNamespace() {
    this.namespaceNames = await this.organizationsService.fetchNamespace();
  }

  ionViewWillLeave() {
    this.organizationsService.isSignatories = false;
    this.organizationsService.isDocument = false;
    this.organizationsService.isNotify = false;
  }
  navToCreateProject() {
    this.router.navigateByUrl('/create-new-project');
  }
  navToTest() {
    this.router.navigateByUrl('/test');
  }
  navToCreate() {
    this.router.navigateByUrl('/create-organizations');
    this.organizationsService.isSignatories = false;
    this.organizationsService.isDocument = false;
    this.organizationsService.isNotify = false;
  }
  navToNew() {
    this.router.navigateByUrl('/new-organization');
  }
  navToAmin() {
    this.router.navigateByUrl('/admin');
  }
  changeToSignatories() {
    this.organizationsService.isSignatories = true;
    this.organizationsService.isDocument = false;
    this.organizationsService.isNotify = false;
  }
  changeToDocument() {
    this.organizationsService.isDocument = true;
    this.organizationsService.isSignatories = false;
    this.organizationsService.isNotify = false;
  }
  changeToNotify() {
    this.organizationsService.isNotify = true;
    this.organizationsService.isDocument = false;
    this.organizationsService.isSignatories = false;
    //////////////////////// change to multi sign //////////////////////
    this.isProcessing = false;
    const doc = this.signDoc.getDocmument();
    this.file.name = doc.file.name;
    this.file.uploadDate = HelperService.dateToShortString(doc.signDate);
    this.file.fileHash = doc.fileHash;
    this.file.size = HelperService.sizeToString(this.signDoc.document.size);
    this.file.owner.publicKey = this.global.loggedAccount.publicKey;
    this.file.owner.name = this.signature.name;
    // if (this.group.selectedGroup) this.file.cosignAcc = this.group.selectedGroup.name;
    // else this.file.cosignAcc = '';
    this.drawSignature.reset();

    // Reset signing doc in case of back from interactive
    this.signDoc.document.cosigners = [];
  }
  addPublicKey() {
    this.file.cosigners.push(this.signatory);
    console.log(this.file.cosigners);
  }
  removePublickey(publicKey: string) {
    for ( let i = 0; i < this.file.cosigners.length; i++){
      if ( this.file.cosigners[i] === publicKey) { this.file.cosigners.splice(i, 1); i--; }
    }
    console.log(this.file.cosigners);
  }
  addSignatories() {
    console.log(this.file.cosigners);
    if (this.file.cosigners.length == 0) {
      alert('Please add signatories!!!');
    } else{
      // this.organizationsService.organizationManage.signatories = this.signatories;
      for (let i = 0; i < this.file.cosigners.length; i++) {
        if (!this.checkCosignerKey(i)) {
            this.presentToast('Invalid cosigner\'s public keys');
            this.isProcessing = false;
            return;
        }
      }
      this.changeToDocument();
    }
  }
  addDocuments() {
    this.changeToNotify();
  }
  addNotify() {
    this.organizationsService.isNotify = false;
    this.organizationsService.isDocument = false;
    this.organizationsService.isSignatories = false;
  }
  clearPublickey() {
    this.signatories = [];
  }
  async openOrganization(organization) {
    this.organizationsService.setQuerykeySubnamespace(organization);
    console.log(this.organizationsService.queryKeySubNamespace);
    this.organizationsService.namespaceName = organization;
    this.navToCreateProject();
  }

  /////////////////// browse file ////////////////////////

  onFile(files) {
    console.log(files);
    const reader = new FileReader();
    reader.onload = (event) => {
        console.log(event.target);
        const target = <any>event.target;
        const file = {
          data: this.convertDataURIToBinary(target.result),
          dataURI: target.result,
          mediaType: target.result.split(';')[0].split(':')[1],
          name: files[0].name,
          uri: ''
        }
        console.log(file);
        if (file.mediaType != 'application/pdf') {
            console.log('alert');
            return;
        }
        this.signDoc.setDocument(file, files[0].size);
        let input = <any>document.getElementById("browseNew");
        input.value = '';
        // this.goNewFile();
        console.log(this.signDoc.document);
    };
    reader.readAsDataURL(files[0]);
  }
  convertDataURIToBinary(dataURI: string) {
    const BASE64_MARKER = ';base64,';
    const base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    const base64 = dataURI.substring(base64Index);
    const raw = window.atob(base64);
    const rawLength = raw.length;
    const array = new Uint8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
  }
  /**
   * Close the side menu
   */
  closeMenu() {
      this.menu.close("menu");
  }
  /**
   * Navigate to new file view
   */
  goNewFile() {
      this.router.navigate(['new-file-view']);
  }
  /**
   * Alert invalid file type
   */
  async alertInvalidFileType() {
      const alert = await this.alertController.create({
          header: 'Invalid File Type',
          message: 'SirisuSign supports .pdf file only.',
          buttons: ['OK']
      });

      await alert.present();
  }
  /////////////// view-file ///////////////

  ionViewWillEnter() {
    this.getNamespace();
    this.doc = {
        name: this.signDoc.document.file.name,
        signDate: this.signDoc.document.signDate
    }
  }

  dateToShortString = HelperService.dateToShortString;

  /**
   * Navigate to file info page
   */
  goSign() {
      // this.router.navigate(['new-only-me']);
      this.router.navigate(['new-only-me']);
  }

  /**
   * Call a program to view file
   */
  onView() {
      // if (this.platform.is('android')) {
      //     this.filePath.resolveNativePath(this.signDoc.document.file.uri)
      //         .then(filePath => {
      //             console.log(filePath);
      //             this.fileOpener.open(filePath, 'application/pdf')
      //                 .then(() => console.log('File is opened'))
      //                 .catch(e => console.log('Error opening file', e));
      //         })
      //         .catch(err => console.log(err));
      // }

      // if (this.platform.is('ios')) {
      //     this.fileOpener.open(this.signDoc.document.file.uri, 'application/pdf')
      //         .then(() => console.log('File is opened'))
      //         .catch(e => console.log('Error opening file', e));
      // }

      if (this.platform.is('desktop')) {
          let pdfWindow = window.open('');
          pdfWindow.document.write("<iframe width='100%' height='100%' src='" + encodeURI(this.signDoc.document.file.dataURI) + "'></iframe>")
          pdfWindow.document.close();
      }
  }
  //////////////////////////// Multi sign/////////////////////////////////////////////

      /**
     * Naviagte to new-only-me
     */
    goOnlyMe() {
      this.router.navigate(['new-only-me']);
  }

  /**
   * Navigate to progress page
   */
  goProgress() {
      this.global.setIsProgressDone(false);
      this.isProcessing = false;
      this.router.navigate(['new-progress']);
  }

  /**
   * Navigate to progress page change to go new-progress
   */

  async checkNetwork() {
    if (!this.global.isOnline) {
        this.presentToast('No Internet access!');
        return false;
    }

    await this.global.checkNode(this.global.apiNode);

    if (!this.global.isNodeWorking) {
        this.presentToast('API Node Failure..')
        return false;
    }

    return true;
}
  async goInteractive() {
      // this.isProcessing = false;
      // const isAccessible = await this.checkNetwork();
      // if (!isAccessible) return;

      // if (this.isPutAllSignatures) {
      //     this.global.setIsProgressDone(false);
      //     this.router.navigate(['new-progress']);
      // }
      this.router.navigate(['new-interactive']);
  }

  // toggleIsRequireSignatureImg(index: number) {
  //     if (index == -1) {
  //         this.file.owner.isRequireSignatureImg = !this.file.owner.isRequireSignatureImg;
  //     }
  //     else {
  //         this.file.cosigners[index].isRequireSignatureImg = !this.file.cosigners[index].isRequireSignatureImg;
  //     }
  // }

  /*
   * Add one more multisig account to list
   */
  // onAdd() {
  //     let length = this.file.cosigners.length;
  //     if (length < 15) this.file.cosigners.push({ publicKey: '', name: '', isRequireSignatureImg: false });
  // }

  /*
   * Remove a multisig account from list
   * @param index - index of the address need be removed
   */
  // onRemove(index: number, flag: 'cosigners' | 'verifiers') {
  //     if (flag == 'cosigners') this.file.cosigners.splice(index, 1);
  //     if (flag == 'verifiers') this.file.verifiers.splice(index, 1);
  // }

  /**
   * Scan QR code to get cosigner information
   * @param index 
  //  */
  // onScan(index: number, flag: 'cosigners' | 'verifiers') {
  //     this.barcodeScanner.scan().then(barcodeData => {
  //         const info = JSON.parse(barcodeData.text)
  //         if (flag == 'cosigners') {
  //             this.file.cosigners[index].name = info.name;
  //             this.file.cosigners[index].publicKey = info.publicKey;
  //         }
  //         if (flag == 'verifiers')
  //             this.file.verifiers[index] = info.publicKey;
  //     }).catch(err => {
  //         console.log('Error', err);
  //     });
  // }

  /**
   * Check if cosigner public key is valid
   * @param index 
   */
  checkCosignerKey(index: number) {
    this.file.cosigners[index] = this.file.cosigners[index].toUpperCase();
    const hexPattern = RegExp('^[a-fA-F0-9]+$');
    if (this.file.cosigners[index].includes(' ')) { return false; }
    const isEmpty = this.file.cosigners[index] == '';
    const isHexString = hexPattern.test(this.file.cosigners[index]);
    const isRightLength = this.file.cosigners[index].length == 64;
    return (isEmpty || (isRightLength && isHexString));
  }

  /**
   * Check if verifier public key is valid
   * @param index 
   */
  checkVerifierKey(index: number, value?) {
      if (value) {
          this.file.verifiers[index] = value.toUpperCase();
      } // this.file.verifiers[index].toUpperCase();
      else {
          this.file.verifiers[index] = this.file.verifiers[index].toUpperCase();
      }
      const hexPattern = RegExp('^[a-fA-F0-9]+$');
      if (this.file.verifiers[index].includes(' ')) { return false; }
      const isEmpty = this.file.verifiers[index] == '';
      const isHexString = hexPattern.test(this.file.verifiers[index]);
      const isRightLength = this.file.verifiers[index].length == 64;
      return (isEmpty || (isRightLength && isHexString));
  }


  /**
   * This function help the template use ngFor, input and Array together.
   * Because of input-array binding, the view (ngFor) is reloaded after type
   * a character. This ngFor must track content by index instead of array content.
   * @param index
   * @param obj
   * @return index
   */
  // trackByIndex(index: number, obj: any): any {
  //     return index;
  // }

  /**
   * Alert by toast
   */
  async presentToast(msg: string) {
      const toast = await this.toastController.create({
          message: msg,
          duration: 2000,
          position: 'top'
      });
      toast.present();
  }

  /**
   * Navigate to group manage page with mode 1
   */
  // onChooseGroup() {
  //     this.group.mode = 1;
  //     this.router.navigateByUrl('app/tabs/tab-menu/group-manage');
  // }

  /**
   * Launch sign document with others process
   */
  onSignDocument() {
      this.isProcessing = true;

      if (this.file.cosigners.length == 0) {
        this.presentToast('Pleasse add cosigners');
        this.isProcessing = false;
        return;
      }

      if (!this.global.isOnline) {
        this.presentToast('No internet access!');
        this.isProcessing = false;
        return;
      }

      if (this.file.owner.isRequireSignatureImg && this.file.owner.name == '') {
        this.presentToast('Please input your name');
        this.isProcessing = false;
        return;
      }

      this.file.cosigners = this.file.cosigners.filter(value => value != '');
      if (this.file.cosigners.length == 0) {
        this.file.cosigners.push( '' );
        this.presentToast('Please input cosigner\'s information');
        this.isProcessing = false;
        return;
      }
      this.file.verifiers = this.file.verifiers.filter(value => value != '');
      for (let i = 0; i < this.file.verifiers.length; i++) {
          if (!this.checkVerifierKey(i)) {
              this.presentToast('Invalid verifier\'s public key');
              this.isProcessing = false;
              return;
          }
      }

      // Check if verifiers are signers
      const signers = [this.file.owner.publicKey, ...this.file.cosigners.map(signer => signer)];
      const intersec = HelperService.intersecArray(this.file.verifiers, signers);
      if (intersec.length > 0) {
        this.presentToast('Signer cannot be a verifier');
        this.isProcessing = false;
        return;
      }

      // Set signers to draw signature
      this.drawSignature.pdfUri = this.signDoc.document.file.dataURI;
      if (this.file.owner.isRequireSignatureImg) {
        this.drawSignature.setSigner(this.file.owner.publicKey, this.file.owner.name);
      }
      // this.file.cosigners.forEach(cosigner => {
      //   if (cosigner.isRequireSignatureImg) {
      //     this.drawSignature.setSigner(cosigner.publicKey, cosigner.name);
      //   }
      // });

      // Set cosigners and verifiers to document
      this.signDoc.document.cosigners = this.file.cosigners.map(cosigner => cosigner);
      this.signDoc.document.verifiers = this.file.verifiers;

      console.log(this.signDoc.document.cosigners);
      console.log(this.signDoc.document.verifiers);
      console.log(this.drawSignature.signers);

      this.group.selectedGroup = null;
      this.isProcessing = false;
      this.goInteractive();
  }

  ///////////////// Multitask ////////////////
    /**
     * Set stroke dash array of progress circle
     * @param task
     */
  calStrokeDashArray(task) {
    return task.percent + ', 100';
  }

  /**
   * Open task progress
   * @param task 
   */
  onTask(task: SiriusSignTask) {
    if (task.transactionStatus.isDone) {
      this.multitasks.selectedTask = task;
      if (task instanceof SiriusSignSigningTask) {
        this.router.navigate(['new-done']);
      }
      else if (task instanceof SiriusSignCosigningTask) {
        this.router.navigate(['cosign-done']);
      }
    }
  }
}
