import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHistoryPage } from './tab-history.page';

xdescribe('TabHistoryPage', () => {
    let component: TabHistoryPage;
    let fixture: ComponentFixture<TabHistoryPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TabHistoryPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TabHistoryPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
