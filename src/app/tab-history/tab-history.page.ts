import { GlobalService } from './../services/global.service';
import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';

import { HelperService } from '../services/helper.service';
import { HistoryService } from './../services/history.service';
import { DocumentSigningStatus, DocumentClassificationService } from './../services/document-classification.service';

@Component({
    selector: 'app-tab-history',
    templateUrl: 'tab-history.page.html',
    styleUrls: ['tab-history.page.scss']
})

export class TabHistoryPage {
    @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

    ownerPublicKey: string;

    constructor(
        private router: Router,
        private global: GlobalService,
        public documentClassification: DocumentClassificationService
    ) { 
        this.ownerPublicKey = this.global.loggedAccount.publicKey;
    }

    ngOnInit() {
    }

    dateToShortString = HelperService.dateToShortString;

    nowFromDate = HelperService.nowFromDate;

    loadData(event) {
        setTimeout(() => {
            console.log('Load more history');
            // if (!this.history.isLast) this.history.fetchTransactions();
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            // if (this.history.docs.length == 1000) {
            //     event.target.disabled = true;
            // }
        }, 500);
    }

    /**
     * View document info
     */
    onDocument(index: number) {
        this.documentClassification.selectedDocInfo = this.documentClassification.getDocByIndex(index, DocumentSigningStatus.COMPLETED);
        this.router.navigate(['sign-info']);
    }

    /**
     * Refresher
     * @param event 
     */
    async doRefresh(event) {
        await this.documentClassification.fetchCompletedAndWaiting();
        event.target.complete();
    }
}
