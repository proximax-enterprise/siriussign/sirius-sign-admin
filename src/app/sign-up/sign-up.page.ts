import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';

import { WalletService } from './../services/wallet.service';
import { GlobalService } from '../services/global.service';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.page.html',
    styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

    showForm = false;
    errorMess = '';
    inputDat = {
        email: '',
        inputPass: '',
        verifPass: ''
    }

    isSignUpClicked = false;

    constructor(
        private router: Router,
        private wallet: WalletService,
        private firebaseAuth: AngularFireAuth,
        private alertCtrl: AlertController,
        private global: GlobalService
    ) {
        this.setStyleDefault();
    }

    ngOnInit() {
        this.isSignUpClicked = false;
    }

    ionViewWillEnter() {
        this.setStyleDefault();
        this.isSignUpClicked = false;

        // Observe Keyboard show event
        window.addEventListener('keyboardWillShow', (e) => {
            this.setStyle(':root', '--height-logo', 'fit-content');
            this.setStyle(':root', '--padding-top-logo', '20px');
            this.setStyle(':root', '--height-welcomeoff', 'fit-content');
            this.setStyle(':root', '--padding-top-welcomeoff', '5px');
            this.setStyle(':root', '--height-form', 'fit-content');
        });

        // Observe Keyboard hide event
        window.addEventListener('keyboardWillHide', () => {
            this.setStyleDefault();
        });
    }

    /**
     * Set style value
     * @param selector 
     * @param property 
     * @param value 
     */
    setStyle(selector: string, property: string, value: string) {
        // const el = <HTMLElement>document.querySelector(selector);
        // el.style.setProperty(property, value);
        document.documentElement.style.setProperty(property, value);
    }

    /**
     * Set style to default values
     */
    setStyleDefault() {
        this.setStyle(':root', '--height-logo', '30vh');
        this.setStyle(':root', '--padding-top-logo', '15vh');
        this.setStyle(':root', '--height-welcomeoff', '10vh');
        this.setStyle(':root', '--padding-top-welcomeoff', '3vh');
        this.setStyle(':root', '--height-form', '50vh');
    }

    /**
     * Navigate to sign-in page
     */
    goSignIn() {
        this.router.navigate(['sign-in']);
    }

    /**
     * Navigate to recovery page
     */
    goRecovery() {
        this.router.navigate(['recovery']);
    }

    /**
     * Navigate to code-verify page
     */
    goCodeVerify() {
        this.router.navigate(['code-verify']);
    }

    /**
     * Validate email input 
     */
    checkEmail() {
        const emailPattern = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
        let isMatch = emailPattern.test(this.inputDat.email.toLowerCase());
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your email is invalid.';
            return false;
        }
    }

    /**
     * Validate password input
     * @returns {boolean}
     */
    checkPass() {
        let passLen = this.inputDat.inputPass.length;
        if ((passLen < 8) && (passLen > 0)) {
            this.errorMess = 'Password must be at least 8 charaters.';
            return false;
        }
        else {
            this.errorMess = '';
        }
        let isMatch = (this.inputDat.verifPass === this.inputDat.inputPass) || (this.inputDat.verifPass === '');
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your password and confirm password do not match.'
            return false;
        }
    }

    /**
     * Validate form input
     * @returns
     */
    checkInput() {
        if (this.checkEmail()) return this.checkPass();
        return false;
    }

    /**
     * Launch sign up process
     * @async
     */
    async signUp() {
        this.isSignUpClicked = true;

        //Validate info
        let isNoEmail = this.inputDat.email == '';
        let isNoInputPass = this.inputDat.inputPass == '';
        let isNoVerifPass = this.inputDat.verifPass == '';

        if (isNoEmail) {
            this.errorMess = 'Email is required.';
            this.isSignUpClicked = false;
            return;
        }

        if (isNoInputPass) {
            this.errorMess = 'Password is required.';
            this.isSignUpClicked = false;
            return;
        }

        if (isNoVerifPass) {
            this.errorMess = 'Confirm password is required.';
            this.isSignUpClicked = false;
            return;
        }

        if (!this.checkInput()) {
            this.isSignUpClicked = false;
            return;
        }

        //Check if there is a wallet created with the email
        let isExist = await this.wallet.checkWalletExist(this.inputDat.email);
        if (isExist) {
            this.errorMess = 'This email is registered.';
            this.isSignUpClicked = false;
            return;
        }

        //Validated, temporarily store info
        this.wallet.setInfo(this.inputDat.email, this.inputDat.inputPass, 0);

        //Authentify email
        try {
            await this.authEmail();
        }
        catch (err) {
            // await this.alertRegisteredEmail();
            this.isSignUpClicked = false;
            return;
        }

        //Clear password
        this.inputDat.inputPass = '';
        this.inputDat.verifPass = '';

        //Navigate to OTP verify page
        this.goCodeVerify();
    }

    /**
     * Send verify email
     */
    async authEmail() {
        let email = this.inputDat.email;
        let password = this.inputDat.inputPass;
        try {
            await this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
            await this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
        }
        catch (err) {
            throw new Error('The email address is already in use by another account.');
        }
        this.firebaseAuth.auth.currentUser.sendEmailVerification()
            .then(res => console.log(res))
            .catch(err => console.log(err));
    }

    async alertRegisteredEmail() {
        const alert = await this.alertCtrl.create({
            header: 'New account with old email?',
            message: 'This email is registered. If you have a SiriusSign backup file your account, please choose recovery option. If you want to create new account with this email, click OK, but we dont recommend this.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: () => {
                        this.authOldEmail();
                    }
                }
            ]
        });

        await alert.present();
    }

    authOldEmail() {
        const actionCodeSettings = {
            // URL you want to redirect back to. The domain (www.example.com) for
            // this URL must be whitelisted in the Firebase Console.
            url: 'http://localhost',
            // This must be true for email link sign-in.
            handleCodeInApp: true,
            iOS: {
                bundleId: 'com.fds.test'
            },
            android: {
                packageName: 'com.fds.test',
                installApp: true,
                minimumVersion: '12'
            },
            // FDL custom domain.
            dynamicLinkDomain: 'siriussign.page.link'
        };

        this.firebaseAuth.auth.sendSignInLinkToEmail(this.inputDat.email, actionCodeSettings)
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                console.log(err);
            })
    }
}
