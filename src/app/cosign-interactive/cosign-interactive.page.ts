import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
import * as pdfjsLib from 'pdfjs-dist';
import * as pdfWorker from 'pdfjs-dist/build/pdf.worker';
import * as PDF from 'pdf-lib';

import { SignatureCanvas, NamePosition } from '../model/signature-canvas.model';

import { HelperService } from '../services/helper.service';
import { DrawSignatureService, SignatureInfo } from '../services/draw-signature.service';
import { SignatureService } from './../services/signature.service';
import { GlobalService } from './../services/global.service';

@Component({
    selector: 'app-cosign-interactive',
    templateUrl: './cosign-interactive.page.html',
    styleUrls: ['./cosign-interactive.page.scss'],
})
export class CosignInteractivePage {
    signatureImgObj: HTMLImageElement = document.createElement('img');

    imgSrc: string = '/assets/sign-here-xl.png';
    imgObj: HTMLImageElement = document.createElement('img');

    inactiveImgSrc: string = '/assets/sign-here-inactive-xl.png';
    inactiveImgObj: HTMLImageElement = document.createElement('img');

    pdfDoc = null;
    docUint8Array: Uint8Array;
    pageNum: number = 1;
    pageRendering: boolean = false;
    pageNumPending: number = null;
    scale: number = 1;
    pdfCanvas: HTMLCanvasElement;
    pdfCtx: CanvasRenderingContext2D;

    signaturesInfo: SignatureInfo[];
    signCanvases: SignatureCanvas[] = [];
    selectedSignatureIndex: number = 1;

    isInit: boolean = false;
    isCosigned: boolean = false;

    constructor(
        private router: Router,
        private toastController: ToastController,
        private drawSignature: DrawSignatureService,
        private signature: SignatureService,
        private global: GlobalService
    ) {
        // The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = pdfWorker;
    }

    ngOnDestroy() {
    }

    async ionViewWillEnter() {
        await this.initView();
    }

    async initView() {
        document.documentElement.style.setProperty('--width-container', '595px');
        this.signaturesInfo = this.drawSignature.signers;
        this.selectedSignatureIndex = this.drawSignature.selectedSignerIndex;
        this.isInit = true;
        if (!this.signaturesInfo.map(signatureInfo => signatureInfo.publicKey).includes(this.global.loggedAccount.publicKey))
            this.isCosigned = true;

        this.pdfCanvas = <HTMLCanvasElement>document.getElementById('the-pdf');
        this.pdfCtx = this.pdfCanvas.getContext('2d');

        // Device Pixel Ratio
        const dpr = window.devicePixelRatio;

        // User signature image
        await this.signature.fetchFromStorage();
        const pngSignatureImg = await HelperService.getPngSignature(this.signature.signatureImg);
        this.signatureImgObj.src = pngSignatureImg;
        this.signatureImgObj.height = 88;
        this.signatureImgObj.width = 188;

        // Active image
        this.imgObj.src = this.imgSrc;
        this.imgObj.height = 88;
        this.imgObj.width = 188;

        // Inactive image
        this.inactiveImgObj.src = this.inactiveImgSrc;
        this.inactiveImgObj.height = 88;
        this.inactiveImgObj.width = 188;

        // Set pdf
        this.docUint8Array = HelperService.convertDataURIToBinary(this.drawSignature.pdfUri);
        this.pdfDoc = await pdfjsLib.getDocument(this.docUint8Array).promise;

        document.getElementById('page_count').innerHTML = this.pdfDoc.numPages;
        document.getElementById('prev').addEventListener('click', () => this.onPrevPage());
        document.getElementById('next').addEventListener('click', () => this.onNextPage());

        // Initial/first page rendering
        await this.renderPage(this.pageNum);

        // Generate signature canvases
        this.signaturesInfo.forEach((signatureInfo, index) => {
            this.signCanvases[index] = new SignatureCanvas(signatureInfo.publicKey, signatureInfo.signaturePosition.name);
        });

        const signatureImgPublicKeys = this.drawSignature.signatureImgs.map(imgInfo => imgInfo.publicKey);
        const createCanvases = async () => HelperService.asyncForEach(this.signCanvases, async (canvas, index) => {
            canvas.canvasSize.width = this.pdfCanvas.width;
            canvas.canvasSize.height = this.pdfCanvas.height;

            // Check and get signed signature image
            const indexOfImg = signatureImgPublicKeys.indexOf(canvas.id);
            const signatureImgSrc = (indexOfImg > -1) ? await HelperService.getPngSignature(this.drawSignature.signatureImgs[indexOfImg].signatureImg) :
                this.imgSrc;
            const otherSignatureImgObj: HTMLImageElement = document.createElement('img');
            otherSignatureImgObj.src = signatureImgSrc;
            otherSignatureImgObj.height = 88;
            otherSignatureImgObj.width = 188;
            // Setup canvas
            canvas.create('the-signature-' + index, otherSignatureImgObj, this.scale * dpr);
            canvas.pageNumber = this.drawSignature.signers[index].signaturePosition.page;
            canvas.x = this.drawSignature.signers[index].signaturePosition.x;
            canvas.y = this.drawSignature.signers[index].signaturePosition.y;
            canvas.namePosition = this.drawSignature.signers[index].signaturePosition.namePosition;
            canvas.isNamePlace = canvas.namePosition != NamePosition.NONE;
            canvas.isInit = true;

            if (!(this.global.isBrowser) || (dpr != 1)) {
                const canvasElement = document.getElementById('the-signature-' + index);
                canvasElement.style.width = '100%';
            }
        });
        await createCanvases();

        this.signCanvases.forEach((canvas, index) => {
            // Draw signature image
            canvas.drawInactive(this.pageNum);

            // Set z-index for canvas
            const canvasElement = document.getElementById('the-signature-' + index);
            canvasElement.style.zIndex = (index == this.selectedSignatureIndex) ? '2' : '1';
        });

        this.signCanvases[this.selectedSignatureIndex].onSignHere(() => {
            this.signCanvases[this.selectedSignatureIndex].image = this.signatureImgObj;
            this.signCanvases[this.selectedSignatureIndex].drawInactiveWithAnimation(this.pageNum);
            this.isCosigned = true;
        })

        // document.getElementById('download').addEventListener('click', () => { this.run(); });

        // document.getElementById('apply').addEventListener('click', () => {
        //     const heightSign = <HTMLInputElement>document.getElementById('heightSign');
        //     const widthSign = <HTMLInputElement>document.getElementById('widthSign');
        //     this.imgObj.height = parseInt(heightSign.value);
        //     this.imgObj.width = parseInt(widthSign.value);
        //     this.signCanvases[this.selectedSignatureIndex].drawSignature();
        // });

        this.drawSignature.isInit = true;
    }

    /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param num Page number.
    */
    async renderPage(num) {
        this.pageRendering = true;
        // Using promise to fetch the page
        const page = await this.pdfDoc.getPage(num)

        // const pdfScreenRatio = window.innerWidth / page.view[2];
        // this.scale = pdfScreenRatio > 1 ? 1 : pdfScreenRatio;
        if (this.scale == 1) document.documentElement.style.setProperty('--width-container', page.view[2] + 'px');

        // Scale up for crystal clear image
        const dpr = window.devicePixelRatio;
        var viewport = page.getViewport({ scale: this.scale * dpr });
        this.pdfCanvas.height = viewport.height;
        this.pdfCanvas.width = viewport.width;

        // Scale down for right scale ratio
        const canvas = document.getElementById(this.pdfCanvas.id);
        if ((!this.global.isBrowser) || (dpr != 1))
            canvas.style.width = '100%';

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: this.pdfCtx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(() => {
            this.pageRendering = false;
            if (this.pageNumPending !== null) {
                // New page rendering is pending
                this.renderPage(this.pageNumPending);
                this.pageNumPending = null;
            }
        });


        // Update page counters
        document.getElementById('page_num').textContent = num;

        // Update signature
        this.signCanvases.forEach(canvas => canvas.drawInactive(this.pageNum));
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    queueRenderPage(num) {
        if (this.pageRendering) {
            this.pageNumPending = num;
        } else {
            this.renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    onPrevPage() {
        if (this.pageNum <= 1) {
            return;
        }
        this.pageNum--;
        this.queueRenderPage(this.pageNum);
    }


    /**
     * Displays next page.
     */
    onNextPage() {
        console.log(this.pdfDoc)
        if (this.pageNum >= this.pdfDoc.numPages) {
            return;
        }
        this.pageNum++;
        this.queueRenderPage(this.pageNum);
    }

    async run() {
        // Load
        const canvasImageBuffer = HelperService.convertDataURIToBinary(this.signCanvases[this.selectedSignatureIndex].canvas.toDataURL('image/svg'));
        const imageBuffer = await fetch(this.imgSrc)
            .then(res => res.arrayBuffer())
            .catch(err => {
                console.log(err);
                return null;
            });
        const signatureImgBuffer = HelperService.convertDataURIToBinary(this.signatureImgObj.src);
        const pdfDoc = await PDF.PDFDocument.load(this.docUint8Array);
        const canvasPngImage = await pdfDoc.embedPng(canvasImageBuffer);
        const pngImage = await pdfDoc.embedPng(imageBuffer);
        const signaturePngImg = await pdfDoc.embedPng(signatureImgBuffer);


        const pngDims = pngImage.size
        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(PDF.StandardFonts.Helvetica);


        const pages = pdfDoc.getPages();
        const firstPage = pages[this.pageNum - 1];
        const { width, height } = firstPage.getSize();
        firstPage.setFont(helveticaFont);
        firstPage.setFontSize(14);  //px

        // Draw
        this.signCanvases.forEach((signCanvas, index) => {
            const position = signCanvas.getPosition();
            const embedImg = (index == 0) ? signaturePngImg : pngImage;
            firstPage.drawImage(embedImg, {
                x: position.x,
                y: height - position.y - this.imgObj.height,
                height: this.imgObj.height,
                width: this.imgObj.width
            });

            // firstPage.drawImage(canvasPngImage, {
            //     x: 0,
            //     y: 0,
            //     height: this.pdfCanvas.height,
            //     width: this.pdfCanvas.width
            // });

            if (signCanvas.isNamePlace) {
                const namePosition = signCanvas.namePosition;
                const nameRect = signCanvas.computeNameRect(namePosition);

                const isNameCenter = (signCanvas.namePosition == NamePosition.TOP) || (signCanvas.namePosition == NamePosition.BOTTOM);
                let nameX = nameRect.topLeftX;
                const nameY = this.pdfCanvas.height - nameRect.bottomRightY + 16;

                if (isNameCenter) {
                    this.pdfCtx.font = "14px Helvetica";
                    const textMetric = this.pdfCtx.measureText(signCanvas.name);
                    const centerNameX = (nameRect.topLeftX + nameRect.bottomRightX) / 2 - textMetric.width / 2;
                    nameX = centerNameX;
                }

                firstPage.drawText(signCanvas.name, {
                    x: nameX,
                    y: nameY
                })
            }
        })

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();


        var blob = new Blob([pdfBytes], { type: "application/pdf" });
        var link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        link.download = "output.pdf";
        link.click();


        // fs.writeFile('output.pdf', pdfBytes, (err)=>console.error(err))

    }

    /**
     * Alert by toast
     */
    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Check if network is available
     */
    async checkNetwork() {
        if (!this.global.isOnline) {
            this.presentToast('No Internet access!');
            return false;
        }

        await this.global.checkNode(this.global.apiNode);

        if (!this.global.isNodeWorking) {
            this.presentToast('API Node Failure..')
            return false;
        }

        return true;
    }

    /**
     * Put signature done, launch cosign process
     */
    async onFinish() {
        const isAccessible = await this.checkNetwork();
        if (!isAccessible) return;

        if (this.isCosigned) {
            this.global.setIsProgressDone(false);
            this.router.navigate(['cosign-progress']);
        }
        else {
            this.presentToast('Please put your signature');
        }
    }
}
