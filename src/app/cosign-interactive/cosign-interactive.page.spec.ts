import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CosignInteractivePage } from './cosign-interactive.page';

describe('CosignInteractivePage', () => {
    let component: CosignInteractivePage;
    let fixture: ComponentFixture<CosignInteractivePage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CosignInteractivePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CosignInteractivePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
