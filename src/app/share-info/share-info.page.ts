import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { QRCode, ErrorCorrectLevel, QR8BitByte } from 'qrcode-generator-ts/js'

import { GlobalService } from './../services/global.service';
import { SignatureService } from './../services/signature.service';

@Component({
    selector: 'app-share-info',
    templateUrl: './share-info.page.html',
    styleUrls: ['./share-info.page.scss'],
})
export class ShareInfoPage implements OnInit {

    name = '';
    publicKey = '';
    qrCode = '';
    savedPath = '';

    constructor(
        private router: Router,
        private global: GlobalService,
        private signature: SignatureService
    ) { }

    ngOnInit() {
        this.name = this.signature.name;
        this.publicKey = this.global.loggedAccount.publicKey;
        this.qrCode = this.getQR();
    }

    getQR() {
        const info = {
            name: this.signature.name,
            publicKey: this.global.loggedAccount.publicKey
        }
        const infoString = JSON.stringify(info);
        let qr = new QRCode();
        qr.setTypeNumber(5);
        qr.setErrorCorrectLevel(ErrorCorrectLevel.L);
        qr.addData(new QR8BitByte(infoString));
        qr.make();
        return qr.toDataURL(5);
    }
}
