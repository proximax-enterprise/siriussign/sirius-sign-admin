import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { GlobalService } from './../services/global.service';
import { BuyCryptoService } from './../services/buy-crypto.service';
import { LoadingProcessService } from '../services/loading-process.service';
import { WalletService } from './../services/wallet.service';

@Component({
    selector: 'app-card-info',
    templateUrl: './card-info.page.html',
    styleUrls: ['./card-info.page.scss'],
})
export class CardInfoPage {
    // @ViewChild('emailInput', null) emailInput: IonInput;

    // @ViewChild('ionContent', null) content: IonContent;
    isDone: boolean = true;
    email: string = '';
    name: any;
    cardNumber: any;
    cvc: any;
    usdTotal: any;
    constructor(
        private router: Router,
        private alertController: AlertController,
        private global: GlobalService,
        private buyCrypto: BuyCryptoService,
        private loadingProcess: LoadingProcessService,
        private wallet: WalletService
    ) {

    }

    async ionViewWillEnter() {
        console.log('initHere');
        this.usdTotal = this.buyCrypto.usdAmount / 100;
        console.log(this.usdTotal);
        await this.buyCrypto.createStripe().catch(err => {
            this.alertMessage('Server Error', err.message);
        });
    }

    /**
     * Present Alert
     * @param message 
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }

    async buyProcess() {
        this.loadingProcess.present();
        this.buyCrypto.setEmail(this.email);
        this.buyCrypto.setCardNumber(this.cardNumber);
        this.buyCrypto.setCVC(this.cvc);
        this.buyCrypto.setNameOfCard(this.name);
        console.log(this.buyCrypto.cardInfo);

        const result = await this.buyCrypto.payWithStripe(this.global.loggedAccount.publicKey).catch(async () => {
            setTimeout(() => {
                this.loadingProcess.dismiss();
            }, 1000);
            this.router.navigateByUrl('transaction-fail');
        });
        if (result.error && result.error != '') {
            setTimeout(() => {
                this.loadingProcess.dismiss();
            }, 1000);
            this.router.navigateByUrl('transaction-fail');
        }
        else {
            console.log(this.buyCrypto.tokenID);
            this.global.loggedWallet.plan = 2;
            this.wallet.changePlan(this.global.loggedWallet.name, this.global.loggedWallet.plan);
            await this.loadingProcess.dismiss();
            this.router.navigateByUrl('transaction-success');
        }
    }

    test() {
        this.buyCrypto.payWithStripe(this.global.loggedAccount.publicKey);
    }

    getInfo() {
        this.buyCrypto.setEmail(this.email);
    }
}
