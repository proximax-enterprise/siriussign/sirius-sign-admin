import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { OrganizationsService } from '../services/organizations.service';
import { MonitorService } from './../services/monitor.service';
import { Transaction, Account, NetworkType } from 'tsjs-xpx-chain-sdk';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'app-new-organization',
  templateUrl: './new-organization.page.html',
  styleUrls: ['./new-organization.page.scss'],
})
export class NewOrganizationPage implements OnInit {
  loggedAcc: any;
  name: string;
  a: any;
  isCreating: boolean = false;
  constructor(
    private router: Router,
    private global: GlobalService,
    private organizationsService: OrganizationsService,
    private monitor: MonitorService,
    private signingWithoutMultisig: SigningWithoutMultisigService,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }
  navToHome() {
    this.router.navigateByUrl('/home');
  }
  navToTest() {
    this.router.navigateByUrl('/test');
  }
  navToCreate() {
    this.router.navigateByUrl('/create-organizations');
    this.organizationsService.isSignatories = false;
    this.organizationsService.isDocument = false;
    this.organizationsService.isNotify = false;
  }
  navToNew() {
    this.router.navigateByUrl('/new-organization');
  }
  navToAmin() {
    this.router.navigateByUrl('/admin');
  }
  async createOrganization() {
    const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    this.organizationsService.setNamspaceName(this.name);
    console.log(this.organizationsService.namespaceName);
    // console.log(this.global.loggedAccount.privateKey);
    const signedTransaction =  this.organizationsService.createNamespace(privateKey);
    console.log(signedTransaction);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
    // this.global.loggedAccount.address,
    account.address,
    signedTransaction.hash,
    () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
    .then(() => {
      this.organizationsService.isSignatories = true;
      this.router.navigateByUrl('/create-organizations');
    })
    .catch(err => {
        console.log(err);
        return null;
    });
    console.log('done');
  }
  async linkAcctoNamespace() {
    const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    this.organizationsService.setNamspaceName(this.name);
    console.log(this.organizationsService.namespaceName);
    console.log(this.global.loggedAccount.privateKey);
    const signedTransaction =  this.organizationsService.linkAccountToNamespace(privateKey);
    console.log(signedTransaction);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
    this.global.loggedAccount.address,
    signedTransaction.hash,
    () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
    .catch(err => {
        console.log(err);
        return null;
    });
  }
  async createNamespaceWithStorage() {
    const check = /^[a-za-z0-9-_]*$/gm.test(this.name);
    if (!check || this.name == '' ){alert('Wrong format!!! Allowed characters are a, b, c, …, z, 0, 1, 2, …, 9, _ , -.');
    } else {
      this.organizationsService.isCreatingOr = !this.organizationsService.isCreatingOr;
      this.organizationsService.setNamspaceName(this.name);
      console.log(this.organizationsService.namespaceName);
      this.router.navigateByUrl('/namespcae-progress');
    }
  }
}
