import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

import { HelperService } from '../services/helper.service';
import { SignDocumentService } from './../services/sign-document.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

@Component({
    selector: 'app-new-file-view',
    templateUrl: './new-file-view.page.html',
    styleUrls: ['./new-file-view.page.scss'],
})
export class NewFileViewPage implements OnInit {

    doc = {
        name: this.signDoc.document.file.name,
        signDate: this.signDoc.document.signDate
    }

    uri = this.signDoc.document.file.dataURI;
    data = this.signDoc.document.file.data.toString();


    constructor(
        private router: Router,
        private fileOpener: FileOpener,
        private filePath: FilePath,
        private signDoc: SignDocumentService,
        private platform: Platform
    ) { console.log(this.signDoc.document.file.data); }

    ngOnInit() { }

    ionViewWillEnter() {
        this.doc = {
            name: this.signDoc.document.file.name,
            signDate: this.signDoc.document.signDate
        }
        console.log(this.doc.name);
    }

    dateToShortString = HelperService.dateToShortString;

    /**
     * Navigate to file info page
     */
    goSign() {
        // this.router.navigate(['new-only-me']);
        this.router.navigate(['new-only-me']);
    }

    /**
     * Call a program to view file
     */
    onView() {
        if (this.platform.is('android')) {
            this.filePath.resolveNativePath(this.signDoc.document.file.uri)
                .then(filePath => {
                    console.log(filePath);
                    this.fileOpener.open(filePath, 'application/pdf')
                        .then(() => console.log('File is opened'))
                        .catch(e => console.log('Error opening file', e));
                })
                .catch(err => console.log(err));
        }

        if (this.platform.is('ios')) {
            this.fileOpener.open(this.signDoc.document.file.uri, 'application/pdf')
                .then(() => console.log('File is opened'))
                .catch(e => console.log('Error opening file', e));
        }

        if (this.platform.is('desktop')) {
            let pdfWindow = window.open('');
            pdfWindow.document.write("<iframe width='100%' height='100%' src='" + encodeURI(this.signDoc.document.file.dataURI) + "'></iframe>")
            pdfWindow.document.close();
        }
    }

}
