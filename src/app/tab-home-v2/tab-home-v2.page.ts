import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Transaction, TransactionType, AggregateTransaction, TransferTransaction } from 'tsjs-xpx-chain-sdk';

import { SiriusSignDocument } from '../model/siriussign-document.model';
import { SiriusMessageType } from '../model/siriussign-message.model';

import { HelperService } from '../services/helper.service';
import { GlobalService } from './../services/global.service';
import { MonitorService } from './../services/monitor.service';
import { DocumentClassificationService, DocumentSigningStatus } from './../services/document-classification.service';
import { DocumentStorageService } from './../services/document-strorage.service';
import { BuyCryptoService } from './../services/buy-crypto.service';
import { WalletService } from './../services/wallet.service';

@Component({
    selector: 'app-tab-home-v2',
    templateUrl: './tab-home-v2.page.html',
    styleUrls: ['./tab-home-v2.page.scss'],
})
export class TabHomeV2Page implements OnInit {

    initConnection: boolean = true;
    isLoading: boolean = false;
    owner: string;
    isShowAlert: boolean = true;

    flag = {
        COMPLETED: DocumentSigningStatus.COMPLETED,
        WAITING: DocumentSigningStatus.WAITING,
        NEEDSIGN: DocumentSigningStatus.NEEDSIGN
    }

    isPurchasing: boolean = false;

    constructor(
        private router: Router,
        private global: GlobalService,
        private monitor: MonitorService,
        public documentClassification: DocumentClassificationService,
        private documentStorage: DocumentStorageService,
        private buyCrypto: BuyCryptoService,
        private wallet: WalletService
    ) {
        this.owner = this.global.loggedAccount.publicKey;
    }

    ngOnInit() {
        this.documentStorage.setAccount();
        this.checkPlan();
        const initConnectionSub = this.global.observableIsOnline.subscribe(isOnline => {
            this.initConnection = isOnline;
            if (this.initConnection) {
                this.loadApp();
                if (initConnectionSub) initConnectionSub.unsubscribe();
            }
        });
    }

    ionViewWillEnter() {
        this.checkPlan();
    }

    dateToShortString = HelperService.dateToShortString;

    nowFromDate = HelperService.nowFromDate;

    /**
     * Check if account is free plan or paid plan
     */
    checkPlan() {
        switch (this.global.loggedWallet.plan) {
            case 0: {
                this.isShowAlert = true;
                break;
            }
            case 1: {
                this.isShowAlert = false;
                break;
            }
            case 2: {
                this.isShowAlert = false;
                break;
            }
            default: {
                this.isShowAlert = true;
            }
        }
    }

    /**
     * App starting
     */
    async loadApp() {
        this.isLoading = true;
        console.log(this.documentClassification.completed);
        await this.fetchFromStorage();
        console.log(this.documentClassification.completed);
        this.isLoading = false;
        this.fetchAndUpdateNeedSignAndNeedVerifyFromChain();

        if (this.documentClassification.universalId != '') {
            this.documentClassification.selectedDocInfo = this.documentClassification
                .getDocByDocumentAccount(this.documentClassification.universalId, DocumentSigningStatus.ANY);
            this.router.navigate(['sign-info']);
        }

        await this.monitor.openListener();

        setInterval(async () => {
            if (!this.monitor.checkListener()) {
                await this.monitor.openListener();
                this.monitor.listenConfirmed(this.global.loggedAccount.address.plain(), null, (tx: Transaction) => {
                    if (tx.type == TransactionType.AGGREGATE_COMPLETE) {
                        const aggTx = <AggregateTransaction>tx;
                        const checkTx = aggTx.innerTransactions[0];
                        if (checkTx.type == TransactionType.TRANSFER) {
                            const info = HelperService.parseSsMessage((<TransferTransaction>checkTx).message.payload);
                            if (info && (info.header.appCode == this.global.appCodeName) &&
                                (info.header.messageType == SiriusMessageType.SIGN_NOTIFY) || (info.header.messageType == SiriusMessageType.VERYFY_NOTIFY))
                                this.fetchAndUpdateNeedSignAndNeedVerifyFromChain();
                        }
                    }
                });
            }
        }, 60 * 1000);
    }

    /**
     * View document info
     * @param index
     */
    onDocument(index: number, flag: DocumentSigningStatus) {
        this.documentClassification.selectedDocInfo = this.documentClassification.getDocByIndex(index, flag);
        this.router.navigate(['sign-info']);
    }

    /**
     * Reload home and history
     * @async
     */
    async fetchFromChain() {
        // this.documentClassification.needSign = [];
        this.fetchAndUpdateNeedSignAndNeedVerifyFromChain();
        this.documentClassification.sortDocs('needSign');
        await this.documentClassification.fetchCompletedAndWaiting();
        this.documentClassification.sortDocs('compledAndWaiting');
        let storedDocs = await this.documentStorage.fetchDocument();
        const storedDocAccounts = storedDocs.map(doc => doc.documentAccount.publicKey);
        const allDocs = [
            ...this.documentClassification.completed,
            ...this.documentClassification.waiting,
            ...this.documentClassification.verified,
            ...this.documentClassification.verifying
        ];
        const updateDocStorage = async () => HelperService.asyncForEach(allDocs, async (doc: SiriusSignDocument) => {
            if (!storedDocAccounts.includes(doc.documentAccount.publicKey)) {
                await this.documentStorage.storeDocument(doc);
                return;
            }
            const focusedDoc = storedDocs[storedDocAccounts.indexOf(doc.documentAccount.publicKey)];
            if (focusedDoc.cosignatures.length != doc.cosignatures.length) {
                await this.documentStorage.updateDocument(focusedDoc.id, oldDoc => doc);
            }
        });
        await updateDocStorage();
    }

    /**
     * Fetch from storage
     */
    async fetchFromStorage() {
        this.documentClassification.clearDocs();
        const docs = await this.documentStorage.fetchDocument();
        console.log(docs);
        console.log(this.documentClassification.completed);
        console.log(this.documentClassification.needSign)
        if (docs) docs.forEach((doc, i) => this.documentClassification.classify(doc));
        console.log(this.documentClassification.completed);
        this.documentClassification.sortDocs('all');
        console.log(this.documentClassification.completed);
    }

    /**
     * Fetch need-sign document from chain to update local
     */
    async fetchAndUpdateNeedSignAndNeedVerifyFromChain() {
        const [newNeedSign, newNeedVerify] = await this.documentClassification.fetchNeedSignDocs();

        const oldNeedSign = this.documentClassification.needSign;
        const storeNeedSign = async () => HelperService.asyncForEach(newNeedSign, async (newDoc: SiriusSignDocument) => {
            const isStored = oldNeedSign.map(oldDoc => oldDoc.documentAccount.publicKey).includes(newDoc.documentAccount.publicKey);
            if (!isStored) await this.documentStorage.storeDocument(newDoc);
        });
        const updateNeedSign = async () => HelperService.asyncForEach(oldNeedSign, async (oldDoc: SiriusSignDocument) => {
            const isUpdated = !newNeedSign.map(newDoc => newDoc.documentAccount.publicKey).includes(oldDoc.documentAccount.publicKey);
            if (isUpdated) await this.documentStorage.removeDocument(oldDoc.id);
        });

        const oldNeedVerify = this.documentClassification.needVerify;
        const storeNeedVerify = async () => HelperService.asyncForEach(newNeedVerify, async (newDoc: SiriusSignDocument) => {
            const isStored = oldNeedVerify.map(oldDoc => oldDoc.documentAccount.publicKey).includes(newDoc.documentAccount.publicKey);
            if (!isStored) await this.documentStorage.storeDocument(newDoc);
        });
        const updateNeedVerify = async () => HelperService.asyncForEach(oldNeedVerify, async (oldDoc: SiriusSignDocument) => {
            const isUpdated = !newNeedVerify.map(newDoc => newDoc.documentAccount.publicKey).includes(oldDoc.documentAccount.publicKey);
            if (isUpdated) await this.documentStorage.removeDocument(oldDoc.id);
        });

        await updateNeedSign();
        await storeNeedSign();
        await storeNeedVerify();
        await updateNeedVerify();
        this.documentClassification.needSign = newNeedSign;
        this.documentClassification.needVerify = newNeedVerify;
    }

    /**
     * Refresher
     * @param event 
     */
    async doRefresh(event) {
        this.documentClassification.clearDocs();
        await this.fetchFromStorage();
        await this.fetchFromChain();
        event.target.complete();
    }

    navigate() {
        this.router.navigate(['upgrade-choose']);
    }

    async onChangeShowAlert() {
        this.isPurchasing = true;
        await this.buyCrypto.tryFree();
        this.global.loggedWallet.plan = 1;
        this.wallet.changePlan(this.global.loggedWallet.name, this.global.loggedWallet.plan);
        this.isPurchasing = false;
        this.isShowAlert = false;
    }
}