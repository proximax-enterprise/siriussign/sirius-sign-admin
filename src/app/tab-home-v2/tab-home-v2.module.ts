import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabHomeV2Page } from './tab-home-v2.page';

const routes: Routes = [
    {
        path: '',
        component: TabHomeV2Page
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [TabHomeV2Page]
})
export class TabHomeV2PageModule { }
