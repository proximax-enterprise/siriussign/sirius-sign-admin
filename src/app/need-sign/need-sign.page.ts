import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HelperService } from '../services/helper.service';
import { GlobalService } from './../services/global.service';
import { DocumentClassificationService, DocumentSigningStatus } from '../services/document-classification.service';

@Component({
    selector: 'app-need-sign',
    templateUrl: './need-sign.page.html',
    styleUrls: ['./need-sign.page.scss'],
})

export class NeedSignPage implements OnInit {
    ownerPublicKey: string;

    constructor(
        private router: Router,
        private global: GlobalService,
        public documentClassification: DocumentClassificationService
    ) {
        this.ownerPublicKey = this.global.loggedAccount.publicKey;
    }

    ngOnInit() {
    }

    dateToShortString = HelperService.dateToShortString;

    /**
     * View document info
     */
    onDocument(index: number) {
        this.documentClassification.selectedDocInfo = this.documentClassification.getDocByIndex(index, DocumentSigningStatus.NEEDSIGN);
        this.router.navigate(['sign-info']);
    }
}
