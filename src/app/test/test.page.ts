import { Component, OnInit } from '@angular/core';
import { 
  Password,
  TransferTransaction,
  Deadline,
  NetworkType,
  PlainMessage,
  UInt64,
  QueryParams,
  AddressAliasTransaction,
  TransactionType,
  RegisterNamespaceTransaction,
  AccountHttp,
  NamespaceHttp,
  NamespaceId, 
  MosaicId, MosaicNonce, MosaicDefinitionTransaction, MosaicProperties, MosaicSupplyChangeTransaction, MosaicSupplyType, MosaicHttp,
  AliasActionType,
  AggregateTransaction,
  Order,
  Mosaic,
  SimpleWallet, Listener, Address, Account, PublicAccount, Transaction, TransactionHttp } from 'tsjs-xpx-chain-sdk';
import { MonitorService } from './../services/monitor.service';
import { Observable } from 'rxjs';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';
import { SignDocumentService } from '../services/sign-document.service';
import { UploadResult } from 'tsjs-chain-xipfs-sdk';
// import { UploadService } from './../service/upload.service';
import { SignDocument } from '../services/sign-document.service';
import { BackupService, BackupInfo } from './../services/backup.service';
import { GlobalService } from '../services/global.service';
// import { SiriusSignSigningTask } from '../models/doc-manage.model';
// import { SignDocService } from '../service/sign-doc.service';
import { Router } from '@angular/router';
import { OrganizationsService } from '../services/organizations.service';
import { Storage } from '@ionic/storage';
import { DocumentStorageService } from './../services/document-strorage.service';


@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage implements OnInit {
  testName: string;
  accountt: Account;
  document: SignDocument = {
    file: {
      data: new Uint8Array(0),
      dataURI: '',
      mediaType: '',
      name: '',
      uri: ''
    },
    size: 0,
    signDate: new Date(),
    cosigners: [],
    verifiers: [],
    multisigAcc: null,
    documentAcc: null,
    fileHash: '',
    signTxHash: '',
    uploadTxHash: '',
    isEncrypt: false,
    signatureUploadTxHash: '',
    status: ''
  };
  backupInfo: BackupInfo;
  NETWORK_TYPE = NetworkType.PRIVATE_TEST;
  maxFee = UInt64.fromUint(0);
  generationHash = '031248ED76E99807C198D2A219EEBBE678D78B036D92BA505988994356DF61B8';

  constructor(
    private monitor: MonitorService,
    private signingWithoutMultisig: SigningWithoutMultisigService,
    private signDoc: SignDocumentService,
    // private uploadStorage: UploadService,
    private global: GlobalService,
    // private signDocc: SignDocService,
    private router: Router,
    private or: OrganizationsService,
    private storage: Storage,
    private docStore: DocumentStorageService

  ) {


  }

  ngOnInit() {    
  }
  onToTabs() {
    console.log('adfaf');
    
    this.router.navigateByUrl('tabtest');
  }
  navToHome() {
    this.router.navigateByUrl('/home');
  }
  navToTest() {
    this.router.navigateByUrl('/test');
  }
  test() {
    console.log(this.global.loggedAccount);
    console.log(this.global.loggedWallet);
    const NETWORK_TYPE = NetworkType.TEST_NET;
    const maxFee = UInt64.fromUint(0)
    const transactionHttp = new TransactionHttp('https://bctestnet1.brimstone.xpxsirius.io');
    const listener = new Listener('https://bctestnet1.brimstone.xpxsirius.io');
    const sender = Account.createFromPrivateKey('B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433', NETWORK_TYPE); //siriusID
    const recipient = Account.createFromPrivateKey('130E7960434F38FA626B85B51B21917A19F3470802237FAAE6C5313A8F3EF962', NETWORK_TYPE); //dApp
    const tx = TransferTransaction.create(
      Deadline.create(),
      recipient.address,
      [],
      PlainMessage.create("Hello !"),
      NetworkType.TEST_NET,
      maxFee
    );
    console.log('typeeeeeeeeeeee: ' + tx.type + TransactionType.REGISTER_NAMESPACE);
    console.log(tx.message.payload);
    // const generationHash = '3D9507C8038633C0EB2658704A5E7BC983E4327A99AC14D032D67F5AACBCCF6A';
    // const generationHash = '7B631D803F912B00DC0CBED3014BBD17A302BA50B99D233B9C2D9533B842ABDF';
    const generationHash = '56D112C98F7A7E34D1AEDC4BD01BC06CA2276DD546A93E36690B785E82439CA9';
    const signedTx = sender.sign(tx, generationHash);
    console.log(signedTx);
    
    console.log(NetworkType.TEST_NET);
  }
  
  getAccInfo() {
    // B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433
    const accountHttp = new AccountHttp(this.global.apiNode);
    const publicKey = '28281923190B84B8FA3B69AC03984C1BBD2E553F1072F996A8F72F67F926A14D';
    const publicAccount =  PublicAccount.createFromPublicKey(publicKey, NetworkType.TEST_NET);
    console.log();
    

    const pageSize = 10; // Page size between 10 and 100, otherwise 10

    accountHttp
        .getAccountInfo(publicAccount.address)
        .subscribe(transactions => console.log(transactions), err => console.error(err));
  }

  monitorTest() {
    this.monitor.openListener();
    
  }

  testJs() {
    const greetingPoster = new Promise((resolve, reject) => {
      console.log('Inside Promise (proof of being eager)');
      resolve('get');
    });
    console.log('Before calling then on Promise');
    greetingPoster.then(res => console.log(`Greeting from promise: ${res}`))

    // greetingPoster.then(res => console.log(`Greeting from promise: ${res}`));
  }

  testOb() {
    const greetingLady = new Observable(observer => {
      console.log('Inside Observable (proof of being lazy)');
      observer.next('Hello! I am glad to get to know you.');
      observer.complete();
    });
    
    console.log('Before calling subscribe on Observable');
    
    greetingLady.subscribe({
      next: console.log,
      complete: () => console.log('End of conversation with preety lady')
    });
  }
  close() {
    this.monitor.closeListener();
  }



  async account() {
    const sender = Account.createFromPrivateKey('C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAC9', NetworkType.PRIVATE_TEST); //dApp
    const recipient = Account.createFromPrivateKey('6E638FDB80CD04A9070AAB5BA966D77DFB215B145186873392B504B3B04532F2', NetworkType.PRIVATE_TEST);
    // const recipient = Account.generateNewAccount(NetworkType.PRIVATE_TEST);
    const tx = TransferTransaction.create(
      Deadline.create(),
      recipient.address,
      [],
      PlainMessage.create("Hello !"),
      NetworkType.PRIVATE_TEST,
      this.maxFee
    );
    const signedTx = sender.sign(tx, this.generationHash);
    console.log(signedTx);

    // const transactionHttp = new TransactionHttp("https://demo-sc-api-1.ssi.xpxsirius.io");

    // transactionHttp.announce(signedTx);

    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      // recipient.address,
      recipient.address,
      signedTx.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTx); })
      .catch(err => {
          console.log(err);
          return null;
      });
  }


  checknameSpace() {
    const namespaceHttp = new NamespaceHttp('https://demo-sc-api-1.ssi.xpxsirius.io');

    const namespace = new NamespaceId('tri-1');
    namespaceHttp
      .getNamespace(namespace)
      .subscribe(namespace => console.log(namespace), err => console.log(err)
      );
  }

  async namespace() {
    const duration = UInt64.fromUint(100);
    // const recipient = Account.createFromPrivateKey('130E7960434F38FA626B85B51B21917A19F3470802237FAAE6C5313A8F3EF962', this.NETWORK_TYPE); //dApp
    const sender = Account.createFromPrivateKey('C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAC9', this.NETWORK_TYPE); //siriusID
    // AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1
    // 38252D1431EB9B59E7DF02BE111B3BE7E2B714A8F3ED1DE5EF9439BF4AFB6980
    // this.accountt = this.signingWithoutMultisig.createDocumentAccount();
    const namespaceName = "trivu";
    const registerNamespaceTransaction = RegisterNamespaceTransaction.createRootNamespace(
      Deadline.create(),
      namespaceName,
      duration,
      NetworkType.PRIVATE_TEST,
      this.maxFee);
    const signedTransaction = sender.sign(registerNamespaceTransaction, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      sender.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
  }
  
  async createSubnamespace() {
    // replace with root namespace name
    const rootNamespaceName = 'trivu';
    // replace with root subnamespace name
    const subnamespaceName = 'bar';
    // replace with network type
    const sender = Account.createFromPrivateKey('C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAC9', this.NETWORK_TYPE); //siriusID

    const namespaceRegistrationTransaction = RegisterNamespaceTransaction.createSubNamespace(
        Deadline.create(),
        subnamespaceName,
        rootNamespaceName,
        NetworkType.PRIVATE_TEST,
        UInt64.fromUint(0));

    const signedTransaction = sender.sign(namespaceRegistrationTransaction, this.generationHash);

    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      sender.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
  }

  async linkAccountToNamespace() {
    // 130E7960434F38FA626B85B51B21917A19F3470802237FAAE6C5313A8F3EF962
    // AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1
    const namespaceId = new NamespaceId('trivu');

    const privateKey = 'C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAC9';
    const account = Account.createFromPrivateKey(privateKey, NetworkType.PRIVATE_TEST);
    console.log(account.address);

    const accLink = Account.createFromPrivateKey('6E638FDB80CD04A9070AAB5BA966D77DFB215B145186873392B504B3B04532F2', NetworkType.PRIVATE_TEST);
    console.log(accLink.address);

    const networkGenerationHash = this.generationHash;

    const addressAliasTransaction = AddressAliasTransaction.create(
      Deadline.create(),
      AliasActionType.Link,
      namespaceId,
      accLink.address,
      NetworkType.PRIVATE_TEST,
      UInt64.fromUint(0),
    );
    console.log(addressAliasTransaction);

    const signedTransaction = account.sign(addressAliasTransaction, networkGenerationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      account.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
  }

  async transferTxToNamespace() {
    const namespaceId = new NamespaceId('fdstest');
    const sender = Account.createFromPrivateKey('B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433', this.NETWORK_TYPE);

    const transferTransaction = TransferTransaction.create(
      Deadline.create(),
      namespaceId,
      [],
      PlainMessage.create('Test'),
      NetworkType.TEST_NET,
      UInt64.fromUint(0));

    const signedTransaction = sender.sign(transferTransaction, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      sender.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
  }

  async fetchBalance() {
    const accountHttp = new AccountHttp(this.global.apiNode);
    const mosaicId = this.global.mosaicId;
    const mosaicInfo = this.global.mosaicInfo;
    const mosaicName = this.global.mosaicName;
    // 130E7960434F38FA626B85B51B21917A19F3470802237FAAE6C5313A8F3EF962
    // AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1
    const sender = Account.createFromPrivateKey('973736FFED9B5A2D9CCBA15DBB370FBF05F1BA0DA113E1B0AC85E9A27AF5D6F9', this.NETWORK_TYPE); //siriusID

    return new Promise<string>(resolve => {
        accountHttp.getAccountInfo(sender.address)
            .subscribe(accountInfo => {
                const mosaics = accountInfo.mosaics;
                const filteredMosaic = mosaics.filter(mosaic => mosaic.id.equals(mosaicId));
                if (filteredMosaic.length == 0) 
                {
                  resolve('0 ' + mosaicName);
                  console.log('No mosaic');
                }

                else {
                    const amount = filteredMosaic[0].amount;
                    const balance = amount.compact() / Math.pow(10, mosaicInfo.divisibility);
                    resolve(balance + ' ' + mosaicName);
                    console.log(balance + ' ' + mosaicName);

                }
            },
                err => resolve('0 ' + mosaicName));
    });
  }

  async mosaics() {

    const transactionHttp = new TransactionHttp(this.global.apiNode);

    const mosaicDuration = (1 * 365 * 24 * 60 * 4 ); // 1 year - 15 sec per block

    const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);

    const nonce = MosaicNonce.createRandom();
    const mosaicId = MosaicId.createFromNonce(nonce, account.publicAccount);
    console.log(mosaicId);

    const mosaicDefinitionTransaction = MosaicDefinitionTransaction.create(
        Deadline.create(),
        nonce,
        mosaicId,
        MosaicProperties.create({
            supplyMutable: true,
            transferable: true,
            divisibility: 0,
            duration: UInt64.fromUint(mosaicDuration),
        }),
        NetworkType.TEST_NET
    );
    console.log(mosaicDefinitionTransaction);
    
    const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction.create(
      Deadline.create(),
      mosaicId,
      MosaicSupplyType.Increase,
      UInt64.fromUint(100),
      NetworkType.TEST_NET
    );
    console.log(mosaicSupplyChangeTransaction);

    const aggregateTransaction = AggregateTransaction.createComplete(
      Deadline.create(),
      [
          mosaicDefinitionTransaction.toAggregate(account.publicAccount),
          mosaicSupplyChangeTransaction.toAggregate(account.publicAccount)
      ],
      NetworkType.TEST_NET,
      [],
      UInt64.fromUint(100));
    console.log(aggregateTransaction);

    const signedTransaction = account.sign(aggregateTransaction, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      account.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
          console.log(err);
          return null;
      });
  }

  checkMosaic() {
    const mosaicHttp = new MosaicHttp(this.global.apiNode);
    const getMosaicInformation = () => {
      const mosaicId = new MosaicId([
        512193246,
        2146583454
      ]);
      mosaicHttp.getMosaic(mosaicId).subscribe(mosaicInfo => {
          console.log(mosaicInfo.mosaicId.toHex());
          console.log(mosaicInfo);
      }, error => {
          console.error(error);
      }, () => {
          console.log('done.');
      });
    };
    getMosaicInformation();
  }


  getNamespaceMulti() {
    const namespaceHttp = new NamespaceHttp('https://demo-sc-api-1.ssi.xpxsirius.io');
    // 88E0EE3B31B7DFD70C08E3EDFF442110118CB4735064C98A74638BA103FA0EA4
    // AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1
    const sender = Account.createFromPrivateKey('C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAC9', this.NETWORK_TYPE);
    const getReadableNamesForSetOfNamespaces = () => {
      // const address = Address.createFromRawAddress(sender.address);
      namespaceHttp.getNamespacesFromAccount(sender.address).subscribe(namespaceInfos => {
          namespaceHttp.getNamespacesName(namespaceInfos.map(namespaceInfo => namespaceInfo.id))
          .subscribe(namespaceNames => {
              console.log(namespaceNames);
          }, error => {
              console.error(error);
          }, () => {
              console.log('done.');
          });
      }, error => {
          console.error(error);
      }, () => {
          // console.log('done.');
      });
    };
    getReadableNamesForSetOfNamespaces();
  }

  getAddressLinktoNamespace() {
    const namespaceHttp = new NamespaceHttp('https://demo-sc-api-1.ssi.xpxsirius.io');
    const getNamespaceInformation = () => {
      const namespaceId = new NamespaceId('trivu');
      namespaceHttp.getLinkedAddress(namespaceId)
      .subscribe(namespaceInfo => {
          console.log(namespaceInfo.pretty());
          }, error => {
              console.error(error);
          }, () => {
              console.log('done.');
          });
    };
    getNamespaceInformation();
  }
  getConfirmedAllTransactions = () => {
    const accountHttp = new AccountHttp(this.global.apiNode);
    const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    // const publicAccount = PublicAccount.createFromPublicKey(account.pu, NetworkType.TEST_NET);
    return accountHttp.transactions(account.publicAccount, {
        pageSize: 10,
        order: Order.DESC
    }).subscribe(tx => {
        console.log(tx);
    }, error => {
        console.error(error);
    }, () => {
        console.log('done.');
    });
  }

  Tx() {

  }

  async testFc() {
    // const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    // this.or.filterRegisterNamespace(privateKey);

    this.storage.remove(this.or.queryKeyNamespace).then((val) => {
      console.log(val);
    });

    // const a = await this.docStore.fetchDocumentWithNamespace();
    // console.log(a);
    
    // this.or.fetchAccount();
  }

  storePrivateKey() {
    this.storage.set('name', 'Tri VU');
  }

  getPrivatekey() {
    this.storage.get('age').then((val) => {
      console.log('Your age is', val);
    });
  }

  signFlow() {
    // const sender = Account.createFromPrivateKey('B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433', this.NETWORK_TYPE);
    // const sender = Account.createFromPrivateKey('AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1', NetworkType.TEST_NET);
    // this.document.documentAcc = Account.generateNewAccount(this.global.networkType);
    // console.log(this.document.documentAcc);
    // const signedTx = await this.signDocc.createDocSigningTransaction(this.document, true);
    // console.log(signedTx);
    const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    const accLinkToNamespace = Account.generateNewAccount(this.global.networkType);
    console.log(accLinkToNamespace);
    console.log(account);
    this.storage.set('Acc', accLinkToNamespace);
    // this.storage.set('Acc', account);


  }

  async storePriv() {
    // const obj =  {
    //   a: 1,
    //   b: 2
    // };
    // const obj3 =  {
    //   e: 11,
    //   f: 22
    // };
    // const ob: any[] = [obj, obj3];
    // const firstPriv = await this.storage.set('Acc', ob);
    // console.log(firstPriv);

    // const priv: [] = await this.storage.get('Acc');
    // console.log(priv);

    // const newpriv = await this.storage.set('Acc', obj3);
    // console.log(newpriv);

    // const added = [...priv, obj];
    // console.log(added);

    // const addedd = await this.storage.set('Acc', added);
    // console.log(addedd);

    // const accLinkToNamespace = Account.generateNewAccount(this.global.networkType);
    // console.log(accLinkToNamespace.privateKey);


    // const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    // const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);

    // const store = await this.or.storePriv(account);
    // console.log(store);

    // const name: string = 'foo';
    // await this.or.fetchAccLinkNamespace(name);
    // setTimeout(() => {
    //   console.log(this.or.accChoosed);
      
    //   }, 1000);
    // const privateKey = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    // const account = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);
    // console.log(account);
    // this.router.navigateByUrl('/namespcae-progress');
    console.log(this.or.queryKeydoc);
    
    const a: any = await this.or.fetchSubNamespace();
    console.log(a);
    

  }
  async sendXpx() {
    const privateKey = '973736FFED9B5A2D9CCBA15DBB370FBF05F1BA0DA113E1B0AC85E9A27AF5D6F9';
    const sender = Account.createFromPrivateKey(privateKey, NetworkType.TEST_NET);

    const privateKeys = 'AD721207DD8ECC8889F816A901091E4F4FC3AECEFF9DDD6DFB53CEBC3F88E7C1';
    const recipient = Account.createFromPrivateKey(privateKeys, NetworkType.TEST_NET);

    const namespaceId = new NamespaceId('prx.xpx');
    const amount = UInt64.fromUint(1000000);
    const mosaic = new Mosaic(namespaceId, amount);
    const tx = TransferTransaction.create(
        Deadline.create(),
        recipient.address,
        [mosaic],
        PlainMessage.create('Test transaction time'),
        NetworkType.TEST_NET,
        UInt64.fromUint(0),
    );
    const signedTransaction = sender.sign(tx, this.generationHash);
    const monitorTx: Transaction = await this.monitor.waitForTransactionConfirmed(
      sender.address,
      signedTransaction.hash,
      () => { this.signingWithoutMultisig.announceTransaction(signedTransaction); })
      .catch(err => {
        console.log(err);
        return null;
    });
  }

  async testString() {
    // console.log(/^[a-za-z0-9]*$/gm.test(this.testName));
    const priv: string[] = await this.storage.get('priv')
    .catch(err => {
    return null;
    });
    console.log(priv);
    const test = [priv, '123asd'];
    console.log(test);
    const test1 = [...priv, '123asd'];
    console.log(test1);

  }

}

