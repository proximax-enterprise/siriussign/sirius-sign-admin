import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

import { HelperService } from './../services/helper.service';
import { GlobalService } from './../services/global.service';
import { SignDocumentService } from './../services/sign-document.service';
import { DrawSignatureService } from './../services/draw-signature.service';
import { SignatureService } from '../services/signature.service';

@Component({
    selector: 'app-new-only-me',
    templateUrl: './new-only-me.page.html',
    styleUrls: ['./new-only-me.page.scss'],
})
export class NewOnlyMePage {

    file = {
        name: '',
        size: 'Unknown',
        uploadDate: '',
        hashFunc: 'SHA256',
        fileHash: '',
        owner: {
            publicKey: '',
            name: '',
            isRequireSignatureImg: false
        },
        storage: 'Public',
        fee: 'No fee'
    };

    readonly labels = {
        name: 'File name',
        size: 'Size',
        uploadDate: 'Upload date',
        hashFunc: 'Hash function',
        fileHash: 'File Hash',
        owner: 'Your information',
        storage: 'Storage',
        fee: 'Network fee'
    };

    readonly fields = ['name', 'size', 'uploadDate', 'owner', 'fileHash', 'storage', 'fee'];

    isProcessing: boolean = false;

    constructor(
        private toastController: ToastController,
        private router: Router,
        private global: GlobalService,
        private signDoc: SignDocumentService,
        private drawSignature: DrawSignatureService,
        private signature: SignatureService
    ) { }

    ionViewWillEnter() {
        this.isProcessing = false;
        const doc = this.signDoc.getDocmument();
        this.file.name = doc.file.name;
        this.file.uploadDate = HelperService.dateToShortString(doc.signDate);
        this.file.fileHash = doc.fileHash;
        this.file.size = HelperService.sizeToString(this.signDoc.document.size);
        this.file.owner.publicKey = this.global.loggedAccount.publicKey;
        this.file.owner.name = this.signature.name;
        this.drawSignature.reset();

        // Reset signing doc in case of back from interactive
        this.signDoc.document.cosigners = [];
    }

    dateToShortString = HelperService.dateToShortString;

    /*
     * Navogate to new-with-others
     */
    goWithOthers() {
        this.router.navigate(['new-with-others']);
    }

    /*
     * Navigate to progress page
     */
    goProgress() {
        this.global.setIsProgressDone(false);
        this.isProcessing = false;
        this.router.navigate(['new-progress']);
    }

    /*
     * Navigate to interactive page
     */
    goInteractive() {
        this.isProcessing = false;
        this.router.navigate(['new-interactive']);
    }

    /**
     * Alert no Internet access
     */
    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Toggle isRequireSignatureImg
     */
    toggleIsRequireSignatureImg() {
        this.file.owner.isRequireSignatureImg = !this.file.owner.isRequireSignatureImg;
    }

    /**
     * Set upload file mode
     * value = 1 -> Encrypt
     * value = 0 -> Plain
     * @param value 
     */
    setStorage(value: '1' | '0') {
        if (value == '1') {
            this.file.storage = 'Private';
            this.signDoc.document.isEncrypt = true;
        }
        else {
            this.file.storage = 'Public';
            this.signDoc.document.isEncrypt = false;
        }
    }

    /**
     * Launch signing process
     */
    async onSignDocument() {
        this.isProcessing = true;
        if (!this.global.isOnline) {
            this.presentToast('No Internet access!');
            this.isProcessing = false;
            return;
        }

        await this.global.checkNode(this.global.apiNode);

        if (!this.global.isNodeWorking) {
            this.presentToast('API Node Failure..')
            this.isProcessing = false;
            return;
        }

        if (this.file.owner.isRequireSignatureImg && this.file.owner.name == '') {
            this.presentToast('Please input your name')
            this.isProcessing = false;
            return;
        }

        this.drawSignature.pdfUri = this.signDoc.document.file.dataURI;
        if (this.file.owner.isRequireSignatureImg) {
            this.drawSignature.setSigner(this.file.owner.publicKey, this.file.owner.name);
            this.goInteractive();
        }
        else {
            this.goProgress();
        }
    }
}
