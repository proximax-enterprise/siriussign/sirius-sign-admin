import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOnlyMePage } from './new-only-me.page';

xdescribe('NewOnlyMePage', () => {
    let component: NewOnlyMePage;
    let fixture: ComponentFixture<NewOnlyMePage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NewOnlyMePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewOnlyMePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
