import { AlertController, ToastController } from '@ionic/angular';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalService } from '../services/global.service';
import { WalletService } from '../services/wallet.service';
import { BuyCryptoService } from './../services/buy-crypto.service';

@Component({
    selector: 'app-upgrade-choose',
    templateUrl: './upgrade-choose.page.html',
    styleUrls: ['./upgrade-choose.page.scss'],
})
export class UpgradeChoosePage {

    isFree: boolean = false;
    usdAmount: any;
    rate: any = '';


    constructor(
        private router: Router,
        private alertController: AlertController,
        private global: GlobalService,
        private wallet: WalletService,
        public buyCrypto: BuyCryptoService,
        private toastController: ToastController
    ) { }

    ionViewWillEnter() {
        this.buyCrypto.getRate().catch(err => {
            this.alertMessage('Server Error', err.message);
        });
    }

    /**
     * Present Alert
     * @param message 
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }

    onChange() {
        this.buyCrypto.usdAmount = this.usdAmount * 100;
        console.log(this.buyCrypto.usdAmount);
        if (this.usdAmount == null || this.usdAmount <= 0) {
            this.presentToast();
        } else {
            this.router.navigate(['card-info']);
        }
    }

    async onChangeforFree() {
        const result = await this.buyCrypto.tryFree();
        this.global.loggedWallet.plan = 1;
        this.wallet.changePlan(this.global.loggedWallet.name, this.global.loggedWallet.plan);
        this.router.navigate(['/app/tabs/tab-menu/home']);
    }

    changeFreeToTrue() {
        this.isFree = true;
    }

    changeFreeToFalse() {
        this.isFree = false;
    }

    buy() {

    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'USD missing!',
            duration: 1000,
            translucent: true
        });
        toast.present();
    }
}
