import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDonePage } from './new-done.page';

xdescribe('NewDonePage', () => {
    let component: NewDonePage;
    let fixture: ComponentFixture<NewDonePage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NewDonePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewDonePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
