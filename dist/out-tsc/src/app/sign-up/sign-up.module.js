import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SignUpPage } from './sign-up.page';
var routes = [
    {
        path: '',
        component: SignUpPage
    }
];
var SignUpPageModule = /** @class */ (function () {
    function SignUpPageModule() {
    }
    SignUpPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SignUpPage]
        })
    ], SignUpPageModule);
    return SignUpPageModule;
}());
export { SignUpPageModule };
//# sourceMappingURL=sign-up.module.js.map