import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
var TabHomePage = /** @class */ (function () {
    function TabHomePage(menu, router) {
        this.menu = menu;
        this.router = router;
        this.numNeedMySign = 0;
        this.numWaitOthers = 0;
        this.docs = [
            {
                name: 'Demo-waiting.pdf',
                signed: 0,
                lastModifiedDate: new Date(2019, 6, 27),
                numCoSign: 5,
                numSigned: 3
            },
            {
                name: 'Demo-signed.pdf',
                signed: 1,
                lastModifiedDate: new Date(2019, 5, 23),
                numCoSign: 5,
                numSigned: 3
            }
        ];
    }
    TabHomePage.prototype.ngOnInit = function () {
    };
    /*
     * Get MM dd, YYYY from Date
     */
    TabHomePage.prototype.dateToShortString = function (date) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        //return  date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
        return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    };
    TabHomePage = tslib_1.__decorate([
        Component({
            selector: 'app-tab-home',
            templateUrl: 'tab-home.page.html',
            styleUrls: ['tab-home.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController, Router])
    ], TabHomePage);
    return TabHomePage;
}());
export { TabHomePage };
//# sourceMappingURL=tab-home.page.js.map