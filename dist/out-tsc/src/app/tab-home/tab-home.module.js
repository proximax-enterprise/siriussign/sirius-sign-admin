import * as tslib_1 from "tslib";
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabHomePage } from './tab-home.page';
var TabHomePageModule = /** @class */ (function () {
    function TabHomePageModule() {
    }
    TabHomePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                IonicModule,
                CommonModule,
                FormsModule,
                RouterModule.forChild([{ path: '', component: TabHomePage }])
            ],
            declarations: [TabHomePage]
        })
    ], TabHomePageModule);
    return TabHomePageModule;
}());
export { TabHomePageModule };
//# sourceMappingURL=tab-home.module.js.map