import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var SignInPage = /** @class */ (function () {
    function SignInPage(route) {
        this.route = route;
        this.errorMess = '';
        this.inputDat = {
            email: '',
            pass: ''
        };
    }
    SignInPage.prototype.ngOnInit = function () {
    };
    /**
      * Validate email input
      */
    SignInPage.prototype.checkEmail = function () {
        var emailPattern = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
        var isMatch = emailPattern.test(this.inputDat.email.toLowerCase());
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your email is invalid.';
            return false;
        }
    };
    /**
      * Validate password input
      */
    SignInPage.prototype.checkPass = function () {
        return true;
    };
    /*
     * Validate form input
     */
    SignInPage.prototype.checkInput = function () {
        if (this.checkEmail())
            return this.checkPass();
        return false;
    };
    /*
     * Launch Sign in process
     */
    SignInPage.prototype.signIn = function () {
        var isNoEmail = this.inputDat.email == '';
        var isNoPass = this.inputDat.pass == '';
        if (isNoEmail)
            this.errorMess = 'Please enter your email.';
        else if (isNoPass)
            this.errorMess = 'Please enter your password.';
        else if (this.checkInput()) {
            this.goHome();
        }
    };
    /*
     * Navigate to App's Home
     */
    SignInPage.prototype.goHome = function () {
        this.route.navigate(['app']);
    };
    /*
     * Navigate to sign-up
     */
    SignInPage.prototype.goSignUp = function () {
        this.route.navigate(['sign-up']);
    };
    SignInPage = tslib_1.__decorate([
        Component({
            selector: 'app-sign-in',
            templateUrl: './sign-in.page.html',
            styleUrls: ['./sign-in.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], SignInPage);
    return SignInPage;
}());
export { SignInPage };
//# sourceMappingURL=sign-in.page.js.map