import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NeedSignPage } from './need-sign.page';
var routes = [
    {
        path: '',
        component: NeedSignPage
    }
];
var NeedSignPageModule = /** @class */ (function () {
    function NeedSignPageModule() {
    }
    NeedSignPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [NeedSignPage]
        })
    ], NeedSignPageModule);
    return NeedSignPageModule;
}());
export { NeedSignPageModule };
//# sourceMappingURL=need-sign.module.js.map