import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var NeedSignPage = /** @class */ (function () {
    function NeedSignPage(router) {
        this.router = router;
        this.numNeedMySign = 0;
        this.numWaitOthers = 0;
        this.docs = [
            {
                name: 'Demo-waiting.pdf',
                signed: 0,
                lastModifiedDate: new Date(2019, 6, 27),
                numCoSign: 5,
                numSigned: 3
            },
            {
                name: 'Demo-signed.pdf',
                signed: 1,
                lastModifiedDate: new Date(2019, 5, 23),
                numCoSign: 5,
                numSigned: 3
            }
        ];
    }
    NeedSignPage.prototype.ngOnInit = function () {
    };
    /*
     * Get MM dd, YYYY from Date
     */
    NeedSignPage.prototype.dateToShortString = function (date) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        //return  date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
        return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    };
    /*
     *
     */
    NeedSignPage.prototype.goHome = function () {
        this.router.navigate(['home']);
    };
    NeedSignPage = tslib_1.__decorate([
        Component({
            selector: 'app-need-sign',
            templateUrl: './need-sign.page.html',
            styleUrls: ['./need-sign.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], NeedSignPage);
    return NeedSignPage;
}());
export { NeedSignPage };
//# sourceMappingURL=need-sign.page.js.map