import * as tslib_1 from "tslib";
import { MenuController } from '@ionic/angular';
import { Component } from '@angular/core';
var TabsPage = /** @class */ (function () {
    function TabsPage(menu) {
        this.menu = menu;
    }
    /*
     * Close the side menu
     */
    TabsPage.prototype.closeMenu = function () {
        this.menu.close("menu");
    };
    /*
     *  Navigate to new doc signing page
     */
    TabsPage.prototype.onCreate = function () {
        this.closeMenu();
    };
    TabsPage = tslib_1.__decorate([
        Component({
            selector: 'app-tabs',
            templateUrl: 'tabs.page.html',
            styleUrls: ['tabs.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs.page.js.map