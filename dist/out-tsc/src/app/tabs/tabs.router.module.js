import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
var routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'tab-menu',
                children: [
                    {
                        path: '',
                        loadChildren: '../menu/menu.module#MenuPageModule'
                    }
                ]
            },
            {
                path: 'tab-home-zz',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-home/tab-home.module#TabHomePageModule'
                    }
                ]
            },
            {
                path: 'tab-verify',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-verify/tab-verify.module#TabVerifyPageModule'
                    }
                ]
            },
            {
                path: 'tab-history',
                children: [
                    {
                        path: '',
                        loadChildren: '../tab-history/tab-history.module#TabHistoryPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: 'tab-menu',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'tabs/tab-menu',
        pathMatch: 'full'
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());
export { TabsPageRoutingModule };
//# sourceMappingURL=tabs.router.module.js.map