import * as tslib_1 from "tslib";
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabHistoryPage } from './tab-history.page';
var TabHistoryPageModule = /** @class */ (function () {
    function TabHistoryPageModule() {
    }
    TabHistoryPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                IonicModule,
                CommonModule,
                FormsModule,
                RouterModule.forChild([{ path: '', component: TabHistoryPage }])
            ],
            declarations: [TabHistoryPage]
        })
    ], TabHistoryPageModule);
    return TabHistoryPageModule;
}());
export { TabHistoryPageModule };
//# sourceMappingURL=tab-history.module.js.map