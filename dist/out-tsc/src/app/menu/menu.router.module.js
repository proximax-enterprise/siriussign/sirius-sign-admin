import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
/*const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../tab-home/tab-home.module#TabHomePageModule'
          },
          {
            path: 'need-sign',
            loadChildren: '../need-sign/need-sign.module#NeedSignPageModule'
          }
        ]
      },
      {
        path: 'need-sign',
        children: [
          {
            path: '',
            loadChildren: '../need-sign/need-sign.module#NeedSignPageModule'
          },
          {
            path: 'home',
            loadChildren: '../tab-home/tab-home.module#TabHomePageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/home',
    pathMatch: 'full'
  },
  {
    path: 'need-sign',
    redirectTo: 'menu/need-sign',
    pathMatch: 'full'
  }
];
*/
var routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: '../tab-home/tab-home.module#TabHomePageModule'
    },
    {
        path: 'need-sign',
        loadChildren: '../need-sign/need-sign.module#NeedSignPageModule'
    }
];
var MenuRoutingModule = /** @class */ (function () {
    function MenuRoutingModule() {
    }
    MenuRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], MenuRoutingModule);
    return MenuRoutingModule;
}());
export { MenuRoutingModule };
//# sourceMappingURL=menu.router.module.js.map