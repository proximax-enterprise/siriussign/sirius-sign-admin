import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
var CodeVerifyPage = /** @class */ (function () {
    function CodeVerifyPage(router, alertCtrl) {
        this.router = router;
        this.alertCtrl = alertCtrl;
    }
    CodeVerifyPage.prototype.ngOnInit = function () {
    };
    /**
      * Navigate to sign-in
      */
    CodeVerifyPage.prototype.goSignIn = function () {
        this.router.navigate(['sign-in']);
    };
    /**
      * Launch verify phone number process
      */
    CodeVerifyPage.prototype.onGetCode = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Enter your code',
                            inputs: [
                                {
                                    name: 'code',
                                    type: 'text'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Ok',
                                    handler: function () {
                                        _this.goSignIn();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CodeVerifyPage = tslib_1.__decorate([
        Component({
            selector: 'app-code-verify',
            templateUrl: './code-verify.page.html',
            styleUrls: ['./code-verify.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router, AlertController])
    ], CodeVerifyPage);
    return CodeVerifyPage;
}());
export { CodeVerifyPage };
//# sourceMappingURL=code-verify.page.js.map