import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CodeVerifyPage } from './code-verify.page';
var routes = [
    {
        path: '',
        component: CodeVerifyPage
    }
];
var CodeVerifyPageModule = /** @class */ (function () {
    function CodeVerifyPageModule() {
    }
    CodeVerifyPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CodeVerifyPage]
        })
    ], CodeVerifyPageModule);
    return CodeVerifyPageModule;
}());
export { CodeVerifyPageModule };
//# sourceMappingURL=code-verify.module.js.map