# Install
## Install Ionic
Follow instruction at `https://ionicframework.com/docs/installation/cli`
## Install dependencies
Run ```npm install```  
If there's any packages update, run ```node patch.js``` to fix crypto.js errors on browser, stream-http on Chrome 77 before building app. This is the postscript, so it will run follow npm install automatically.
# Custom
# Run app
Follow instruction from Ionic document. Recommend using Ionic's Cordova running app cli.
## Android
Follow `https://ionicframework.com/docs/building/android`
## iOS
Follow `https://ionicframework.com/docs/building/ios`