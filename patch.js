const fs = require('fs');

function replaceFile(filePath, searchString, replaceString) {
    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        const result = data.replace(searchString, replaceString);

        fs.writeFile(filePath, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}

const f = 'node_modules/@angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js';
replaceFile(f, /node: false/g, "node: {fs: 'empty', global: true, crypto: 'empty', tls: 'empty', net: 'empty', process: true, module: false, clearImmediate: false, setImmediate: false}");

let f_crypto = 'node_modules/tsjs-xpx-chain-sdk/node_modules/ws/lib/websocket.js';
replaceFile(f_crypto, 'require(\'crypto\');', 'require(\'crypto-browserify\');');

f_crypto = 'node_modules/tsjs-xpx-chain-sdk/dist/src/core/crypto/Crypto.js';
replaceFile(f_crypto, 'require(\'crypto\');', 'require(\'crypto-browserify\');');

f_crypto = 'node_modules/tsjs-chain-xipfs-sdk/build/main/src/lib/cipher/nem-keys-cipher-stream.js';
replaceFile(f_crypto, 'require("crypto")', 'require("crypto-browserify")');

f_crypto = 'node_modules/tsjs-chain-xipfs-sdk/build/main/src/lib/cipher/nem-keys-decipher-stream.js';
replaceFile(f_crypto, 'require("crypto")', 'require("crypto-browserify")');

// fix chrome bug version 77
const streamHttpRequestDirectory = 'node_modules/stream-http/lib/capability.js'
replaceFile(streamHttpRequestDirectory, "exports.fetch = isFunction(global.fetch) && isFunction(global.ReadableStream)",
    "//exports.fetch = isFunction(global.fetch) && isFunction(global.ReadableStream)");

const streamHttpRequestInIpfsHttpClient = 'node_modules/ipfs-http-client/node_modules/stream-http/lib/capability.js';
replaceFile(streamHttpRequestInIpfsHttpClient, "exports.fetch = isFunction(global.fetch) && isFunction(global.ReadableStream)",
    "//exports.fetch = isFunction(global.fetch) && isFunction(global.ReadableStream)");


// add this to package.json
// "ionic": {
//     "fs": false,
//     "path": false,
//     "os": false,
//     "tls": false,
//     "net": false
//   }